<?php

namespace Modules\WebSite;

use Illuminate\Http\Request;
use Modules\Blog\Slides\Slides;
use Modules\DigitalMarketing\ViewCounter\ViewCounter;
use Modules\Core\Http\Controller as Controller;
use Users;

class SiteController extends Controller
{

    public function __construct(ViewCounter $viewCounter)
    {
        $this->slide = new Slides;
        $this->viewCounter = $viewCounter;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {

        $content = [ 'slide' => $this->slide->get()];

        $this->viewCounter->add($request->path());
        return view('WebSite::pages.index')->with($content);
    }
}
