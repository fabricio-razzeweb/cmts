@if (count($services)>=1)
    <section class="section section-default">
        <div class="section-title text-center">
            <h3 class="section-title-item">Instalações</h3>
        </div>
        <div class="row">
            @foreach($services as $service)
                @include('WebSite::components.service-card',$service)
            @endforeach
        </div>
    </section>
@endif
