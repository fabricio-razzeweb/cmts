@if (isset($depoiments) && count($depoiments)>0)
    <section id="banner-depositions">
        <div class="bg-primary-transparent full">
            <div id="carousel-depositions" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    @php
                        $i = 0;
                        $complement = ($i==0)?'active':'';
                    @endphp

                    @foreach ($depoiments as $depoiment)

                        @include('WebSite::components.depositions-item')

                    @endforeach
                </div>
                <a class="left carousel-control" href="#carousel-depositions" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-depositions" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </section>
@endif
