<ol class="breadcrumb">
    <li><a href="{{URL::asset('')}}">Início</a></li>
    <li><a href="#">Notícias</a></li>
</ol>
<section class="section section-default">
    <div class="section-title text-center">
        <h3 class="section-title-item">Notícias</h3>
    </div>
    @if (count($posts)>=1)
        <div class="row">
            @foreach ($posts as $post)
                @include('WebSite::components.post-card')
            @endforeach
            <div class="col-md-10 col-md-offset-1 text-center">
                <a href="{{URL::asset('noticias',env('SSL'))}}" type="button" class="btn btn-circle btn-circle-default">Ver mais notícias</a>
            </div>
        </div>
    @else
        <p class="text-center lead">Nenhuma notícia foi encontrada.</p>
    @endif
</section>
