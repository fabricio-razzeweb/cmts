<!DOCTYPE html>
<html>
    @include('WebSite::basic.head')
    <body>
        @include('WebSite::basic.preloader')
        @include('WebSite::basic.header')
        @include('WebSite::basic.navbar')
        <main class="content col-md-9">
            @yield('content')
        </main>
        <div class="clearfix"></div>
        @yield('full-content')
        @include('WebSite::basic.footer')
        @include('WebSite::basic.scripts')
    </body>
</html>
