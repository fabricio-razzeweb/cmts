<!DOCTYPE html>
<html lang="pt-BR">
    @include('Site::basic.head')
    <body class="paralax">
        @include('Site::basic.preloader')
        @include('Site::basic.navbar')
        @include('Site::components.header-parallax',['title'=>"Serviços"])
        <main class="main-paralax">
            <section class="section section-default">
                <div class="container">
                    <div class="section-title text-center">
                        <h3 class="section-title-item">Os melhores serviços em mastologia para você</h3>
                    </div>
                    <div class="row">
                        @foreach ($services as $service)
                            @include('Site::components.service-card')
                        @endforeach
                        <div class="col-xs-10 col-xs-offset-1 text-right">
                            <p>páginas:</p>
                            {{$services->links()}}
                        </div>
                    </div>
                </div>
            </section>
            @include('Site::basic.footer')
        </main>
        @include('Site::basic.scripts')
    </body>
</html>
