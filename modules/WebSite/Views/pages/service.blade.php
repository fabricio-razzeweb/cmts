<!DOCTYPE html>
<html lang="pt-BR">
    @include('Site::basic.head')
    <body class="paralax">
        @include('Site::basic.preloader')
        @include('Site::basic.navbar')
        @include('Site::components.header-parallax',['title'=>"$service->title", 'background'=>URL::asset('src/imgs/services/'.$service->photo)])
        <main class="main-paralax">
            <ol class="breadcrumb">
                <li><a href="{{URL::asset('',env('SSL'))}}">Ínicio</a></li>
                <li><a href="{{URL::asset('',env('SSL'))}}">Serviços</a></li>
                <li class="active">{{$service->title}}</li>
            </ol>
            <section class="section section-default">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="post">
                                 <div class="post-header">
                                     <div class="post-header-img">
                                         <img src="{{URL::asset('src/imgs/services/'.$service->photo)}}" alt="{{$service->title}}"/>
                                     </div>
                                     <div class="post-header-info">
                                         <ul class="list-inline">
                                             <li>
                                                 <i class="fa fa-calendar-o"></i>
                                                 <span class="post-data">{{$service->created_at->format('d/M/Y')}}</span>
                                             </li>
                                             <li>
                                                 <i class="fa fa-eye"></i>
                                                 <span class="post-views">
                                                     {{$service->views}}
                                                 </span>
                                             </li>
                                         </ul>
                                     </div>
                                 </div>
                                 <div class="post-content">
                                     <h3 class="post-content-title">{{$service->title}}</h3>
                                     <p class="lead">
                                         {!!$service->content!!}
                                     </p>
                                 </div>
                                 <div class="post-footer">

                                 </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="post-categories">
                                <div class="post-categories-title">
                                    <h3 class="post-categories-title-item">Que tal ler um artigo?</h3>
                                </div>
                                <div class="post-categories-main">
                                    <ul class="list-group">
                                        @foreach ($categorys as $category)
                                            <a href="{{URL::asset('artigos/categoria/'.$category->link, env('SSL'))}}"class="list-group-item">
                                                {{$category->name}} <span class="badge">{{$category->views}}</span>
                                            </a>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="post-categories-footer">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section tertiary">
                <div class="container">
                    <div class="section-title text-center">
                        <h3 style="color: #fff; border-color: #fff;" class="section-title-item">Artigos que talvez você queira ler:</h3>
                    </div>
                    <div class="row">
                        @foreach ($posts as $post)
                            @include('Site::components.post-card')
                        @endforeach
                    </div>
                </div>
            </section>
        @include('Site::basic.footer')
        </main>
        @include('Site::basic.scripts')
    </body>
</html>
