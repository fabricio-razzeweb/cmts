@extends('WebSite::layouts.default')
@section('content')
    <ol class="breadcrumb">
        <li><a href="{{URL::asset('')}}">Início</a></li>
        <li><a href="{{URL::asset('noticias')}}">Notícias</a></li>
        <li><a href="#">{{$post->title}}</a></li>
    </ol>
    <section class="section section-default">
        <div class="row">
            <div class="col-md-9">
                <div class="post">
                     <div class="post-header">
                         <div class="post-header-img">
                             <img src="{{URL::asset('src/imgs/posts/'.$post->photo)}}" alt="{{$post->title}}"/>
                         </div>
                         <div class="post-header-info">
                             <ul class="list-inline">
                                 <li>
                                     <i class="fa fa-calendar-o"></i>
                                     <span class="post-data">{{$post->created_at->format('d/M/Y')}}</span>
                                 </li>
                                 <li>
                                     <i class="fa fa-user-o"></i>
                                     <span class="post-author">{{$author->name}}</span>
                                 </li>
                                 <li>
                                     <i class="fa fa-eye"></i>
                                     <span class="post-views">
                                         {{$post->views}}
                                     </span>
                                 </li>
                             </ul>
                         </div>
                     </div>
                     <div class="post-content">
                         <h3 class="post-content-title">{{$post->title}}</h3>
                         <p class="lead">
                             {!!$post->content!!}
                         </p>
                     </div>
                     <div class="post-footer">
                         @include('WebSite::components.shared-buttons')
                     </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card card-team col-xs-12">
                    <img class="img-circle card-team-img" src="{{URL::asset('src/imgs/users/'.$author->photo)}}"/>
                    <div class="card-team-info">
                        <div class="text-center card-team-member">
                            <h3>{{$author->name}}</h3>
                        </div>
                    </div>
                    <div class="card-team-body">
                        <p class="lead">
                            {{$author->description}}
                        </p>
                    </div>
                </div>
                <!-- <div class="post-categories">
                    <div class="post-categories-title">
                        <h3 class="post-categories-title-item">Categorias</h3>
                    </div>
                    <div class="post-categories-main">
                        <ul class="list-group">
                            @foreach ($categorys as $category)
                                <a href="{{URL::asset('artigos/categoria/'.$category->link, env('SSL'))}}"class="list-group-item">
                                    {{$category->name}} <span class="badge">{{$category->views}}</span>
                                </a>
                            @endforeach
                        </ul>
                    </div>
                    <div class="post-categories-footer">

                    </div>
                </div> -->
            </div>
        </div>
    </section>
@endsection
