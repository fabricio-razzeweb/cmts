<!DOCTYPE html>
<html lang="pt-BR">
    @include("WebSite::basic.head")
    <body class="paralax body-diagonal">
        @include("WebSite::basic.preloader")
        @include('WebSite::basic.navbar')
        <header class="background-paralax header-paralax">
            <div class="bg-primary-transparent">
                <h1 class="title-paralax">Sobre nós</h1>
            </div>
        </header>
        <main class="main-paralax">
            <section class="section section-default">
                <div class="container">
                    <div class="section-title text-center">
                        <h3 class="section-title-item">Equipe</h3>
                    </div>
                    <div class="row">
                        @foreach ($doctors as $doctor)
                            <div class="col-md-4">
                                <div class="card card-team col-md-12"> <br>
                                     <img class="icon icon-eg icon-rounded icon-center" src="{{URL::asset('src/imgs/users/'.$doctor->photo, env('SSL'))}}"/>
                                    <div class="card-team-info">
                                        <div class="text-center card-team-member">
                                            <h3>{{$doctor->name}}</h3>
                                        </div>
                                    </div>
                                    <div class="card-team-body">
                                        <p class="lead">
                                            {!!substr($doctor->description, 0, 80)!!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
            <section class="section-full section-primary diagonal diagonal-right diagonal-bottom">
                <h2 class="text-center">História</h2>
                <div class="lead">
                    {!!$enterprise->history!!}
                </div>
            </section>
            <section id="schedule-appointment">
                <div class="section bg-primary-transparent">
                    <div class="container">
                        <!--<h3>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Ab quam consequatur, adipisci, vitae repellat neque impedit
                            maxime dolor vero amet tempora harum quas voluptatibus rem
                            quasi consequuntur facilis explicabo accusantium.
                        </h3>-->
                        <a href="{{URL::asset('login')}}" class="btn btn-circle btn-circle-transparent">Marque uma consulta</a>
                    </div>
                </div>
            </section>
            @include('WebSite::basic.footer')
        </main>
        @include('WebSite::basic.scripts')
    </body>
</html>
