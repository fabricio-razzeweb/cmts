<footer class="footer flat-black">
    <div class="container footer-main">
        <div class="row">
            <h3>{{$myEnterprise->name}}</h3>
            <p>{{$myEnterprise->location}}</p>
            <p>{{$myEnterprise->phone}}</p>
        </div>
    </div>
    <div class="footer-bottom" style="background-color:rgba(0,0,0,0.3)">
        <p class="text-center">Todos os Direitos Reservados {{$myEnterprise->name}} © Desenvolvido por Razze Web.</p>
    </div>
</footer>
