<head>
    <title>{{env('APP_NAME')}}</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
    <link href="{{ URL::asset('libs/bootstrap/css/bootstrap.min.css', env('SSL')) }}" rel="stylesheet">
    <link href="{{ URL::asset('libs/font-awesome/css/font-awesome.min.css', env('SSL')) }}" rel="stylesheet">
    <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
    <link href="{{ URL::asset('assets/main/css/style.css', env('SSL')) }}" rel="stylesheet">
    <link href="{{ URL::asset('libs/lightslider/dist/css/lightslider.min.css')}}" rel="stylesheet">
    <link rel="shortcut icon" href="{{URL::asset('src/layout/logos/logo-imasto-pink.png', env('SSL'))}}">
    <script
      src="{{ URL::asset('libs/jquery/jquery.js', env('SSL')) }}"></script>
     <script
      src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
      integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
      crossorigin="anonymous"></script>
</head>
