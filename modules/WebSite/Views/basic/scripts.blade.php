<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
<script src="{{URL::asset('assets/main/js/main.js')}}"></script>
<script>
    AOS.init();
</script>
