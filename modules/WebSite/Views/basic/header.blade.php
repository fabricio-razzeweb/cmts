<header class="header">
    <div class="bg-primary-transparent" style="background-color:transparent">
        <h1 class="title text-center">
            <img src="{{URL::asset('src/layout/logos/logo.png')}}" style="max-width:70%; text-align:center">
        </h1>
    </div>
</header>
