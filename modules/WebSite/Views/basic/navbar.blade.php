<nav class="col-md-3">
    <ul class="nav navbar-nav">
        <li class="navbar-list">
            <a class="navbar-list-item" href="{{URL::asset('')}}">
                Início
            </a>
        </li>
        <li class="navbar-list">
            <a class="navbar-list-item" href="{{URL::asset('cursos')}}">
                Cursos
            </a>
        </li>
        <li class="navbar-list">
            <a class="navbar-list-item" href="{{URL::asset('instalacoes')}}">
                Instalações
            </a>
        </li>
        <li class="navbar-list">
            <a class="navbar-list-item" href="{{URL::asset('agenda')}}">
                Agenda
            </a>
        </li>
        <li class="navbar-list">
            <a class="navbar-list-item" href="{{URL::asset('noticias')}}">
                Notícias
            </a>
        </li>
        <li class="navbar-list">
            <a class="navbar-list-item" href="{{URL::asset('historico')}}">
                Histórico
            </a>
        </li>
        <!-- <li class="navbar-list">
            <a class="navbar-list-item" href="{{URL::asset('contato')}}">
                Contato
            </a>
        </li> -->
        <li class="navbar-list">
            <a class="navbar-list-item" href="{{URL::asset('loja')}}">
                Venda de Pianos
            </a>
        </li>
    </ul>
</nav>
