<div class="col-md-4" data-aos="fade-up" data-aos-duration="500">
    <div class="card card-post">
        <div class="card-post-img">
            <img src="{{URL::asset('src/imgs/posts/'.$post->photo)}}"/>
        </div>
        <div class="card-post-info">
            <div class="col-xs-2">
                <div class="card-post-data">
                    <span class="card-post-day">{{$post->created_at->format("d")}}</span>
                    <span class="card-post-month">{{$post->created_at->format("M")}}</span>
                </div>
            </div>
            <div class="col-xs-10">
                <div class="card-post-title">
                    <h3>{{substr($post->title,0,20)}}...</h3>
                </div>
                <div class="card-post-author">
                    <p>{{$post->user->name}}</p>
                </div>
            </div>
        </div>
        <div class="card-post-body">
            <p>
                {!!substr(strip_tags($post->content), 0, 120)!!}
            </p>
            <div class="ellipsis"></div>
        </div>
        <a href='{{URL::asset("noticias/$post->link")}}' class="btn btn-circle btn-circle-default">Leia mais</a>
    </div>
</div>
