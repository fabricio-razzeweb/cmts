@if ($loop->first)
    @php
        $complement = 'active';
    @endphp
    @else
        @php
            $complement = '';
        @endphp
@endif
<div class="item {{$complement}}">
    <div class="carousel-caption">
        <img class="icon icon-eg icon-rounded icon-center" src="{{URL::asset('src/imgs/depoiments/'.$depoiment->photo)}}"/>
        <h2>{{$depoiment->name}}</h2>
        <p class="lead">
            {{$depoiment->content}}
        </p>
    </div>
</div>
