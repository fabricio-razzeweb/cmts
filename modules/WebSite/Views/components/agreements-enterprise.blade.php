<hr>
<section class="section section-default container">
    <div class="section-title text-center">
        <h3 class="section-title-item">Conveniados</h3>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-push-2">
            <ul id="agreements-gallery">
                @foreach ($agreements as $agreement)
                    @if ($loop->index == 10)
                        @break
                    @endif
                <li class="item-slide">
                    <img src="{{URL::asset('src/imgs/agreements/'. $agreement->photo)}}"
                        width="140"
                        height="140"/>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</section>
