<!-- Modal -->
<div class="modal fade" id="popUpModal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header f-white">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4><span class="fa fa-comment"></span> Mensagem do sistema</h4>
      </div>
      <div class="modal-body">
        <h4 class="popUpModal-msg text-center">
            @if (session('alertMsg'))
                {{session('alertMsg')}}
            @endif
        </h4>
      </div>
      <div class="modal-footer text-center">
          <button class="btn btn-default btn-danger pull-right link-action" data-dismiss="modal">
            <span class="fa fa-close"></span> Fechar
          </button>
      </div>
    </div>
  </div>
</div>

@if (session('alertMsg'))
    <script type="text/javascript">
        $(window).load(function(){
            $("#popUpModal").modal('show');
        });
    </script>
@endif
