<div class="row">
    <br>
    <div class="col-xs-4 gallery-item-left">
        @foreach ($slide as $slidePhoto)
            <a href="{{($slidePhoto->btn_link)? $slidePhoto->btn_link : '#'}}">
                <div class="col-md-12 gallery-item gallery-item-0{{$loop->iteration}}" style="background-image:url({{$slidePhoto->photo}})">
                    <div class="gallery-item-effect">
                        <div class="gallery-item-legend text-center">{{$slidePhoto->title}}</div>
                    </div>
                </div>
            </a>
            @if ($loop->iteration == 3)
                @break
            @endif
        @endforeach
    </div>
    <div class="col-xs-8">
        @foreach ($slide as $slidePhoto)
            @if ($loop->iteration == 4)
                <a href="{{($slidePhoto->btn_link)? $slidePhoto->btn_link : '#'}}">
                    <div class="col-md-12 gallery-item gallery-item-0{{$loop->iteration}}" style="background-image:url({{$slidePhoto->photo}})">
                        <div class="gallery-item-effect">
                            <div class="gallery-item-legend text-center">{{$slidePhoto->title}}</div>
                        </div>
                    </div>
                </a>
                @break
            @endif
        @endforeach
    </div>
    <div class="col-xs-4 gallery-item-left">
        @foreach ($slide as $slidePhoto)
            @if ($loop->iteration == 5)
                <a href="{{($slidePhoto->btn_link)? $slidePhoto->btn_link : '#'}}">
                    <div class="col-md-12 gallery-item gallery-item-0{{$loop->iteration}}" style="background-image:url({{$slidePhoto->photo}})">
                        <div class="gallery-item-effect">
                            <div class="gallery-item-legend text-center">{{$slidePhoto->title}}</div>
                        </div>
                    </div>
                </a>
                @break
            @endif
        @endforeach
    </div>
    <div class="col-xs-4">
        @foreach ($slide as $slidePhoto)
            @if ($loop->iteration == 6)
                <a href="{{($slidePhoto->btn_link)? $slidePhoto->btn_link : '#'}}">
                    <div class="col-md-12 gallery-item gallery-item-0{{$loop->iteration}}" style="background-image:url({{$slidePhoto->photo}})">
                        <div class="gallery-item-effect">
                            <div class="gallery-item-legend text-center">{{$slidePhoto->title}}</div>
                        </div>
                    </div>
                </a>
                @break
            @endif
        @endforeach
    </div>
    <br><br>
</div>
