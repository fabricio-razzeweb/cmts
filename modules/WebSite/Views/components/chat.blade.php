<input id="session_hash" type="hidden" value="{{$session_hash}}">
<div class="modal fade chat-modal" id="chat-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header tertiary">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="text-center">Perguntas? Fale conosco!</h4>
                <p class="text-center">Online há 4 horas.</p>
            </div>
            <div class="modal-body">
                <div id="chat-info">
                    <div class="new-user">
                        <h3 class="title">Olá, novo por aqui?</h3>
                        <p>Como seu primeiro acesso, precisamos que nos envie algumas informações!</p>
                        <form>
                            <div class="form-group">
                                <input type="text" id="user-name" class="form-control" placeholder="Nome completo" required>
                            </div>
                            <div class="form-group">
                                <input type="tef" id="user-phone" class="form-control" placeholder="Telefone" required>
                            </div>
                            <div class="form-group">
                                <input type="email" id="user-email" class="form-control" placeholder="E-mail" required>
                            </div>
                            <div class="form-group">
                                <button type="button" id="user-send" class="btn btn-circle btn-circle-default">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="chat-messages">
                    <div class="message clearfix">
                        <div class="message-container received">
                            <span>Olá! Como posso ajudá-lo?</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="chat-textarea-container">
                    <textarea id="message" class="chat-textarea" rows="1" placeholder="Digite sua mensagem aqui..." autofocus></textarea>
                </div>
                <div class="chat-controls">
                    <button id="send-message" class="btn btn-circle btn-circle-default">
                        <i class="fa fa-paper-plane" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="chat-alert" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ocorreu um erro</h4>
            </div>
            <div class="modal-body">
                Por favor, preencha todos os campos.
            </div>
        </div>
    </div>
</div>
