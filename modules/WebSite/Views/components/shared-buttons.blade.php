<?php $url='http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>
<!--SOCIAL-BUTTONS-->
<div class="row col-sm-10 col-sm-offset-1"><br /><br />

    <div class="col-md-6" >
      <a href="https://twitter.com/share?url={{$url}}" class="btn btn-lg btn-danger btn-circle btn-twitter btn-block" style="background-color:#1da1f2">
        <img src="{{asset('src/layout/icons/icon-twitter.png')}}" alt="twitter" class="small-icon" />
          Compartilhar
      </a>
    </div>

    <div class="col-md-6">
      <a href="http://www.facebook.com/sharer.php?u={{$url}}" class="btn btn-lg btn-circle btn-primary btn-block"  style="background-color:#3b5998">
        <img src="{{asset('src/layout/icons/icon-facebook.png')}}" alt="facebook" class="small-icon" />
          Compartilhar
      </a>
    </div>

</div>
