<section class="section primary">
    <div class="container">
        <div class="row">
            <div class="col-md-4 item">
                <div class="row">
                    <div class="col-xs-3">
                        <img class="icon icon-lg icon-rounded" src="{{URL::asset('src/layout/icons/icon-phone.png')}}"/>
                    </div>
                    <div class="col-xs-9 text-center">
                        <b>Telefones:</b>
                        <p class="text-center">{{$myEnterprise->phone}}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 item">
                <div class="row">
                    <div class="col-xs-3">
                        <img class="icon icon-lg icon-rounded" src="{{URL::asset('src/layout/icons/icon-location.png')}}"/>
                    </div>
                    <div class="col-xs-9 text-center">
                        <b>Localização:</b>
                        <p class="text-center">{{$myEnterprise->location}}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-xs-3">
                        <img class="icon icon-lg icon-rounded" src="{{URL::asset('src/layout/icons/icon-email.png')}}"/>
                    </div>
                    <div class="col-xs-9 text-center">
                        <b>Email:</b>
                        <p class="text-center">{{$myEnterprise->email}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
