<div class="col-md-4 item" data-aos="zoom-in" data-aos-duration="1000">
    <img class="icon icon-eg icon-rounded icon-center" src={{URL::asset("src/imgs/services/"."$service->photo", env('SSL'))}}>
    <div class="item-description text-center">
        <a href={{URL::asset("servicos/"."$service->link", env('SSL'))}}>
            <h3>{{$service->title}}</h3>
        </a>
        <p class="content">
            <p class="text-center">
                {{substr(strip_tags($service->content), 0, 120)}}
            </p>
        </p>
    </div>
</div>
