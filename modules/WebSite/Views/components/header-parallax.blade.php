<header class="background-paralax header-paralax" style={{(isset($background))? "background-image:url($background)" : ''}}>
    <div class="bg-primary-transparent">
        <h1 class="title-paralax">{{$title}}</h1>
    </div>
</header>
