<section id="banner-contact">
    <div class="section-title section-title-bg text-center">
        <h2 class="title">Onde nos encontrar?</h2>
    </div>
    <div class="section-full">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <iframe class="g-maps"
                    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d687.251554627553!2d-38.51690607052283!3d-12.99218758523223!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x16712b88503a347c!2sCentro+Musical+Teodoro+Salles!5e0!3m2!1spt-BR!2sbr!4v1502986992111" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</section>
