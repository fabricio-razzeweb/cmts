<header>
    <div class="bg-primary-transparent full'">
        <div id="carousel-header" class="carousel slide bg-primary" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                @php
                    $i=0;
                @endphp

                @foreach ($slide as $slideItem)
                    @if ($loop->first)
                        @php
                            $complement = 'active';
                        @endphp
                        @else
                            @php
                                $complement = '';
                            @endphp
                    @endif
                    <li data-target="#carousel-header" data-slide-to="{{$i}}" class="{{$complement}}"></li>
                    @php
                        $i++;
                    @endphp
                @endforeach
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                @foreach ($slide as $slideItem)

                    @if ($loop->first)
                        @php
                            $complement = 'active';
                        @endphp
                        @else
                            @php
                                $complement = '';
                            @endphp
                    @endif

                    <div class="item {{$complement}} background-paralax header-paralax" style="background-image:url({{URL::asset('src/imgs/slides/'.$slideItem->photo, env('SSL'))}})">
                        <div class="carousel-caption">
                            <h2 class="text-center">{{$slideItem->title}}</h2>
                            @if (isset($slideItem->btn_content) && isset($slideItem->btn_link))
                                <a href="{{$slideItem->btn_link}}" class="btn btn-circle btn-circle-default">{{$slideItem->btn_content}}</a>
                            @endif

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</header>
