<?php
namespace Modules\WebSite;

use Modules\MyEnterprise\MyEnterprise;
use Illuminate\Contracts\View\View;

class SiteInformationViewComposer
{

    function __construct(MyEnterprise $myEnterprise)
    {
        $this->myEnterprise = $myEnterprise;
    }

    public function compose(View $view)
    {
        return $view->with('myEnterprise', $this->myEnterprise->first());
    }
}
