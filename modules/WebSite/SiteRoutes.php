<?php

    Route::group(['middleware'=>['web']], function(){
        Route::get("","Modules\WebSite\SiteController@index");
        Route::get("artigos","Modules\Blog\Posts\PostsSiteController@getAllForSite");
        Route::get("artigos/categoria/{category_name}/{category_id}","Modules\Blog\Posts\PostsSiteController@getAllForCategoryInSite");
        Route::get("sobre-nos","Modules\MyEnterprise\MyEnterpriseController@getOneForSite");
        Route::get("artigos/{title}/{id}","Modules\Blog\Posts\PostsSiteController@getOneForSite");
    });

 ?>
