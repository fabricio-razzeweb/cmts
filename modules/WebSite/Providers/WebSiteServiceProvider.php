<?php

namespace Modules\WebSite\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\WebSite\SiteInformationViewComposer;

class WebSiteServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer("WebSite::basic.footer",
        \Modules\WebSite\SiteInformationViewComposer::class);

        $this->loadRoutesFrom(__DIR__.'/../SiteRoutes.php');
        $this->loadViewsFrom(__DIR__.'/../Views', 'WebSite');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
