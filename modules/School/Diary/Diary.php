<?php
namespace Modules\School\Diary;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Support\PrepareDataToSave;

class Diary extends Model
{
    use SoftDeletes, PrepareDataToSave;

    protected $table = 'diary';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'title',
      'description',
      'date',
      'hour',
      'users_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function getDateAttribute($value)
    {
        $value = explode("-", $value);
        return $value[0]."/".$value[1]."/".$value[2];
    }
}
