<?php
namespace Modules\School\Diary;

use Illuminate\Http\Request;
use Modules\School\Diary\Diary;
use Modules\Core\Support\RestAPI\BaseControllerRestAPI;
use Modules\School\Diary\DiaryRepository;
use Modules\School\Diary\Requests\DiaryRequestStore;
use Modules\School\Diary\Requests\DiaryRequestUpdate;

class DiaryController extends BaseControllerRestAPI
{
    protected $model;
    protected $nameElementUI = "Agenda";
    protected $resourceName = "event";
    protected $resourceGroupName = "events";

    public function __construct(Diary $model, DiaryRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    public function store(DiaryRequestStore $request)
    {
        return parent::genericStore($request);
    }

    public function update(DiaryRequestUpdate $request, int $id)
    {
        return parent::genericUpdate($request, $id);
    }
}
