<?php

Route::group(['middleware'=>['web']],function()
{
    Route::group(['prefix'=>'admin/dashboard','middleware'=>['auth_common_users']],function ()
    {

        Route::group(['prefix'=>'api','middleware'=>['jwt.auth']],function(){

            Route::resource('diary',\Modules\School\Diary\DiaryController::class);

        });

    });
    Route::get("agenda","Modules\School\Diary\DiarySiteController@index");
});
