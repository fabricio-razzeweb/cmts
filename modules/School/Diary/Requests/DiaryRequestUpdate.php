<?php
namespace Modules\School\Diary\Requests;

use Modules\Core\Http\JWTRequest as JWTRequest;

class DiaryRequestUpdate extends JWTRequest
{
    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        return [
            'title' => 'required|max:80|min:5',
            'description' => 'required',
            'date' => 'required',
            'hour' => 'required',
            'users_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'users_id.required'=>"Você precisa fazer login para executar está ação",
            'title.min' => 'Preencha o campo de Título',
            'title.required' => 'Preencha o campo de Título',
            'description.required' => 'Preencha o campo de Descrição',
            'date.required' => 'Preencha o campo de Data',
            'hour.required' => 'Preencha o campo de Hora',
        ];
    }

}
