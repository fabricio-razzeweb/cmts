@extends('WebSite::layouts.default')
@section('content')
    <ol class="breadcrumb">
        <li><a href="{{URL::asset('')}}">Início</a></li>
        <li><a href="#">Agenda</a></li>
    </ol>
    <section class="section section-default">
        <div class="section-title text-center">
            <h3 class="section-title-item">Agenda</h3>
        </div>

        @if (count($events) > 0)
            @foreach ($events as $commitment)
                @include('Diary::Site.card')
            @endforeach
        @else
            <h2>Opss! ainda não divulgamos nossos eventos</h2>
        @endif

    </section>
@endsection
