<div class="col-md-6" data-aos="fade-up" data-aos-duration="500">
    <a href="#" class="card card-commitment">
        <div class="col-xs-2">
            <div class="card-commitment-data">
                <span class="card-commitment-day">{{(new Carbon\Carbon($commitment->date))->format("d")}}</span>
                <span class="card-commitment-month">{{(new Carbon\Carbon($commitment->date))->format("F")}}</span>
            </div>
        </div>
        <div class="col-xs-10">
            <div class="card-commitment-title">
                <h3>{{$commitment->title}}</h3>
            </div>
            <div class="card-commitment-body">
                <p>
                    {!!substr(strip_tags($commitment->description), 0, 200)!!}
                </p>
                <div class="ellipsis"></div>
            </div>
        </div>
    </a>
</div>
