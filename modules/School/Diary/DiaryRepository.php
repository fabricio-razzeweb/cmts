<?php
namespace Modules\School\Diary;

use Modules\Core\Support\Contracts\RepositoryInterface;
use Modules\Core\Support\BaseRepository;
use Modules\School\Diary\Diary;

class DiaryRepository extends BaseRepository implements RepositoryInterface
{

    /**
    * This attribute is the model of repository
    * @var Modules\School\Diary
    */
    protected $modelClass;

    public function __construct(Diary $model)
    {
        $this->modelClass = $model;
    }
}
