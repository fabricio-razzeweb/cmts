<?php
namespace Modules\School\Diary;

use Illuminate\Http\Request;
use Modules\School\Diary\Diary;
use Modules\School\Diary\DiaryRepository;
use Illuminate\Routing\Controller as BaseController;
use Modules\DigitalMarketing\ViewCounter\ViewCounter;

class DiarySiteController extends BaseController
{
    protected $model;

    public function __construct(Diary $model,
                                DiaryRepository $repository,
                                ViewCounter $viewCounter)
    {
        $this->model = $model;
        $this->repository = $repository;
        $this->viewCounter = $viewCounter;
    }

    public function index(Request $request)
    {
        $events = $this->model->get();
        $this->viewCounter->add($request->path());
        return view('Diary::Site.index', compact('events'));
    }
}
