<?php
namespace Modules\School\Courses;

use Modules\Core\Support\Contracts\RepositoryInterface;
use Modules\Core\Support\BaseRepository;
use Modules\School\Courses\Courses;

class CoursesRepository extends BaseRepository implements RepositoryInterface
{

    /**
    * This attribute is the model of repository
    * @var Modules\School\Courses
    */
    protected $modelClass;

    public function __construct(Courses $model)
    {
        $this->modelClass = $model;
    }
}
