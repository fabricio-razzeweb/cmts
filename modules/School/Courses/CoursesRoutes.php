<?php

Route::group(['middleware'=>['web']],function()
{
    Route::group(['prefix'=>'admin/dashboard','middleware'=>['auth_common_users']],function ()
    {
        Route::group(['prefix'=>'api','middleware'=>['jwt.auth']],function(){
            Route::resource('courses',\Modules\School\Courses\CoursesController::class);
        });
    });

    Route::get("cursos","Modules\School\Courses\CoursesSiteController@index");
    Route::get("cursos/{title}/{id}","Modules\School\Courses\CoursesSiteController@show");
});
