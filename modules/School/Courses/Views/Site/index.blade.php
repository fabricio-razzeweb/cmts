@extends('WebSite::layouts.default')
@section('content')
    <ol class="breadcrumb">
        <li><a href="{{URL::asset('')}}">Início</a></li>
        <li><a href="#">Cursos</a></li>
    </ol>
    <section class="section section-default">
        <div class="section-title text-center">
            <h3 class="section-title-item">Cursos</h3>
        </div>
        @if (count($courses) > 0)
            @foreach ($courses as $course)
                @include('Courses::Site.card')
            @endforeach
        @else
            <h2>Opss! ainda não divulgamos nossos cursos</h2>
        @endif

    </section>
@endsection
