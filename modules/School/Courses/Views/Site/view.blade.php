@extends('WebSite::layouts.default')
@section('content')
    <ol class="breadcrumb">
        <li><a href="{{URL::asset('')}}">Início</a></li>
        <li><a href="{{URL::asset('cursos')}}">Cursos</a></li>
        <li><a href="#">{{$course->title}}</a></li>
    </ol>
    <div class="col-md-12">
        <div class="col-md-5 text-center">
            <img src="{{$course->photo}}" alt="{{$course->title}}" style="max-width:90%;">
        </div>
        <br><br>
        <div class="col-md-7 ">
            <h3>{{$course->title}}</h3> <br>
            <p>
                {!!$course->description!!}
            </p>
        </div>
    </div>
@endsection
