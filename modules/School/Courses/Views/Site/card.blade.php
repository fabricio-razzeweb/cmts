<div class="col-md-6" data-aos="fade-up" data-aos-duration="500">
    <a href="{{URL::asset('cursos/'.\Modules\Core\Helpers\TextAdapter::generateLink($course->title)."/".$course->id)}}" class="card card-post">
        <div class="col-md-4">
            <div class="card-post-img">
                <img src="{{URL::asset($course->photo)}}"/>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card-post-info">
                <div class="col-xs-2">
                    <div class="card-post-data">
                        <span class="card-post-day">{{$course->created_at->format("d")}}</span>
                        <span class="card-post-month">{{$course->created_at->format("M")}}</span>
                    </div>
                </div>
                <div class="col-xs-10">
                    <div class="card-post-title">
                        <h3>{{$course->title}}</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-post-body">
            <p>
                {!!substr(strip_tags($course->description), 0, 120)!!}
            </p>
            <div class="ellipsis"></div>
        </div>
    </a>
</div>
