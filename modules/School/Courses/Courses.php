<?php
namespace Modules\School\Courses;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Modules\Core\Support\PrepareDataToSave;
use Modules\Core\Support\SavePhoto;

class Courses extends Model
{
    use SoftDeletes, SavePhoto;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'title',
      'description',
      'photo',
      'keywords',
      'users_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $resourcesPath = "src/imgs/courses/";

    public function getPhotoAttribute($value)
    {
        return env('APP_URL').'/src/imgs/courses/'.$value;
    }

    public function prepareDataToSave(array $data)
    {
        if (isset($data['photo'])) {
             $data['photo'] = $this->savePhoto($data['photo'], $data['title'], 400, 600);
        }

        return $this->fill($data);
    }
}
