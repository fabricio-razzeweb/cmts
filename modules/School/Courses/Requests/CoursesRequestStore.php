<?php
namespace Modules\School\Courses\Requests;

use Modules\Core\Http\JWTRequest as JWTRequest;

class CoursesRequestStore extends JWTRequest
{

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        return [
            'title' => 'required|max:80|min:5',
            'description' => 'required|min:5',
            'photo' => 'required|image',
            'users_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'users_id.required'=>"Você precisa fazer login para executar está ação",
            'title.min' => "O título tem que ter no mínimo 5 caracteres",
            'title.max' => "O título tem que ter no mínimo 280 caracteres",
            'title.required' => "Preencha o campo de Nome",
            'description.required' => "A descrição do curso é obrigatória",
            'description.max' => "O campo descrição tem o número máximo é de 240 caracteres",
            'description.min' => "O campo descrição tem o número minimo é de 5 caracteres",
            'photo.required' => "Adicione a foto do curso",
            'photo.image' => "A foto tem que ter o formato png ou jpeg"
        ];
    }

}
