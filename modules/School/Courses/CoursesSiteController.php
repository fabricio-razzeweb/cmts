<?php
namespace Modules\School\Courses;

use Illuminate\Http\Request;
use Modules\School\Courses\Courses;
use Modules\School\Courses\CoursesRepository;
use Illuminate\Routing\Controller as BaseController;
use Modules\DigitalMarketing\ViewCounter\ViewCounter;

class CoursesSiteController extends BaseController
{
    protected $model;

    public function __construct(Courses $model,
                                CoursesRepository $repository,
                                ViewCounter $viewCounter)
    {
        $this->model = $model;
        $this->repository = $repository;
        $this->viewCounter = $viewCounter;
    }

    public function index(Request $request)
    {
        $courses = $this->model->get();
        $this->viewCounter->add($request->path());
        return view('Courses::Site.index', compact('courses'));
    }

    public function show(Request $request, $title, $id)
    {
        $course = $this->model->find($id);
        $this->viewCounter->add($request->path(), $id);
        $this->viewCounter->count($request->path(), $id);

        return view('Courses::Site.view', compact('course'));
    }
}
