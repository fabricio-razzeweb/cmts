<?php
namespace Modules\School\Courses;

use Illuminate\Http\Request;
use Modules\School\Courses\Courses;
use Modules\Core\Support\RestAPI\BaseControllerRestAPI;
use Modules\School\Courses\CoursesRepository;
use Modules\School\Courses\Requests\CoursesRequestStore;
use Modules\School\Courses\Requests\CoursesRequestUpdate;

class CoursesController extends BaseControllerRestAPI
{
    protected $model;
    protected $nameElementUI = "Cursos";
    protected $resourceName = "course";
    protected $resourceGroupName = "courses";

    public function __construct(Courses $model, CoursesRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    public function store(CoursesRequestStore $request)
    {
        return parent::genericStore($request);
    }

    public function update(CoursesRequestUpdate $request, int $id)
    {
        return parent::genericUpdate($request, $id);
    }
}
