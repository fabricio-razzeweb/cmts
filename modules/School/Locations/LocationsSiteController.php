<?php
namespace Modules\School\Locations;

use Illuminate\Http\Request;
use Modules\School\Locations\Locations;
use Modules\School\Locations\LocationsRepository;
use Illuminate\Routing\Controller as BaseController;
use Modules\DigitalMarketing\ViewCounter\ViewCounter;

class LocationsSiteController extends BaseController
{
    protected $model;

    public function __construct(Locations $model,
                                LocationsRepository $repository,
                                ViewCounter $viewCounter)
    {
        $this->model = $model;
        $this->repository = $repository;
        $this->viewCounter = $viewCounter;
    }

    public function index(Request $request)
    {
        $locations = $this->model->get();
        $this->viewCounter->add($request->path());
        return view('Locations::Site.index', compact('locations'));
    }
}
