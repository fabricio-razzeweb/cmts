<div class="col-md-4 item" data-aos="zoom-in" data-aos-duration="1000">
    <div class="item-description text-center">
        <img class="col-md-12" src="{{URL::asset('src/layout/icons/icon-location-simple.png')}}" alt="">
        <h3>{{$location->name}}</h3>
        <p class="content">
            <p class="text-center">
                {{$location->description}}
            </p>
        </p>
    </div>
</div>
