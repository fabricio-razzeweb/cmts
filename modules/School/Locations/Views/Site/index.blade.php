@extends('WebSite::layouts.default')
@section('content')
    <ol class="breadcrumb">
        <li><a href="{{URL::asset('')}}">Início</a></li>
        <li><a href="#">Intalações</a></li>
    </ol>
    <section class="section section-default">
        <div class="section-title text-center">
            <h3 class="section-title-item">Instalações</h3>
        </div>

        @if (count($locations) > 0)
            @foreach ($locations as $location)
                @include('Locations::Site.card')
            @endforeach
        @else
            <h2>Opss! ainda não divulgamos nossas instalações</h2>
        @endif

    </section>
@endsection
