<?php
namespace Modules\School\Locations;

use Illuminate\Http\Request;
use Modules\School\Locations\Locations;
use Modules\Core\Support\RestAPI\BaseControllerRestAPI;
use Modules\School\Locations\LocationsRepository;
use Modules\School\Locations\Requests\LocationsRequestStore;
use Modules\School\Locations\Requests\LocationsRequestUpdate;

class LocationsController extends BaseControllerRestAPI
{
    protected $model;
    protected $nameElementUI = "Localizações";
    protected $resourceName = "location";
    protected $resourceGroupName = "locations";

    public function __construct(Locations $model, LocationsRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    public function store(LocationsRequestStore $request)
    {
        return parent::genericStore($request);
    }

    public function update(LocationsRequestUpdate $request, int $id)
    {
        return parent::genericUpdate($request, $id);
    }
}
