<?php
namespace Modules\School\Locations\Requests;

use Modules\Core\Http\JWTRequest as JWTRequest;

class LocationsRequestUpdate extends JWTRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'description' => 'required|max:80|min:5',
          'users_id' => 'required',
        ];
    }

    public function messages()
    {
      return [
        'users_id.required'=>"Você precisa fazer login para executar está ação",
        'description.min' => 'Preencha o campo de Descrição',
        'description.required' => 'Preencha o campo de Descrição',
      ];
    }

}
