<?php

Route::group(['middleware'=>['web']],function()
{
    Route::group(['prefix'=>'admin/dashboard','middleware'=>['auth_common_users']],function ()
    {
        Route::group(['prefix'=>'api','middleware'=>['jwt.auth']],function(){
            Route::resource('locations',\Modules\School\Locations\LocationsController::class);
        });
    });
    Route::get("instalacoes","Modules\School\Locations\LocationsSiteController@index");
});
