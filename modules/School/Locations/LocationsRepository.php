<?php
namespace Modules\School\Locations;

use Modules\Core\Support\Contracts\RepositoryInterface;
use Modules\Core\Support\BaseRepository;
use Modules\School\Locations\Locations;

class LocationsRepository extends BaseRepository implements RepositoryInterface
{

    /**
    * This attribute is the model of repository
    * @var Modules\School\Locations
    */
    protected $modelClass;

    public function __construct(Locations $model)
    {
        $this->modelClass = $model;
    }
}
