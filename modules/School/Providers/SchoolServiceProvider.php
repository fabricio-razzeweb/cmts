<?php
namespace Modules\School\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class SchoolServiceProvider extends ServiceProvider
{
    /**
    * The event listener mappings for the application.
    *
    * @var array
    */
    protected $listen = [];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $subModulesList = [
            'Courses',
            'Diary',
            'Locations'
        ];

        foreach ($subModulesList as $module) {
            $this->loadRoutesFrom(__DIR__.'/../'.$module.'/'.$module.'Routes.php');
            $this->loadMigrationsFrom(__DIR__.'/../'.$module.'/Migrations/');
            $this->loadViewsFrom(__DIR__.'/../'.$module.'/Views/', $module);
        }
    }
}
