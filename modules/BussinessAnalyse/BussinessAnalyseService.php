<?php
namespace Modules\BussinessAnalyse;
use DB as DB;
use Carbon\Carbon;

class BussinessAnalyseService
{
    protected $beginDate;
    protected $endDate;

    public function setBeginDate(Carbon $date)
    {
        $this->beginDate = $date;

        return $this;
    }

    public function setEndDate(Carbon $date)
    {
        $this->endDate = $date;

        return $this;
    }

    protected function analayseDataOfTable($table, $conditionals = null)
    {
        $beginDate = clone $this->beginDate;
        $endDate = clone $this->endDate;
        $daysPeriodSize = $endDate->diffInDays($beginDate);

        $dataOfAnalyse = [];
        $dataOfAnalyse['labels'] = [];
        $dataOfAnalyse['data'] = [];

        for ($currentDayForAnalyse = 0;
            $currentDayForAnalyse <= $daysPeriodSize;
            $currentDayForAnalyse++) {

                $currentDateInAnalyse = $beginDate->toDateString();

                $dataOfAnalyse['labels'][] = $beginDate->format('d/m/Y');

                $query = DB::table($table);

                if ($conditionals) {
                    $query = $conditionals($query);
                }

                $dataOfAnalyse['data'][] = $query
                ->whereDate('created_at', $currentDateInAnalyse)
                ->count();

                $beginDate->addDay();
        }

        return $dataOfAnalyse;
    }

    public function getViewsOfPostsForDate()
    {
        return $this->analayseDataOfTable('posts_views');
    }

    public function getAllResultsOfAnalyse()
    {
        $dataOfResults = [];
        $dataOfResults['viewsOfPosts'] = $this->getViewsOfPostsForDate();

        return $dataOfResults;
    }
}
