<?php
namespace Modules\BussinessAnalyse;

use Illuminate\Http\Request;
use Modules\Core\Http\Controller as Controller;
use Carbon\Carbon;
use Modules\BussinessAnalyse\BussinessAnalyseService;

class BussinessAnalyseController extends Controller
{

    public function __construct(BussinessAnalyseService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        if ($request->has('date_begin') && $request->has('date_end')) {
            $dateBegin = Carbon::createFromFormat('d/m/Y', $request->get('date_begin'));
            $dateEnd = Carbon::createFromFormat('d/m/Y', $request->get('date_end'));
        } else {
            $dateBegin = Carbon::now()->subDays(30);
            $dateEnd = Carbon::now();
        }

        $resultsOfAnalyse = $this->service
        ->setBeginDate($dateBegin)
        ->setEndDate($dateEnd)
        ->getAllResultsOfAnalyse();

        return response()->json(compact('resultsOfAnalyse'));
    }

}
