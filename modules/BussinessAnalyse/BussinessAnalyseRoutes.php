<?php

Route::group(['prefix' => 'admin/dashboard/api', 'middleware' => ['jwt.auth']], function(){
    Route::resource('bussiness_analyse', 'Modules\BussinessAnalyse\BussinessAnalyseController');
});
