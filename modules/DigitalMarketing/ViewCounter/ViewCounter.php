<?php

namespace Modules\DigitalMarketing\ViewCounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class ViewCounter extends Model
{

    private function getPageByPath($path) {
        $page_names = config('counter_page_names');
        $page = $path !== '/'? $page_names[explode('/', $path)[0]]
                             : $page_names[$path];

        return $page;
    }

    public function add($path, $id = null)
    {

        $page = self::getPageByPath($path);

        DB::table('views')->insert([
            'page' => $page,
            'page_id' => $id,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        return $this;
    }

    public function count($path, $id = null)
    {

        $page = self::getPageByPath($path);
        $return;

        if ($id) {
            $return = DB::table('views')->where([
                ['page', '=', $page],
                ['page_id', '=', $id]
            ])->count();
        } else {
            $return = DB::table('views')->where('page', $page)->count();
        }

        return $return;
    }
}
