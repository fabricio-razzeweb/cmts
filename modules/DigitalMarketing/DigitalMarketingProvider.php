<?php

namespace Modules\DigitalMarketing;

use Illuminate\Support\ServiceProvider;

class DigitalMarketingProvider extends ServiceProvider
{

    /**
    * Bootstrap the application services.
    *
    * @return void
    */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__."/ViewCounter/Migrations/");
    }

    /**
    * Register the application services.
    *
    * @return void
    */
    public function register()
    {
        //
    }
}
