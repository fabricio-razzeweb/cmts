<?php

namespace Modules\Messages;

use Modules\Messages\Requests\MessagesRequestStore;
use Modules\Messages\Requests\MessagesAnswerRequest;
use Illuminate\Http\Request;
use Modules\Messages\Messages;
use Modules\Core\Http\Controller as Controller;

class MessagesController extends Controller
{
    protected $nameElementUI="Mensagem";
    protected $table="messages";
    protected $model;

    public function __construct()
    {
        $this->model=new Messages;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        // If request is one search?
        if($request->get('search')){
            //Cath search
            $search = $request->get('search');
            if($request->get('search')=="*"){
                // Search all itens
                $Messages=$this->model->get();

            }else{

                // Search object in database
                $Messages = $this->model->where("name", "LIKE", "%{$search}%")->get();
            }
        }else{
            // If normal request return itens paginate for id
            $Messages = $this->model->orderBy('id','desc')->paginate(15);
        }
        // Return response for client
        return response()->json(compact('Messages'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\MessagesRequest   $request
    * @return \Illuminate\Http\Response
    */
    public function store(MessagesRequestStore  $request)
    {

        //This is data for transfer
        $elementData = $this->model->prepareDataForTransfer($request);

        //Calling MODEL and insert data in the tables
        $executionStatus = $elementData->save();

        //event(new \Modules\Messages\Events\SendMessageToSiteAdm($request->all()));

        if ($executionStatus) {
            $msg = "$this->nameElementUI, enviada com Sucesso!";
        }else{
            $msg = "Ocorreu um erro ao executar está operação";
        }

        return  redirect()
                ->back()
                ->with(['alertMsg' => $msg]);
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $countAwnser = $this->model->where('messages.id',$id)
        ->select('messages.*','messages_answer.answer as answer')
        ->join('messages_answer','messages.id','=','messages_answer.messages_id')
        ->count();

        if ($countAwnser>0) {
            $element = $this->model->where('messages.id',$id)
            ->select('messages.*','messages_answer.answer as answer')
            ->join('messages_answer','messages.id','=','messages_answer.messages_id')
            ->get();
        }else{
            $element = $this->model->where('messages.id',$id)
            ->select('messages.*')
            ->get();
        }


        return response()->json(compact('element'));
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
        return $this->fethOne($id);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\MessagesRequest   $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(MessagesAnswerRequest $request, $id)
    {
        $execution = [];

        $message = $this->model->find($id)->saveAnswer($request->answer);

        if ($message) {
            $execution['status'] = true;
            $execution['msg'] = "Mensagem enviada com sucesso";
        }else{
            $execution['status'] = false;
            $execution['msg'] = "Ocorreu um erro ao enviar sua mensagem";
        }

        event(new \Modules\Messages\Events\SendMessageToSiteUser($message->email, $request->answer));

        return response()->json(compact('execution'));
    }



    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        // Delete element of DB
        return parent::delete($id);
    }
}
