<?php
namespace Modules\Messages\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use URL;
use Modules\Mails\MailsLayoutConfig;

class MessageToSiteAdmMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $content;
    /**
    * Create a new message instance.
    *
    * @return void
    */
    public function __construct($content)
    {
        $config = new MailsLayoutConfig;
        $config = $config->content;

        $this->content = array_merge($content,$config);
    }

    /**
    * Build the message.
    *
    * @return $this
    */
    public function build()
    {
        return $this
            ->subject("Chegou uma mensagem no site!")
            ->view('Mails::layouts.messageToSite')
            ->with($this->content);
    }
}
