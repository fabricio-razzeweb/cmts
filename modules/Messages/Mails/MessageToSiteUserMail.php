<?php
namespace Modules\Messages\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use URL;
use Modules\Mails\MailsLayoutConfig;

class MessageToSiteUserMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $content;
    /**
    * Create a new message instance.
    *
    * @return void
    */
    public function __construct($content)
    {
        $config = new MailsLayoutConfig;
        $this->content = $config->content;
        $this->content['content'] = $content;
    }

    /**
    * Build the message.
    *
    * @return $this
    */
    public function build()
    {
        return $this
            ->subject(env("APP_NAME")." - RESPOSTA A SUA MENSAGEM")
            ->view('Mails::layouts.messageToUser')
            ->with($this->content);
    }
}
