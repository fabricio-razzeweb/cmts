<?php

namespace Modules\Messages\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Messages\Events\SendMessageToSiteAdm;
use Modules\Messages\Listeners\SendEmailForAdm;
use Modules\Messages\Events\SendMessageToSiteUser;
use Modules\Messages\Listeners\SendEmailForUser;

class EventServiceProvider extends ServiceProvider
{
    /**
    * The event listener mappings for the application.
    *
    * @var array
    */
    protected $listen = [
        SendMessageToSiteAdm::class => [
            SendEmailForAdm::class,
        ],
        SendMessageToSiteUser::class => [
            SendEmailForUser::class,
        ],

    ];

    /**
    * Register any events for your application.
    *
    * @return void
    */
    public function boot()
    {
        parent::boot();
    }
}
