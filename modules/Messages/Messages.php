<?php
namespace Modules\Messages;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Messages extends Model
{
    use SoftDeletes;
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'name',
        'phone',
        'subject',
        'email',
        'content',
    ];

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [];


    /**
    * Method prepareDataForTransfer()
    * This method prepare data for write and save of data
    * @param $request this is request of user (Object soon of Illuminate/Requests)
    * @return $element whith atributes necessary of writing in database
    */

    public function prepareDataForTransfer($request)
    {

        $fields = $this->fillable;

        for ($i=0; $i < count($fields); $i++) {
            $field = $fields[$i];
            $this->$field = $request->input("$field");
        }

        return $this;

    }

    public function saveAnswer($answer)
    {
        $executionStatus = DB::table('messages_answer')
                            ->insert(['messages_id' => $this->id,
                                        'answer' => $answer]);
        if ($executionStatus) {
            return $this;
        }else{
            return false;
        }
    }

}
