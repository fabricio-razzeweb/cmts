<?php
namespace Modules\Messages\Listeners;

use Modules\Messages\Events\SendMessageToSiteUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use URL;
use Mail;

class SendEmailForUser
{

    /**
    * Handle the event.
    *
    * @param  OrderShipped  $event
    * @return void
    */
    public function handle(SendMessageToSiteUser $event)
    {
        try {

            $mail = Mail::to($event->email)
                        ->send(new \Modules\Messages\Mails\MessageToSiteUserMail($event->message));
            if ($mail==null) {
                return true;
            }else{
                return false;
            }

        } catch (Exception $e) {
            return back()
            ->with(['error',$e]);
        }

    }
}
