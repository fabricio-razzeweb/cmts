<?php
namespace Modules\Messages\Listeners;

use Modules\Messages\Events\SendMessageToSiteAdm;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use URL;
use Mail;

class SendEmailForAdm
{

    /**
    * Handle the event.
    *
    * @param  OrderShipped  $event
    * @return void
    */
    public function handle(SendMessageToSiteAdm $event)
    {
        try {

            $mail = Mail::to(env('EMAIL_REVEICER_SITE_MESSAGES'))
                        ->send(new \Modules\Messages\Mails\MessageToSiteAdmMail($event->message));
            if ($mail==null) {
                return true;
            }else{
                return false;
            }

        } catch (Exception $e) {
            return back()
            ->with(['error',$e]);
        }

    }
}
