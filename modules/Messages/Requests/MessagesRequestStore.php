<?php
namespace Modules\Messages\Requests;

use Modules\Core\Http\JWTRequest as JWTRequest;

class MessagesRequestStore extends JWTRequest
{


    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        return [
            'name' => 'required|max:80',
            'subject' => 'required|max:180',
            'phone' => 'required|max:24',
            'email' => 'required|max:120',
            'content' => 'required',
        ];
    }

    public function messages()
    {
        return [];
    }

}



?>
