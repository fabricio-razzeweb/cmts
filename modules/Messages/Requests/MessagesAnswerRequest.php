<?php
namespace Modules\Messages\Requests;

use Modules\Core\Http\JWTRequest as JWTRequest;

class MessagesAnswerRequest extends JWTRequest
{


    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        return [
            'id' => 'required|integer',
            'answer' => 'required|max:500',
        ];
    }

    public function messages()
    {
        return [];
    }

}



?>
