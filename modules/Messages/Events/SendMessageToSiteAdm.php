<?php
namespace Modules\Messages\Events;

use Illuminate\Queue\SerializesModels;

class SendMessageToSiteAdm
{
    use SerializesModels;

    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

}
