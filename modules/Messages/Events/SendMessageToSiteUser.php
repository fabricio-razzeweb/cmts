<?php
namespace Modules\Messages\Events;

use Illuminate\Queue\SerializesModels;

class SendMessageToSiteUser
{
    use SerializesModels;

    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($email, $message)
    {
        $this->email = $email;
        $this->message = $message;
    }
}
