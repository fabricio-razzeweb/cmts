<?php
namespace Modules\Users\Tests;
use Tests\TestCase;

class UsersSocialMediaResgisterTest extends TestCase
{
    public function testIfRegisterUser()
    {
        $data = [
            'name' => 'Fualno teste Social',
            'password' => '88114527',
            'email' => "fabriciosena".rand(1,99999)."@gmail.com",
            'sex' => 'M',
            'phone' => '1(077) 98811-452',
            'date_birth' => '1997-01-08',
            'token' => '2347890263479362387946225',
            'platform' => 'android',
            'api_type' =>  'facebook',
            'api_id' =>  '12345678'
        ];

        $this->post('api/users/register/social', $data)
        ->assertStatus(200)
        ->assertJsonStructure([
            'execution' => ['status']
        ])
        ->assertJson([
            'execution' => ['status' => true]
        ]);
    }
}
