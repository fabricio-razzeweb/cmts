<?php
namespace Modules\Users\Tests;
use Tests\TestCase;

class UsersTest extends TestCase
{
    public function testIfRegisterUser()
    {
        $data = [
            'name' => 'Fualno teste',
            'password' => '88114527',
            'email' => "fabriciosena".rand(1,99999)."@gmail.com",
            'sex' => 'M',
            'phone' => '(077) 98811-4527',
            'date_birth' => '1997-01-08'
        ];

        $this->post('api/users/register', $data)
        ->assertStatus(200)
        ->assertJsonStructure([
            'execution' => ['status', 'msg']
        ])
        ->assertJson([
            'execution' => ['status' => true]
        ]);
    }
}
