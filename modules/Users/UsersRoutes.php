<?php
// APP ROUTES
Route::group(['prefix'=>'api', 'middleware'=>['api']], function()
{
    Route::post('users/register', "Modules\Users\UsersRegisterController@store");
    Route::post('users/register/social', "Modules\Users\UsersAppSocialMediaRegisterController@store");
    Route::post('users/register/social/check', "Modules\Users\UsersAppSocialMediaRegisterController@checkIfSocialUserIsRegistred");
    Route::post('users/check/email', "Modules\Users\UsersRegisterController@checkEmailIsValid");
    Route::post('users/check/phone', "Modules\Users\UsersRegisterController@checkPhoneIsValid");

    Route::post('recovery/password/call',['as'=>'user.recoveryPassword','uses'=>'\Modules\Users\UserPasswordRecoveryController@beginProccess']);
});

Route::group(['middleware'=>['web']], function()
{
    Route::get('recuperar/senha',['uses'=>'\Modules\Users\UserPasswordRecoveryController@showRecoveryPasswordPage']);
    Route::get('cadastro/confirmar',['uses'=>'\Modules\Users\UsersRegisterConfirmationController@confirm']);
    Route::post('recovery/password/change',['as'=>'user.passwordChange','uses'=>'\Modules\Users\UserPasswordRecoveryController@changePasswordOfUser']);
});
