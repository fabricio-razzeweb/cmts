<?php
namespace Modules\Users;

use Auth;
use Modules\Users\Users;
use Illuminate\Http\Request;
use App\Http\Requests;
use Modules\Core\Http\Controller as Controller;
use DB;
use Carbon\Carbon;

class UserPasswordRecoveryController extends Controller
{

    public function beginProccess(Request $request)
    {
        $execution = [];

        $user = new Users;
        $userRequired = $user
        ->where('email', trim(strtolower($request->email)))
        ->first();

        if (count($userRequired)>0) {

            $hash = md5(date('Y-m-d H:i:s').rand(1, 999999999));
            $dateExpired = Carbon::now()->addMinutes(15);

            $execution['status'] = DB::table('password_recovery')
            ->insert([
                'users_id' => $userRequired->id,
                'hash' => $hash,
                'expired_at' => $dateExpired->format('Y-m-d H:i:s')
            ]);

            event(new \Modules\Users\Events\RecoveryPasswordEvent($userRequired, $hash));

            if ($execution['status']) {
                $execution['message'] = "Enviamos para o seu email um link para
                                            mudar sua senha, ele é valido por
                                            15 minutos";
            }else{
                $execution['message'] = "Desculpe ocorreu um erro ao processar
                                        está atividade, por favor entre em contato
                                        com nossa equipe";
            }

        }else{
            $execution['message'] = "Opss...este email não está cadastrado
                                        em nosso sistema";
        }

        return response()->json(compact('execution'));
    }

    protected function verifyHash($hash)
    {
        $executionStatus = DB::table('password_recovery')
        ->where('hash', $hash)
        ->where('expired_at', '>=', date('Y-m-d H:i:s'))
        ->count();

        return $executionStatus;
    }

    protected function getUser($hash)
    {
        $permissionToRecovery = DB::table('password_recovery')
        ->where('hash', $hash)
        ->where('expired_at', '>=', date('Y-m-d H:i:s'))
        ->first();

        $user = new Users;

        return $user->find($permissionToRecovery->users_id);
    }

    public function showRecoveryPasswordPage(Request $request)
    {
        $execution = [];

        $hash = $request->h;

        if ($this->verifyHash($hash)>0) {

            $userId = $this->getUser($hash)->id;

            $execution['userId'] = $userId;
            $execution['hash'] = $hash;
            $execution['showRevoveryPassWordFormModal'] = $hash;
        }else{
            $execution['alertMsg'] = "Desculpe este pedido de recuperação
                                        de senha já expirou";
        }

        return redirect(env('APP_URL'))
        ->with($execution);
    }

    public function changePasswordOfUser(Request $request)
    {
        $execution = [];

        $hash = $request->hash;

        if ($this->verifyHash($hash)) {

            $user = new Users;

            $user = $this->getUser($hash);

            $user->password = bcrypt($request->password);

            $executionStatus = $user->save();

            if ($executionStatus) {
                $execution['alertMsg'] = "Senha alterada com sucesso!";
            }else{
                $execution['alertMsg'] = "Ocorreu um erro ao alterar sua senha";
            }

        }else{
            $execution['alertMsg'] = "Desculpe este pedido de
                                        recuperação de senha já expirou";
        }

        return redirect(env('APP_URL'))
        ->with($execution);
    }

}
