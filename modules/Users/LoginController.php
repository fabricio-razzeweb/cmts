<?php

namespace Modules\Users;

use Illuminate\Http\Request;
use Modules\Users\Users;
use Modules\Core\Http\Controller as Controller;
use Illuminate\Support\Facades\Auth;
use Cookie;

class LoginController extends Controller
{

    protected $authGuard = "web";

    public function __construct()
    {
        $this->model = new Users;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if (Auth::guard($this->authGuard)->check()) {
            $baseUrl = Users::USER_LOGIN_ROUTES[Auth::user()->users_types_id];

            return redirect()->intended($baseUrl.'/dashboard');
        }else{
            return view('Users::login');
        }

    }

    public function auth(Request $request)
    {
        $remember = ($request->is_remember == "on")? true : false;

        $credentials = ['email' => $request->email,
                        'password' => $request->password];

        $authorization = Auth::guard($this->authGuard)
        ->attempt($credentials,$remember);

        if ($authorization) {
            $baseUrl = Users::USER_LOGIN_ROUTES[Auth::user()->users_types_id];

            return redirect()->intended($baseUrl.'/dashboard');
        }

        return back()
        ->with('authError', 'Email ou senha incorretos.')
        ->withInput($request->except('password'));

    }

    public function logout()
    {

        $cookiesForForgetInLogout = ["userName",
                                    'userId',
                                    'tk',
                                    'userPhoto'];

        foreach ($cookiesForForgetInLogout as $cookieForForget) {
            Cookie::forget($cookieForForget);
        }

        $baseUrl = Users::USER_LOGIN_ROUTES[Auth::user()->users_types_id];

        Auth::guard($this->authGuard)->logout();

        return redirect()->intended($baseUrl.'/dashboard');
    }

}
