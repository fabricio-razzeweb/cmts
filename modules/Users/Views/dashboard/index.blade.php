<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <title>{{env('APP_NAME')}} - Admin</title>
    @include('Users::dashboard.Components.basicResources')
  </head>
  <body id="users-admin" class="hold-transition skin-black fixed sidebar-mini" ng-app="dashboard">
    <div ng-view></div>
    @include('Users::dashboard.Components.preloader')
    @include('Users::dashboard.Components.alertModal')
    @include('Users::dashboard.Components.basicScripts')
  </body>
</html>
