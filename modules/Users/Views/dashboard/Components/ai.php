<script type="text/javascript">
function AIDiana() {

    this.recognizer = {};

    this.talks = function (text) {
        this.speechText = new SpeechSynthesisUtterance();
        this.speechText.lang = 'pt-br';
        this.speechText.rate = 1.10;
        this.speechText.text = text;
        window.speechSynthesis.speak(this.speechText);
    }

    this.verifyRecognition = function  () {
        window.SpeechRecognition = window.SpeechRecognition ||
        window.webkitSpeechRecognition || window.mozSpeechRecognition ||
        null || window.msSpeechRecognition;

        return window.SpeechRecognition;
    }

    this.setRecognizer = function () {
        this.recognizer = new window.SpeechRecognition();
        this.recognizer.continuous = false;
    }

    this.verifyInCommands = function (commands, wordsTalks) {
        for (let command in commands) {
            console.log(wordsTalks, command);
            if (command == wordsTalks) {
                console.log(commands[command]);
                return commands[command]();
            }
        }
    }

    this.listen = function (commands) {
        if (this.recognizer) {
            var AIDiana = this;

            this.recognizer.onresult = function(event){
                for (var i = event.resultIndex; i < event.results.length; i++) {
                    AIDiana.verifyInCommands(commands, event.results[i][0].transcript);
                }
            }
            console.log(this.recognizer);
            this.recognizer.start();
        }

    }

    if (this.verifyRecognition()) {
        this.setRecognizer();
    }

    return this;
};
var AITalks = new AIDiana();
</script>
