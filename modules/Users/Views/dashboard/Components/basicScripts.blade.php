<div class="scripts">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
    <!--DIRECTIVES AND DIRECTIVES INTERFACES-->
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/admins/app.js', env('SSL')) }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/directives/fileModel.directive.js', env('SSL')) }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/interface/directives/ui-header.directive.js', env('SSL')) }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/interface/directives/ui-nav.directive.js', env('SSL')) }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/interface/directives/ui-paginator.directive.js', env('SSL')) }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/interface/directives/ui-alertFormDanger.directive.js', env('SSL')) }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/interface/directives/ui-footer.directive.js', env('SSL')) }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/interface/directives/ui-defaultInput.directive.js', env('SSL')) }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/interface/directives/ui-kitButtonsFormPost.directive.js', env('SSL')) }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/interface/directives/ui-kitButtonsFormPut.directive.js', env('SSL')) }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/interface/directives/ui-kitButtonsTopList.directive.js', env('SSL')) }}"></script>
    <!--SERVICES AND CONTROLLERS-->
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/helpers/preloader.js', env('SSL')) }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/helpers/listReader.js', env('SSL')) }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/helpers/alerter.js', env('SSL')) }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/helpers/deleter.js', env('SSL')) }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/helpers/groupReader.js', env('SSL')) }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/analyser/analyserRequestService.js', env('SSL')) }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/auth/authService.js', env('SSL')) }}"></script>
    <?php

        $modulesJS = [
            "home",
            "categorys",
            "users",
            "messages",
            "posts",
            "myEnterprise",
            "slides",
            "courses",
            "locations",
            "diary"
        ];

        for ($i=0; $i <= sizeof($modulesJS)-1; $i++) {
            echo "	<script type='text/javascript' src='".URL::asset("/assets/admin/app/admins/modules/".$modulesJS[$i]."/".$modulesJS[$i]."APIService.js",env('SSL'))."'></script>
            <script type='text/javascript' src='".URL::asset("/assets/admin/app/admins/modules/".$modulesJS[$i]."/".$modulesJS[$i]."Controller.js",env('SSL'))."'></script>
            <script type='text/javascript' src='".URL::asset("/assets/admin/app/admins/modules/".$modulesJS[$i]."/".$modulesJS[$i]."Routes.js",env('SSL'))."'></script>";
        }
    ?>
    <script type='text/javascript' src='{{URL::asset("/assets/admin/app/core/modules/myAccount/myAccountAPIService.js")}}'></script>
    <script type='text/javascript' src='{{URL::asset("/assets/admin/app/core/modules/myAccount/myAccountController.js")}}'></script>
    <script type='text/javascript' src='{{URL::asset("/assets/admin/app/core/modules/myAccount/myAccountRoutes.js")}}'></script>

    <!--ROUTE CONFIG-->
    <script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/config/routeConfig.js', env('SSL')) }}"></script>


    <script src="{{ URL::asset('assets/admin/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>

    <script src="{{ URL::asset('assets/admin/dist/js/app.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('libs\dropzone\dropzone.min.js') }}"> </script>
    <script type="text/javascript" src="{{ URL::asset('libs\dropzone\ng-dropzone.js') }}"> </script>
</div>
