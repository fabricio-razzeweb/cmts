<script type="text/javascript">
    window.localStorage.clear();
    window.localStorage.setItem('username',"<?=$userName?>")
    window.localStorage.setItem('userPhoto',"<?=$userPhoto?>")
    window.localStorage.setItem('userId',"<?=$userId?>");
    window.localStorage.setItem('tk',"<?=$tk?>");
    window.localStorage.setItem('userHash',"<?=$userHash?>");
</script>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Theme style -->
<link rel="stylesheet" href="{{ URL::asset('assets/admin/dist/css/AdminLTE.min.css', env('SSL')) }}">
<link rel="stylesheet" href="{{ URL::asset('assets/admin/dist/css/style-admin.css', env('SSL')) }}">
<!-- admin Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{ URL::asset('assets/admin/dist/css/skins/_all-skins.min.css', env('SSL')) }}">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">var CSRFPROTECTION = "{{ csrf_token() }}";</script>
<link rel="stylesheet" type="text/css" href="{{ URL::asset('libs/normalize/normalize.css', env('SSL')) }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('libs/bootstrap/css/bootstrap.min.css', env('SSL')) }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('libs/font-awesome/css/font-awesome.min.css', env('SSL')) }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('libs/dropzone/basic.min.css', env('SSL')) }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('libs/dropzone/dropzone.min.css', env('SSL')) }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('libs/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.min.css', env('SSL')) }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('libs/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css', env('SSL')) }}">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<!--LIBRARIES-->
<script type="text/javascript" src="{{ URL::asset('libs/jquery/jquery.js', env('SSL')) }}"></script>
<script type="text/javascript" src="{{ URL::asset('libs/jcrypt/jcrypt.js', env('SSL')) }}"></script>
<script type="text/javascript" src="{{ URL::asset('libs/jquery/jquery-ui.min.js', env('SSL')) }}"></script>
<script type="text/javascript" src="{{ URL::asset('libs/bootstrap/js/bootstrap.min.js', env('SSL')) }}"></script>
<script type="text/javascript" src="{{ URL::asset('libs\moment\min\moment.min.js', env('SSL')) }}"></script>
<script type="text/javascript" src="{{ URL::asset('libs\moment\locale\pt-br.js', env('SSL')) }}"></script>
<script type="text/javascript" src="{{ URL::asset('libs/angular/angular.js', env('SSL')) }}"></script>
<script type="text/javascript" src="{{ URL::asset('libs/dropzone/dropzone.min.js', env('SSL')) }}"></script>
<script type="text/javascript" src="{{ URL::asset('libs/angular/angular-sanitize.js', env('SSL')) }}"></script>
<script type="text/javascript" src="{{ URL::asset('libs/angular/angular-route.js', env('SSL')) }}"></script>
<script type="text/javascript" src="{{ URL::asset('libs/angular/angular-locale-pt-br.js', env('SSL')) }}"></script>
<script type="text/javascript" src="{{ URL::asset('libs\oclazyload\dist\ocLazyLoad.min.js', env('SSL')) }}"></script>
<script type="text/javascript" src="{{ URL::asset('libs/ng-mask/dist/ng-mask.js', env('SSL')) }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/extensions/textareaEditor.js', env('SSL')) }}"></script>
<script type="text/javascript" src="{{ URL::asset('libs/textarea_editor/tinymce.min.js', env('SSL')) }}"></script>
<script type="text/javascript" src="{{ URL::asset('libs/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js', env('SSL')) }}" charset="utf-8"></script>
<script type="text/javascript" src="{{ URL::asset('libs/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.js', env('SSL')) }}"></script>
<script type="text/javascript" src="{{ URL::asset('libs/angular-bootstrap-datetimepicker-directive/angular-bootstrap-datetimepicker-directive.js', env('SSL')) }}" charset="utf-8"></script>
<script type="text/javascript" src="{{ URL::asset('libs/chart/Chart.js', env('SSL')) }}"></script>
<script type="text/javascript" src="{{ URL::asset('libs/chart/angular-chart.min.js', env('SSL')) }}"></script>

<!--EFFECTS-->
<script type="text/javascript" src="{{ URL::asset('assets/admin/app/core/effects/nav-effects.js', env('SSL')) }}"></script>
<link rel="icon" type="image/x-icon" href="{{ URL::asset('src/layout/icons/favicon.png', env('SSL')) }}">
