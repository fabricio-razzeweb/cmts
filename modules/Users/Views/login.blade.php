<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{env('APP_NAME')}} - Admin</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ URL::asset('libs/bootstrap/css/bootstrap.min.css', env('SSL')) }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ URL::asset('libs/font-awesome/css/font-awesome.min.css', env('SSL')) }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset('assets/admin/dist/css/AdminLTE.min.css', env('SSL')) }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/admin/dist/css/style-admin.css', env('SSL')) }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ URL::asset('assets/admin/plugins/iCheck/square/blue.css', env('SSL')) }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page bg-login parrallax">

    <section class="login-page-content blue-transparent">

        <div class="login-box">

            <!-- /.login-logo -->
            <div class="login-box-body box">
                <div class="login-logo">
                    <img class="col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2" src="{{ URL::asset('src\layout\logos\logo-post-up.png', env('SSL')) }}" alt="{{env('APP_NAME')}}">
                </div><br>
                <form action="{{ route('authUser', env('SSL')) }}" method="POST" class="form-flat col-xs-12">
                    <br><br>
                    <div class="form-group has-feedback">
                        {{ csrf_field() }}
                        <input type="email" class="form-control form-control-orange--hover" placeholder="Email" name="email">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control form-control-orange--hover" placeholder="Password" name="password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    @if(session('authError'))
                        <div class="alert alert-danger">
                            {{session('authError')}}
                        </div>
                    @endif
                    <div class="row">

                        <!-- /.col -->
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-lg btn-success btn-block btn-flat">
                                <span class="fa fa-sign-in"></span>
                                Login
                            </button>
                        </div>
                        <!-- /.col -->
                        <!-- /.col -->
                        <div class="col-xs-12 text-center">
                            <div class="checkbox icheck">
                                <label>
                                    <input type="checkbox" name="is_remember"> Lembra-me
                                </label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->
    </section>
    <footer class="row blue"><br>
        <p class="f-white text-center">Todos os direitos reservados {{env('APP_NAME')}} &#169 Desenvolvido por Razze Web <i>Labs</i></p>
    </footer>
    <!-- jQuery 2.2.3 -->
    <script src="{{ URL::asset('libs/jquery/jquery.js', env('SSL')) }}"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{ URL::asset('libs/bootstrap/js/bootstrap.min.js', env('SSL')) }}"></script>
    <script src="{{ URL::asset('libs/masked-input/masked-input.js', env('SSL')) }}"></script>
    <!-- iCheck -->
    <script src="{{ URL::asset('assets/admin/plugins/iCheck/icheck.min.js', env('SSL')) }}"></script>
    <script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
    </script>
</body>
</html>
