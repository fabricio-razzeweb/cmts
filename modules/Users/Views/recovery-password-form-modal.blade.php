@if (session('showRevoveryPassWordFormModal') != null)
    <!-- Modal -->
    <div class="modal fade" id="recoveryPassWordFormModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-orange f-white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4><span class="fa fa-comment"></span> Recuperar Senha</h4>
                </div>
                <div class="modal-body row">
                    <h4 class="popUpModal-msg">
                        <form class="form-flat" action="{{route('user.passwordChange', env('SSL'))}}" method="POST">
                            {{csrf_field()}}
                            <div class="form-group col-md-12">
                                <label for="password"><span class="glyphicon glyphicon-lock"></span> Nova Senha:</label>
                                <input type="password" class="form-control form-control-orange--hover" name="password" value="{{old('password')}}" id="password" placeholder="exemplo@email.com" required="required">
                            </div><br>
                            <div class="col-md-12">
                                <button class="btn btn-success btn-lg btn-block btn-flat" type="submit">
                                    <span class="fa fa-check"></span> Alterar Senha
                                </button>
                            </div>
                            <input type="hidden" name="hash" value="{{session('hash')}}">
                        </form>
                    </h4>
                </div>

            </div>
        </div>
    </div>

    <script type="text/javascript">
    $(window).load(function(){
        $("#recoveryPassWordFormModal").modal('show');
    });
    </script>
@endif
