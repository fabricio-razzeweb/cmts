<?php

namespace Modules\Users\Requests;

use Modules\Core\Http\JWTRequest as JWTRequest;

class UsersRequestStore extends JWTRequest
{


    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        $rules = [
            'name'=>'required|max:250|min:5',
            'email'=>'required|max:80|min:8',
            'password'=>'required|max:24|min:8',
            'users_type_id'=>'required|integer',
        ];

        if ($this->users_type_id==1) {
            //$rules['cpf'] = "required|unique:users,cpf,".$this->get('id');
            $rules['date_birth'] = "required";
            $rules['phone'] = "required";
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'cpf.cpf'=>"O CPF digitado é inválido, por favor digite outro",
            'cpf.unique'=>"O este CPF já foi cadastrado, por favor tente com outro",
            'email.unique'=>"Este Email já foi cadastrado, por favor tente com outro",
        ];
    }

}



?>
