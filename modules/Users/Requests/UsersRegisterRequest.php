<?php

namespace Modules\Users\Requests;

use Modules\Core\Http\JWTRequest as JWTRequest;

class UsersRegisterRequest extends JWTRequest
{


    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:250|min:5',
            'email' => 'required|max:80|min:8|email|unique:users',
            'password' => 'required|max:24|min:8',
            'date_birth' => 'required|date',
            'phone' => 'required|string|unique:users',
            'sex' => 'string'
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'email.unique' => "Este Email já foi cadastrado, por favor tente com outro",
            'phone.unique' => "Este Telefone já foi cadastrado, por favor tente com outro",
            'date_birth.required' => "Digite sua data de nascimento",
            'date_birth.date' => "Está data de nascimento é inválida",
            'phone.required' => "Digite seu telefone",
            'password.required' => "Digite sua senha",
            'name.required' => "Digite seu nome",
            'name.string' => "Nome Inválido",
            'email.email' => "Este email não é um email válido"
        ];
    }

}



?>
