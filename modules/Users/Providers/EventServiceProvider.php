<?php
namespace Modules\Users\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Users\Events\UsersSubscribeEvents;
use Modules\Users\Listeners\SendWelcomeMailForNewSubscriber;
use Modules\Users\Events\RecoveryPasswordEvent;
use Modules\Users\Listeners\SendEmailForRecoveryPassword;

class EventServiceProvider extends ServiceProvider
{
    /**
    * The event listener mappings for the application.
    *
    * @var array
    */
    protected $listen = [
        UsersSubscribeEvents::class => [
            SendWelcomeMailForNewSubscriber::class,
        ],
        RecoveryPasswordEvent::class => [
            SendEmailForRecoveryPassword::class,
        ],
    ];

    /**
    * Register any events for your application.
    *
    * @return void
    */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../Migrations/');
        parent::boot();
    }
}
