<?php

namespace Modules\Users\Auth\Middlewares;

use Closure;

use Config;

use Illuminate\Support\Facades\Auth;

class AuthCommonUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
     public function handle($request, Closure $next, $guard = null)
     {

         Config::set('jwt.user', \Modules\Users\Users::class);
         Config::set('auth.providers.users.model', \Modules\Users\Users::class);

         $authGuard = Auth::guard('web');

         //If user is logged
         if ($authGuard->check()) {

               return $next($request);

         }else{
           $msgError = "Área restrita, por favor faça login para poder acessa-la";
         }

         return redirect('/admin')
           ->with('authError',$msgError);;

     }
}
