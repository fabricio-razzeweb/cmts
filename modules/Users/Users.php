<?php

namespace Modules\Users;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth as Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;
use Carbon\Carbon;

class Users extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;
    const GUARD_NAME = '';
    const USER_DEFAULT_TYPES = [
            'admin' => 1,
            'postman' => 2,
            'contact' => 4
    ];
    const USER_LOGIN_ROUTES = [
        1 => 'admin'
    ];

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'name',
        'email',
        'password',
        'photo',
        'is_active',
        'users_type_id',
    ];

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFieldsNames()
    {
        return $this->fillable;
    }

    public function getPhotoAttribute($value)
    {
        if (strpos($value,'http') === false) {
            return env('APP_URL').'/src/imgs/users/'.$value;
        }else{
            return $value;
        }
    }

    /**
    * Is Used for treate date of birth of client of format Y-m-d for d/m/Y
    */
    public function getDateBirthAttribute($value)
    {
        if (strpos($value, "/")) {
            return $value;
        } else {
            return Carbon::parse($value)->format('d/m/Y');
        }
    }

    /**
    * Is Used for treate date of birth of client of format d/m/Y for Y-m-d
    */
    public function setDateBirthAttribute($value)
    {
        if (strpos($value, "/")) {
            $this->attributes['date_birth'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        }elseif(strpos($value, "-")) {
            $this->attributes['date_birth'] = $value;
        }

    }

    public static function getAuthUser()
    {
        if (Auth::guard(self::GUARD_NAME)->check()) {

            $user = Auth::guard(self::GUARD_NAME)->user();

        }else{
            $user = null;
            $userNotAuth = true;
        }

        if(!isset($userNotAuth)){
            return $user;
        }

        return false;
    }

    public static function getAuthUserByJwt()
    {
        return self::findOrFail(Auth::user()->id);
    }

    public function verifyIfUserMailIsRegistred(string $mail)
    {
        return $this->where('email', $mail)->count() > 0;
    }

    public function verifyIfUserPhoneIsRegistred(string $phone)
    {
        return $this->where('phone', $phone)->count() > 0;
    }

    public function getOptions()
    {
        $options = [];
        $options['usersTypes'] = DB::table('users_type')->get();

        return $options;
    }

    public function posts()
    {
        return $this->hasMany(\Modules\Blog\Posts\Posts::class,'users_id');
    }

    public function categorys()
    {
        return $this->hasMany(\Modules\Blog\Categorys\Categorys::class,'users_id');
    }

}
