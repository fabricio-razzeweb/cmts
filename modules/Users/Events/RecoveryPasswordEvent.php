<?php
namespace Modules\Users\Events;

use Illuminate\Queue\SerializesModels;

class RecoveryPasswordEvent
{
    use SerializesModels;

    public $order;
    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $hash)
    {
        $this->hash = $hash;
        $this->user = $user;
    }

}
