<?php
namespace Modules\Users\Events;

use Illuminate\Queue\SerializesModels;

class UsersSubscribeEvents
{
    use SerializesModels;

    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

}
