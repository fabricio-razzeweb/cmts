<?php
namespace Modules\Users\Listeners;

use Modules\Users\Events\UsersSubscribeEvents;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use URL;
use Mail;

class SendWelcomeMailForNewSubscriber
{

    /**
    * Handle the event.
    *
    * @param  NewSubscribe  $event
    * @return void
    */
    public function handle(UsersSubscribeEvents $event)
    {
        $user = $event->user;
        try {
            $content = [
                'btnLink'=>URL::asset(env('APP_SITE_PREFIX')."register/confirm/email?h=".$user->hash, env('SSL')),
                'btnTextInside'=>"COMFIRMAR EMAIL",
                'title'=>"Seja Bem-vindo ao Vitrine Conquista",
                'linkSeeOn'=>URL::asset("",env('SSL')),
                'user'=>$user,
            ];

            if (isset($event->redirectUrl)) {
                $content['btnLink'] .= "&&redirect_url=$event->redirectUrl";
            }

            $mail = Mail::to($user->email)
                        ->send(new \Modules\Mails\WelcomeToApplication($content));

            if ($mail=null) {
                return true;
            }else{
                return false;
            }

        } catch (Exception $e) {
            return back()
            ->with(['error','Desculpe seu email é inválido entre em contato com nossa equipe']);
        }

    }
}
