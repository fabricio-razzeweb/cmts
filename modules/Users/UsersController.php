<?php

namespace Modules\Users;

use Modules\Users\Requests\UsersRequestStore;
use Modules\Users\Requests\UsersRequestUpdate;
use Illuminate\Http\Request;
use App\Http\Requests;
use Modules\Users\Users;
use Illuminate\Support\Facades\Auth as Auth;
use Modules\Core\Http\Controller as Controller;
use Hash;
use stdClass;

class UsersController extends Controller
{
    protected $archivesDirectoryType="imgs/";
    protected $archivesFinalDirectory="users";
    protected $nameElementUI="Usuário";
    protected $table="Users";
    public $viewFileEndName="User";
    public $viewDirectory="admin.Users.";
    protected $URLprefix="dashboard/usuario";

    public function __construct()
    {
        $this->model=new Users;
    }

    /**
    * Method ConfigForUpload()
    * This method config upload of file from method of parent uploadArchive()
    * @return Array $dataArchive
    *  - $dataArchive[directoryName] this is directory of type of archivesDirectoryType
    *  - $dataArchive[archiveRequestName] this is name of arcchive sended in request
    *  - $dataArchive[archiveExt] this is name of file extension
    */
    protected function ConfigForUpload()
    {
        $dataArchive=[
            'directoryNameType'=>$this->archivesDirectoryType,
            'archiveRequestName'=>"photo",
            'archiveExt'=>"png",
        ];
        return $dataArchive;
    }

    /**
    * Method uploadImage()
    * This method upload image of the element for the system
    * @param $request this is request of user (Object soon of Illuminate/Requests)
    * @param $archivePrefixName this is prefix name of image Ex: "title-image-23125673243267.png"
    * @return $fileName consist in the name of file after upload
    */
    protected function uploadImage($request ,$archivePrefixName)
    {
        // File Upload this return Archive name
        return $fileName=parent::uploadArchive($request,$archivePrefixName);
    }

    /**
    * Method prepareDataForTransfer()
    * This method prepare data for write and save of data
    * @param $request this is request of user (Object soon of Illuminate/Requests)
    * @param $element this is the element powered in consult of DateTimeImmutable
    * @return $element whith atributes necessary of writing in database
    */
    protected function prepareDataForTransfer($request,$element=null)
    {
        // If is Insert request
        if($element==null){

            // Create Element Ex: User, Category
            $element = $this->model;

            if ($request->hasFile('photo')) {
                //Make this upload image
                $element->photo=$this->uploadImage($request ,$request->input('name'));
            }else{
                $element->photo='default-user.png';
            }

        }else{

            //Check is file is seted
            if (isset($_FILES['photo']) && $_FILES['photo']['size']>0) {

                // Delete Old Photo
                parent::deleteArchive($element->photo);

                // Set new Photo
                $element->photo=$this->uploadImage($request ,$request->input('name'));

            }else{
                $element->photo='default-user.png';
            }

        }
        $fields = $this->model->getFieldsNames();

        for ($i=0; $i < count($fields); $i++) {

            $field = $fields[$i];

            if($request->has($field)){

                if ($fields[$i]==="password") {
                    $value = bcrypt($request->input("$field"));
                }else{
                    $value = $request->input("$field");
                }

                $element->$field = $value;

            }


        }

        if ($request->has("is_active")) {

            if ($request->input("is_active")=='true') {

                $element->is_active = 1;

            }else{

                $element->is_active = 0;

            }

        }


        return $element;
    }



    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if($request->get('type')){
            $Users = $this->model->where("users_type_id", "=", $request->get('type'))->get();
            return response()->json(compact('Users'));
        }else{
            return parent::basicIndex($request);
        }
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  UsersRequestStore   $request
    * @return \Illuminate\Http\Response
    */
    public function store(UsersRequestStore  $request)
    {
        $execution = [];

        $user = $this->prepareDataForTransfer($request);

        if ($request->has('phone')) {
            $user->phone = $request->phone;
        }

        if ($request->has('cpf')) {
            $user->cpf = $request->cpf;
        }

        $user->hash = bcrypt(date(`Y-m-d H:i:s`));
        $execution['status'] = $user->save();

        if ($request->neighborhood) {
            $user->saveUserAddress($request->all());
        }

        if ($execution['status']) {
            $execution['msg'] = "$this->nameElementUI, Cadastrado com sucesso!";
        }else{
            $execution['msg'] = "Desculpe ocorreu um erro ao executar está atividade";
        }

        return response()->json(compact('execution'));
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $element = $this->model->find($id)->getUserAddress();

        return response()->json(compact('element'));
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
        return $this->fethOne($id);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\UsersRequest   $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UsersRequestUpdate  $request, $id)
    {
        $execution = [];

        $user = $this->model->find((int)$id);

        $user = $this->prepareDataForTransfer($request, $user);

        if ($request->has('phone')) {
            $user->phone = $request->phone;
        }

        if ($request->has('cpf')) {
            $user->cpf = $request->cpf;
        }

        $execution['status'] = $user->save();

        if ($request->neighborhood) {
            $user->saveUserAddress($request->all());
        }

        if ($execution['status']) {
            $execution['msg'] = "$this->nameElementUI, Editado com sucesso!";
        }else{
            $execution['msg'] = "Desculpe ocorreu um erro ao executar está atividade";
        }

        return response()->json(compact('execution'));
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        return parent::delete($id);
    }

    public function myAccount()
    {
        $user = Auth::guard('web_users')->user();
        return response()->json(compact('user'));
    }

    public function updateMyAccount(Request $request)
    {

        //Get auth user
        $user = $this->model->findAuthUserInDB();

        //Check if password of user is equal of old password typed for user
        if (Hash::check($request->oldPassword,$user->password)) {

            //Update User Account
            return parent::basicUpdate($request, $user->id);

        }else{

            $execution = [
                'status'=>false,
                'msg'=>"Ação não concluida, pois a sua antiga senha é inválida",
            ];

            return response()->json(compact('execution'));

        }

    }

    public function checkMail(Request $request)
    {

        $countUserWhitThisMail = $this->model->where('email',$request->email)
        ->count();

        if ($countUserWhitThisMail>0) {
            $status = false;
            $msg = "Desculpe, mas este email já foi cadastrado!";
        }else{
            $status = true;
            $msg = "Este email está disponível para uso";
        }

        $execution = ['status'=>$status,
                        'msg'=>$msg];

        return response()->json(compact('execution'));
    }

    public function getOptions()
    {
        $options = $this->model->getOptions();

        return response()->json(compact('options'));
    }

}
