<?php
namespace Modules\Users;

use Illuminate\Support\Facades\Auth;
use Modules\Core\Http\Controller as Controller;
use Modules\Core\Http\Auth\JWTAuthController as JWTAuthController;

class UsersDashboardController extends Controller
{
    public function index()
    {
        $user = Auth::guard("web")->user();
        $token = JWTAuthController::generateToken($user);

        if ($token) {

            return view('Users::dashboard.index')
            ->with([
                'userName' => $user->name,
                'userPhoto' => $user->photo,
                'userId' => $user->id,
                'tk' => $token,
                'userHash' => $user->hash
            ]);

        }else{
            return back();
        }
    }
}
