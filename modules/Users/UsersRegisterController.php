<?php
namespace Modules\Users;

use Modules\Users\Users;
use Illuminate\Http\Request;
use Modules\Core\Http\Controller as Controller;
use Modules\Users\Requests\UsersRegisterRequest;

/*
* Class - UsersRegisterController
* Observations - This class is used for register users in application
* Author - Fabricio Magalhães Sena
*/
class UsersRegisterController extends Controller
{

    protected $usersModel;

    function __construct()
    {
        $this->usersModel = new Users;
    }

    protected function prepareDataForTransfer($request)
    {
        $user = $this->usersModel;
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->sex = $request->sex;
        $user->date_birth = $request->date_birth;
        $user->users_type_id = Users::USER_DEFAULT_TYPES['users'];// 3 is user
        $user->is_active = true;
        $user->photo='default-user.png';

        return $user;
    }

    public function store(UsersRegisterRequest $request)
    {
        $execution = [];
        $user = $this->prepareDataForTransfer($request);
        $user->hash = bcrypt(date(`Y-m-d H:i:s`));
        $execution['status'] = $user->save();

        if ($execution['status']) {
            
            event(new \Modules\Users\Events\UsersSubscribeEvents($user));

            $execution['msg'] = "Você foi Cadastrado com sucesso, por favor
                        confirme seu email atraves do link enviado para
                        sua caixa de entrada.";
        }else{
            $execution['msg'] = "Desculpe houve um erro ao cadastrar, tente
                                novamente mais tarde";
        }

        return response()
                ->json(compact('execution'));
    }

    public function checkEmailIsValid(Request $request)
    {
        $execution = [];
        $execution['status'] = !$this->usersModel
        ->verifyIfUserMailIsRegistred($request->email);

        return response()->json(compact('execution'));
    }

    public function checkNumberIsValid(Request $request)
    {
        $execution = [];
        $execution['status'] = !$this->usersModel
        ->verifyIfUserPhoneIsRegistred($request->phone);

        return response()->json(compact('execution'));
    }
}
