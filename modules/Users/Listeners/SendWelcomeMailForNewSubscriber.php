<?php
namespace Modules\Users\Listeners;

use Modules\Users\Events\UsersSubscribeEvents;
use Modules\FullNotifications\Sender;

class SendWelcomeMailForNewSubscriber
{

    /**
    * Handle the event.
    *
    * @param  UsersSubscribeEvents  $event
    * @return void
    */
    public function handle(UsersSubscribeEvents $event)
    {
        $message = "Olá, ".$event->user->name.", bem vindo ao nosso sistema
                    por favor confirme seu email";

        $link = [
            'label' => 'Confirmar Email',
            'link' => env('APP_URL').'cadastro/confirmar?h='.$event->user->hash
        ];

        Sender::setUser($event->user)
                ->setData([
                    'title' => "Confirme seu email",
                    'content' => $message,
                    'link' => $link
                ])->sendMail();

    }
}
