<?php
namespace Modules\Users\Listeners;

use Modules\Users\Events\RecoveryPasswordEvent;
use Modules\FullNotifications\Sender;

class SendEmailForRecoveryPassword
{

    /**
    * Handle the event.
    *
    * @param  OrderShipped  $event
    * @return void
    */
    public function handle(RecoveryPasswordEvent $event)
    {
        $message = "Olá, ".$event->user->name.", este pedido de recuperação de
                    senha expira em 10 minutos, clique no link abaixo para redefinir
                    sua senha.";

        $link = [
            'label' => 'Redefinir Senha',
            'link' => env('APP_URL').'recuperar/senha?h='.$event->hash
        ];

        Sender::setUser($event->user)
                ->setData([
                    'title' => "Recuperação de Senha",
                    'content' => $message,
                    'link' => $link
                ])->sendMail();
    }
}
