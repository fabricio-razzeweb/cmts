<?php

namespace Modules\Users\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Users;
use DB;

class UsersPolicy
{
    use HandlesAuthorization;

    protected function getUser()
    {
        return Users::getAuthUser();
    }
}
