<?php

namespace Modules\Users\Policies;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Modules\Products\Policies\ProductsPolicy;
use Modules\Products\Products;
use Modules\Stores\Policies\StoresPolicy;
use Modules\Stores\Stores;
use Modules\Orders\Policies\OrdersPolicy;
use Modules\Orders\Orders;
use Modules\Users\Policies\UsersPolicy;
use Modules\Users\Users;
use DB;

class AuthUserActionsServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Products::class => ProductsPolicy::class,
        Users::class => UsersPolicy::class,
        Stores::class => StoresPolicy::class,
        Orders::class => OrdersPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
