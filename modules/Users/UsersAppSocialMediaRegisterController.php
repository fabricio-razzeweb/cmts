<?php
namespace Modules\Users;

use Modules\Users\Users;
use Illuminate\Http\Request as Request;
use Modules\Core\Http\Controller as Controller;
use Modules\PushNotifications\UsersDevicesTokens\UsersDevicesTokens;
use Modules\Users\UsersSocialMediaKey;
use JWTAuth;

class UsersAppSocialMediaRegisterController extends Controller
{
    protected $usersModel;
    protected $usersDevicesTokensModel;
    protected $usersSocialMediaKeyModel;

    function __construct(Users $usersModel, UsersDevicesTokens $usersDevicesTokensModel,
                        UsersSocialMediaKey $usersSocialMediaKeyModel)
    {
        $this->usersModel = $usersModel;
        $this->usersDevicesTokensModel = $usersDevicesTokensModel;
        $this->usersSocialMediaKeyModel = $usersSocialMediaKeyModel;
    }

    protected function registerUser(array $data)
    {
        $user = $this->usersModel->fill($data);
        $user->users_type_id = Users::USER_DEFAULT_TYPES['users'];
        $user->save();

        return $user;
    }

    protected function registerDeviceToken(array $data, $user)
    {
        $deviceToken = $this->usersDevicesTokensModel->fill($request->all());
        $deviceToken->users_id = $user->id;
        return $deviceToken->save();
    }

    public function store(Request $request)
    {
        $execution = [];

        if ($this->usersSocialMediaKeyModel->verifyIfUserIsRegistred($request->all())) {

            $user = $this->usersSocialMediaKeyModel->where([
                'api_type' => $request->api_type,
                'api_id' => $request->api_id
            ])->first()
            ->user()
            ->first();

            $execution['status'] = true;

        } else if (!$this->usersModel->verifyIfUserMailIsRegistred($request->email) ||
                !$this->usersModel->verifyIfUserMailIsRegistred($request->phone)) {

            $user = $this->registerUser($request->all());

            $execution['status'] = $this->usersSocialMediaKeyModel->fill([
               'api_type' => $request->api_type,
               'api_id' => $request->api_id,
               'users_id' => $user->id
           ])->save();

        } else {

            $user = $this->usersModel
                    ->orWhere('email', $request->email)
                    ->orWhere('phone', $request->phone)
                    ->first();

            $execution['status'] = true;
        }

        $token = JWTAuth::fromUser($user);

        $user=['name' => $user->name,
               'photo' => $user->photo,
               'id' => $user->id,
               'hash' => $user->hash];

        return response()->json(compact('execution', 'user', 'token'));
    }

    public function checkIfSocialUserIsRegistred(Request $request)
    {
        if ($this->usersSocialMediaKeyModel->verifyIfUserIsRegistred($request->all())) {

            $user = $this->usersSocialMediaKeyModel->where([
                'api_type' => $request->api_type,
                'api_id' => $request->api_id
            ])->first()
            ->user()
            ->first();

            $execution['status'] = true;

            if ($user) {
                $token = JWTAuth::fromUser($user);

                $user=['name' => $user->name,
                       'photo' => $user->photo,
                       'id' => $user->id,
                       'hash' => $user->hash];

                return response()->json(compact('execution', 'user', 'token'));
            } else {
                $execution['status'] = false;
                return response()->json(compact('execution'));
            }

        } else {
            $execution['status'] = false;
            return response()->json(compact('execution'));
        }
    }
}
