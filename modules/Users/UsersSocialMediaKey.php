<?php

namespace Modules\Users;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsersSocialMediaKey extends Model
{
    use SoftDeletes;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'users_id',
        'api_id',
        'api_type',
    ];

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [];

    public function verifyIfUserIsRegistred(array $params): bool
    {
        $numberOfUsersWithAPiId = $this->where('api_id', $params['api_id'])
            ->where('api_type', $params['api_type'])
            ->count();

        return  $numberOfUsersWithAPiId > 0;
    }

    public function user()
    {
        return $this->belongsTo(\Modules\Users\Users::class, 'users_id');
    }
}
