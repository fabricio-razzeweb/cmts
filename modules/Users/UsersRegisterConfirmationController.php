<?php
namespace Modules\Users;

use Modules\Users\Users;
use Illuminate\Http\Request;
use Modules\Core\Http\Controller as Controller;

class UsersRegisterConfirmationController {

    protected $usersModel;

    function __construct()
    {
        $this->usersModel = new Users;
    }

    public function confirm(Request $request)
    {
        $user = $this->usersModel
                ->where('hash', $request->get('h'))
                ->first();

        if ($user) {
            $user->email_confirmed = true;
            $user->save();

            return redirect(env('APP_URL')."/paciente?confirmed=true");

        } else {

            return redirect(env('APP_URL'))
                    ->with([
                        'alertMsg' => "A chave gerada é inválida"
                    ]);

        }
    }
}
