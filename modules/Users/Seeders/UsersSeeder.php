<?php
namespace Modules\Users\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_types')->insert([
            [
                'name' => 'admin'
            ],
            [
                'name' => 'postman'
            ],
            [
                'name' => 'secretary'
            ]
        ]);

        DB::table('users')->insert([
            'name' => 'Razze Web',
            'email' => 'razzeweb@gmail.com',
            'photo' => 'default.png',
            'password' => bcrypt('12345678'),
            'users_types_id' => 1
        ]);
    }
}
