<?php
namespace Modules\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BeatyfulMails extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The data of email.
     *
     * @var array $data
     */
    public $data;
    public $config;
    
    /**
     * Create a new message instance.
     * @param array $data
     * -> $data['title'] - this is the title of Email
     * -> $data['content'] - this is the content of email
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
        $this->config = config('mail-template');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Mail::examples.simple');
    }

}
