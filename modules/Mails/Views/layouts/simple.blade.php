@include('Mails::template.head', ['aplicationName' => $config['enterpriseInfo']['name']])
<body style="font-size:12px;">
    <table id="mainStructure" width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#ecebeb;">
        @include('Mails::template.menu-bar')
        <repeater>
            @include('Mails::template.top-navigation')

            @yield('content')

            @include('Mails::template.contact-itens-list', ['simpleListOfItens' => $config['enterpriseInfo']['contactMethods']])
            @include('Mails::template.box-footer-social-links', ['socialLinksList' => $config['socialLinksList']])
        </repeater>
        @include('Mails::template.footer-bar')
    </table>
</body>
</html>
