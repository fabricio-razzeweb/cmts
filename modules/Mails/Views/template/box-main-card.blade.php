<layout label='layout-1/1'>
    <!-- START LAYOUT-1/1 -->
    <tr>
        <td align="center" valign="top" class="fix-box">

            <!-- start  container width 600px -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #ffffff; ">


                <tr>
                    <td valign="top">

                        <!-- start container width 560px -->
                        <table width="540" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" bgcolor="#ffffff" style="background-color:#ffffff;">


                            <!-- start text content -->
                            <tr>
                                <td valign="top">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr>
                                            <td valign="top" width="auto" align="center">
                                                <!-- start button -->
                                                <table border="0" align="center" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="auto" align="center" valign="middle" height="28" style=" background-color:#ffffff; border:1px solid #ececed; background-clip: padding-box; font-size:18px; font-family:Roboto,Open Sans, Arial,Tahoma, Helvetica, sans-serif; text-align:center;  color:#a3a2a2; font-weight: 300; padding-left:18px; padding-right:18px; ">

                                                            <span style="color: #a3a2a2; font-weight: 300;">
                                                                <a href="#" style="text-decoration: none; color:#a3a2a2; font-weight: 300;">
                                                                    <singleline label="heading (layout-1/1) TB12">
                                                                        {!!$title!!}
                                                                    </singleline>
                                                                </a>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- end button -->
                                            </td>
                                        </tr>



                                    </table>
                                </td>
                            </tr>
                            <!-- end text content -->


                        </table>
                        <!-- end  container width 560px -->
                    </td>
                </tr>
            </table>
            <!-- end  container width 600px -->
        </td>
    </tr>
    <!-- END LAYOUT-1/1 -->
</layout>


<layout label='layout-1/2'>
    <!-- START LAYOUT-1/2 -->
    <tr>
        <td align="center" valign="top" class="fix-box">

            <!-- start  container width 600px -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #ffffff; ">


                <tr>
                    <td valign="top">

                        <!-- start container width 560px -->
                        <table width="540" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" bgcolor="#ffffff" style="background-color:#ffffff;">


                            <!-- start text content -->
                            <tr>
                                <td valign="top">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">


                                        <!-- start text content -->
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellspacing="0" cellpadding="0" align="center">


                                                    <!--start space height -->
                                                    <tr>
                                                        <td height="15"></td>
                                                    </tr>
                                                    <!--end space height -->

                                                    <tr>
                                                        <td style="font-size: 13px; line-height: 22px; font-family:Roboto,Open Sans,Arial,Tahoma, Helvetica, sans-serif; color:#a3a2a2; font-weight:300; text-align:center; ">

                                                            <multiline label="content (layout-1/2) TB13">
                                                                {!!$content!!}
                                                            </multiline>

                                                        </td>
                                                    </tr>

                                                    <!--start space height -->
                                                    <tr>
                                                        <td height="15"></td>
                                                    </tr>
                                                    <!--end space height -->



                                                </table>
                                            </td>
                                        </tr>
                                        <!-- end text content -->

                                        <tr>
                                            <td valign="top" width="auto" align="center">
                                                <!-- start button -->
                                                <table border="0" align="center" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        @if (isset($link))
                                                            <td width="auto" align="center" valign="middle" height="32" style=" background-color:{{$mainColor}};  border-radius:1px; background-clip: padding-box;font-size:18px; font-family:Roboto,Open Sans, Arial,Tahoma, Helvetica, sans-serif; text-align:center;  color:#ffffff; font-weight: 300; padding-left:24px; padding-right:24px; padding:10px;">
                                                                <span style="color: #ffffff; font-weight: 300;">
                                                                    <a href="{{$link->link}}" style="text-decoration: none; color:#ffffff; font-weight: 300;">
                                                                        <singleline label="button left (layout-1/2) TB14">{{$link->name}}</singleline>
                                                                    </a>
                                                                </span>
                                                            </td>
                                                        @endif
                                                    </tr>
                                                </table>
                                                <!-- end button -->
                                            </td>

                                        </tr>

                                    </table>
                                </td>
                            </tr>
                            <!-- end text content -->

                            <!--start space height -->
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <!--end space height -->


                        </table>
                        <!-- end  container width 560px -->
                    </td>
                </tr>
            </table>
            <!-- end  container width 600px -->
        </td>
    </tr>
    <!-- END LAYOUT-1/2 -->
</layout>

<layout label='shadow space 1'>
    <!-- START SHADOW-->
    <tr>
        <td valign="top" align="center" class="fix-box">
            <table width="600" height="11" align="center" border="0" cellspacing="0" cellpadding="0" style="background-color: #ffffff;" class="full-width">
                <tr>
                    <td valign="top" height="11" class="image-100-percent">
                        <img editable="" label="shadow 1 TB17" src="{{$config['basePathOfResources']}}images/shadow.png" width="600" alt="space" style="display:block; max-height:11px; max-width:600px;"> </td>
                    </tr>
                </table>
            </td>
        </tr>
        <!-- END SHADOW-->
    </layout>
