<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
@include('Mails::template.head')
<body style="font-size:12px;">
    <table id="mainStructure" width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#ecebeb;">
        @include('Mails::template.menu-bar')
        <repeater>
            @include('Mails::template.top-navigation')
            @include('Mails::template.box-main-card')
            @include('Mails::template.box-itens-list-33')
            @yield('content')
            @include('Mails::template.simple-itens-list')
            @include('Mails::template.box-footer-social-links')
        </repeater>
        @include('Mails::template.footer-bar')
    </table>
</body>
</html>
