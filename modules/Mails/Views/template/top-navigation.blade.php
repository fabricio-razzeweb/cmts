<layout label='Top Navigation'>

    <!--START TOP NAVIGATION ​LAYOUT-->
    <tr>
        <td valign="top">
            <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">


                <!-- START CONTAINER NAVIGATION -->
                <tr>
                    <td align="center" valign="top">

                        <!-- start top navigation container -->
                        <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container">

                            <tr>
                                <td valign="top">


                                    <!-- start top navigaton -->
                                    <table width="560" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width">

                                        <!-- start space -->
                                        <tr>
                                            <td valign="top" height="20">
                                            </td>
                                        </tr>
                                        <!-- end space -->

                                        <tr>
                                            <td valign="middle">

                                                <table align="left" border="0" cellspacing="0" cellpadding="0" class="container2">

                                                    <tr>
                                                        
                                                        <td align="center" valign="top">
                                                            <a href="{{$config['enterpriseInfo']['website']}}" style="text-decoration: none;">
                                                                <img editable="" label="Top logo TB9" src="{{$config['enterpriseInfo']['logo']}}" width="114" style="max-width:114px;" alt="Logo" border="0" hspace="0" vspace="0">
                                                            </a>
                                                        </td>

                                                    </tr>


                                                    <!-- start space -->
                                                    <tr>
                                                        <td valign="top" class="increase-Height-20">

                                                        </td>
                                                    </tr>
                                                    <!-- end space -->

                                                </table>

                                                <!--start content nav -->
                                                <table border="0" align="right" cellpadding="0" cellspacing="0" class="container2">


                                                    <!--start call us -->
                                                    <tr>
                                                        <td valign="middle" align="center">

                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="clear-align" style="height:100%;">
                                                                <tr>
                                                                    <td style="font-size: 13px;  line-height: 18px; color: #a3a2a2;  font-weight:300; text-align: center; font-family:Roboto,Open Sans,Arail,Tahoma, Helvetica, Arial, sans-serif;">

                                                                        <span style="text-decoration: none; color: #a3a2a2;">
                                                                            <multiline label="menu TB10">

                                                                             </multiline>
                                                                         </span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <!--end call us -->

                                                </table>
                                                <!--end content nav -->

                                            </td>
                                        </tr>

                                        <!-- start space -->
                                        <tr>
                                            <td valign="top" height="20">
                                            </td>
                                        </tr>
                                        <!-- end space -->

                                    </table>
                                    <!-- end top navigaton -->
                                </td>
                            </tr>
                        </table>
                        <!-- end top navigation container -->

                    </td>
                </tr>
                <!-- END CONTAINER NAVIGATION -->

            </table>
        </td>
    </tr>
    <!--END TOP NAVIGATION ​LAYOUT-->
</layout>

<layout label='white space'>
    <!-- START HEIGHT SPACE 20PX LAYOUT-1 -->
    <tr>
        <td valign="top" align="center" class="fix-box">
            <table width="600" height="20" align="center" border="0" cellspacing="0" cellpadding="0" style="background-color: #ffffff;" class="full-width">
                <tr>
                    <td valign="top" height="20">
                        <img editable="" label="space layout-1 TB11" src="{{$config['basePathOfResources']}}images/space.png" width="20" alt="space" style="display:block; max-height:20px; max-width:20px;"> </td>
                </tr>
            </table>
        </td>
    </tr>
    <!-- END HEIGHT SPACE 20PX LAYOUT-1-->
</layout>
