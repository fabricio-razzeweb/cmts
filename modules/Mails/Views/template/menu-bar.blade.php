<!--START VIEW ONLINE AND ICON SOCAIL -->
<tr>
    <td align="center" valign="top" style="background-color: {{$config['mainColor']}}; ">

        <!-- start container 600 -->
        <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: {{$config['mainColor']}}; ">
            <tr>
                <td valign="top">

                    <table width="560" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" style="background-color: {{$config['mainColor']}}; ">
                        <!-- start space -->
                        <tr>
                            <td valign="top" height="10">
                            </td>
                        </tr>
                        <!-- end space -->
                        <tr>
                            <td valign="top">

                                <!-- start container -->
                                <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">

                                    <tr>
                                        <td valign="top">

                                            <!-- start view online -->
                                            <table align="left" border="0" cellspacing="0" cellpadding="0" class="container2">
                                                <tr>
                                                    <td>
                                                        <table align="center" border="0" cellspacing="0" cellpadding="0">
                                                            @if (isset($menuLinksList))
                                                                @foreach ($menuLinksList as $menuLink)
                                                                    <tr>
                                                                        <td style="font-size: 12px; line-height: 27px; font-family:Roboto,Open Sans, Arial,Tahoma, Helvetica, sans-serif; color:#ffffff; font-weight:300; text-align:center;">
                                                                            <span style="text-decoration: none; color: #ffffff;">
                                                                                <webversion style="text-decoration: none; color: #ffffff;">{{$menuLink['name']}}</webversion>
                                                                            </span>
                                                                        </td>
                                                                        <td style="padding-left:5px;" align="left" valign="middle">
                                                                            <img editable="" label="arrow view online TB0" src="{{$config['basePathOfResources']}}images/{{$menuLink['icon']}}" width="10" alt="arrow1" style="max-width:10px; display:block !important;" border="0" hspace="0" vspace="0">
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        </table>
                                                    </td>
                                                </tr>
                                                <!-- start space -->
                                                <tr>
                                                    <td valign="top" class="increase-Height">
                                                    </td>
                                                </tr>
                                                <!-- end space -->
                                            </table>
                                            <!-- end view online -->

                                            <!--start icon socail -->
                                            <table border="0" align="right" cellpadding="0" cellspacing="0" class="container2">

                                                <tr>
                                                    <td valign="top" align="center">

                                                        <table border="0" align="center" cellpadding="0" cellspacing="0" class="container">
                                                            <tr>
                                                                <td valign="top" align="center">

                                                                    <table border="0" align="center" cellpadding="0" cellspacing="0" class="container">
                                                                        <tr>
                                                                            @if (isset($config['socialLinksList']))
                                                                                @foreach ($config['socialLinksList'] as $socialLink)
                                                                                    <td align="center" valign="middle" id="clear-padding">
                                                                                        <a href="{{$socialLink['link']}}" style="text-decoration: none;">
                                                                                            <img editable="" label="icon-facebook TB1" src="{{$config['basePathOfResources']}}images/{{$socialLink['icon']}}" width="30" alt="icon-facebook" style="max-width:30px;" border="0" hspace="0" vspace="0">
                                                                                        </a>
                                                                                    </td>
                                                                                @endforeach
                                                                            @endif

                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </td>
                                                </tr>
                                            </table>
                                            <!--end icon socail -->

                                        </td>
                                    </tr>
                                </table>
                                <!-- end container  -->
                            </td>
                        </tr>

                        <!-- start space -->
                        <tr>
                            <td valign="top" height="10">
                            </td>
                        </tr>
                        <!-- end space -->

                        <!-- start space -->
                        <tr>
                            <td valign="top" class="increase-Height">
                            </td>
                        </tr>
                        <!-- end space -->

                    </table>
                    <!-- end container 600-->
                </td>
            </tr>
        </table>

    </td>
</tr>
<!--END VIEW ONLINE AND ICON SOCAIL-->
