<layout label='layotu-16'>

    <!-- START LAYOUT-16 -->

    <tr>
        <td align="center" valign="top" class="fix-box">

            <!-- start  container width 600px -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #ffffff; border-bottom:1px solid #c7c7c7;">
                <tr>
                    <td valign="top">
                        <table  width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <h3 style="font-family: sans-serif; color: #333; text-align: center;">{{$config['enterpriseInfo']['name']}}</h3>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!--start space height -->
                <tr>
                    <td height="10"></td>
                </tr>
                <!--end space height -->

                <tr>
                    <td valign="top">


                        <!-- start logo footer and address -->
                        <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="background-color:{{$config['mainColor']}}">
                            <tr>
                                <td valign="top">

                                    <!--start icon socail navigation -->
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" class="container">
                                        <tr>
                                            <td valign="top" align="left">

                                                <table border="0" align="left" cellpadding="0" cellspacing="0" class="container">
                                                    <tr>
                                                        @if (isset($socialLinksList))
                                                            @foreach ($socialLinksList as $socialLink)
                                                                <td height="30" align="center" valign="middle" class="clear-padding">
                                                                    <a href="{{$socialLink['link']}}" style="text-decoration: none;">
                                                                        <img editable="" label="icon-facebook-color TB112"
                                                                        src="{{$config['basePathOfResources']}}images/{{$socialLink['icon']}}"
                                                                        width="30" alt="icon-facebook" style="max-width:30px;" border="0" hspace="0" vspace="0">
                                                                    </a>
                                                                </td>
                                                            @endforeach
                                                        @endif
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                    </table>
                                    <!--end icon socail navigation -->
                                </td>
                            </tr>
                        </table>
                        <!-- end logo footer and address -->

                    </td>
                </tr>

            </table>
        </td>
    </tr>
    <!-- END LAYOUT-16-->

</layout>
