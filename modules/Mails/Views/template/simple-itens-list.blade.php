<layout label='layotu-12'>


    <!-- START LAYOUT-12 -->

    <tr>
        <td align="center" valign="top" class="fix-box">

            <!-- start container width 600px -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" bgcolor="#ffffff" style="background-color: #ffffff; border-bottom:1px solid #c7c7c7;">
                <tr>
                    <td valign="top">

                        <!-- start  container width 560px -->
                        <table width="560" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" bgcolor="#ffffff" style="background-color:#ffffff;">


                            <!-- start image and content -->
                            <tr>
                                <td valign="top">
                                    @if (isset($simpleListOfItens))
                                        @foreach ($simpleListOfItens as $iten)
                                            <!-- start content left -->
                                            <table width="170" border="0" cellspacing="0" cellpadding="0" align="left" class="col-3">

                                                <!--start space height -->
                                                <tr>
                                                    <td height="20"></td>
                                                </tr>
                                                <!--end space height -->

                                                <tr>
                                                    <td width="100%" valign="top" align="center">
                                                        <a href="" style="text-decoration: none;">
                                                            <img editable="" label="image-14 TB90" src="{{$iten->image}}" width="58" alt="image14_58x58" style="max-width:58px; display:block !important; " border="0" hspace="0" vspace="0">
                                                        </a>
                                                    </td>
                                                </tr>

                                                <!--start space height -->
                                                <tr>
                                                    <td height="20"></td>
                                                </tr>
                                                <!--end space height -->

                                                <tr style="text-align:center">
                                                    <td>
                                                        <table border="0" cellspacing="0" cellpadding="0" align="left">
                                                            <tr>
                                                                <td style="font-size: 18px; line-height: 22px; font-family:Roboto,Open Sans, Arial,Tahoma, Helvetica, sans-serif; color:#555555; font-weight:300; text-align:center;">
                                                                    <span style="color: #555555; font-weight: 300;">
                                                                        <a href="#" style="text-decoration: none; color: rgb(85, 85, 85); font-weight: 300;">
                                                                            <singleline label="heading left (layout-12) TB91"> {{$iten->name}} </singleline>
                                                                        </a>
                                                                    </span>
                                                                </td>
                                                            </tr>

                                                            <!--start space height -->
                                                            <tr>
                                                                <td height="15"></td>
                                                            </tr>
                                                            <!--end space height -->

                                                            <tr>
                                                                <td style="font-size: 13px; line-height: 22px; font-family:Roboto,Open Sans, Arial,Tahoma, Helvetica, sans-serif; color:#a3a2a2; font-weight:300; text-align:center; ">

                                                                    <multiline label="content left (layout-12) TB92">{{$iten->description}}</multiline>

                                                                </td>
                                                            </tr>

                                                        </table>

                                                    </td>
                                                </tr>

                                                <!--start space height -->
                                                <tr>
                                                    <td height="20" class="col-underline"></td>
                                                </tr>
                                                <!--end space height -->

                                            </table>
                                            <!-- end content left -->

                                            <!-- start space width  -->
                                            <table class="remove" width="1" border="0" cellpadding="0" cellspacing="0" align="left" style="font-size: 0;line-height: 0;border-collapse: collapse;">
                                                <tr>
                                                    <td width="0" height="3" style="font-size: 0;line-height: 0;border-collapse: collapse;">
                                                        <p style="padding-left: 20px;">&nbsp;</p>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- end space width  -->
                                        @endforeach
                                    @endif
                                </td>
                            </tr>
                            <!-- end image and content -->

                        </table>
                        <!-- end container width 560px -->
                    </td>
                </tr>
            </table>
            <!-- end container width 600px -->
        </td>
    </tr>
    <!-- END LAYOUT-12 -->
</layout>
