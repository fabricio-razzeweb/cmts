<layout label='layotu-14'>


    <!-- START LAYOUT-14 -->
    <tr>
        <td align="left" style="font-size: 16px; line-height: 22px; font-family:Roboto,Open Sans, Arial,Tahoma, Helvetica, sans-serif; color:#a3a2a2; font-weight:300; text-align:left;">
            <span style="color: #a3a2a2; font-weight:300;">
                    <a href="#" style="text-decoration: none; color:#a3a2a2; font-weight: 300;">
                        <singleline label="title (layout-8/1) TB59">
                             <span style="text-decoration: none; color: {{$config['mainColor']}}; font-weight: 300;">{{$boxItensListTitle}}</span>
                        </singleline>
                    </a>
            </span>
        </td>
        <td align="center" valign="top" class="fix-box">

            <!-- start  container width 600px -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" bgcolor="#ffffff" style="background-color: #ffffff; border-bottom:1px solid #c7c7c7;">
                <tr>
                    <td valign="top">

                        <!-- start container width 560px -->
                        <table width="560" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" bgcolor="#ffffff" style="background-color:#ffffff;">

                            <!-- start image and content -->
                            <tr>
                                <td valign="top" width="100%">

                                    <!-- start content left -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">

                                        @if (isset($itenListLateral))
                                            @foreach ($itenListLateral as $iten)
                                                <!--start space height -->
                                                <tr>
                                                    <td height="20"></td>
                                                </tr>
                                                <!--end space height -->


                                                <!-- start content top-->
                                                <tr>
                                                    <td valign="top" align="left">

                                                        <table border="0" cellspacing="0" cellpadding="0" align="left">
                                                            <tr>

                                                                <td valign="top" align="left" style="padding-right:20px;">
                                                                    <a href="" style="text-decoration: none;">
                                                                        <img editable="" label="face-1 TB100" src="{{$iten->image}}" width="69" alt="face1_69x69" style="max-width:69px; display:block !important; " border="0" hspace="0" vspace="0">
                                                                    </a>
                                                                </td>


                                                                <td valign="top">

                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
                                                                        <tr>
                                                                            <td style="font-size: 18px; line-height: 22px; font-family:Roboto,Open Sans, Arial,Tahoma, Helvetica, sans-serif; color:#555555; font-weight:300; text-align:left;">
                                                                                <span style="color: #555555; font-weight: 300;">
                                                                                    <a href="#" style="text-decoration: none; color: rgb(85, 85, 85); font-weight: 300;">
                                                                                        <singleline label="heading top (layout-14) TB101">{{$iten->name}} </singleline>
                                                                                    </a>
                                                                                </span>
                                                                            </td>
                                                                        </tr>

                                                                        <!--start space height -->
                                                                        <tr>
                                                                            <td height="10"></td>
                                                                        </tr>
                                                                        <!--end space height -->

                                                                        <tr>
                                                                            <td style="font-size: 13px; line-height: 22px; font-family:Roboto,Open Sans,Arial,Tahoma, Helvetica, sans-serif; color:#a3a2a2; font-weight:300; text-align:left; ">

                                                                                <multiline label="content top (layout-14) TB102">
                                                                                        {!!$iten->content!!}
                                                                                </multiline>

                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <!-- end  content top-->


                                                <!--start space height -->
                                                <tr>
                                                    <td height="15" class="col-underline"></td>
                                                </tr>
                                                <!--end space height -->
                                            @endforeach
                                        @endif


                                    </table>
                                    <!-- end content left -->


                                </td>
                            </tr>
                            <!-- end image and content -->

                        </table>
                        <!-- end  container width 560px -->
                    </td>
                </tr>
            </table>
            <!-- end  container width 600px -->
        </td>
    </tr>
    <!-- END LAYOUT-14 -->
</layout>
