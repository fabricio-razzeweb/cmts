<!-- START LAYOUT-5 -->
<layout label='layotu-5'>
    <tr>

        <td align="center" valign="top" class="fix-box">

            <!-- start LAYOUT-5 container width 600px -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" bgcolor="#ffffff" style="background-color: #ffffff; border-bottom:1px solid #c7c7c7;">
                <tr>
                    <td valign="top">

                        <!-- start LAYOUT-5 container width 560px -->
                        <table width="560" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" bgcolor="#ffffff" style="background-color:#ffffff;">
                            <tr align="center" valign="top" class="fix-box" width="600px">
                                <td align="center"
                                style="background-color: #fff;
                                font-size: 24px;
                                line-height: 22px;
                                font-family:Roboto,Open Sans, Arial,Tahoma, Helvetica, sans-serif;
                                color:#a3a2a2;
                                font-weight:300;
                                text-align:center;">
                                    <span style="color: #a3a2a2; font-weight:300;">
                                            <a href="#" style="text-decoration: none; color:#a3a2a2; font-weight: 300;">
                                                <singleline label="title (layout-8/1) TB59">
                                                     <span style="text-decoration: none; color: {{$mainColor}}; font-weight: 300;">
                                                         {{$boxItensListTitle}}
                                                     </span>
                                                </singleline>
                                            </a>
                                    </span>
                                </td>
                            </tr>

                            <!-- start image and content -->
                            <tr>
                                <td valign="top">
                                    @if (isset($products))
                                        @foreach ($products as $product)
                                            <!-- start content left -->
                                            <table width="170" border="0" cellspacing="0" cellpadding="0" align="left" class="col-3">

                                                <!--start space height -->
                                                <tr>
                                                    <td height="20"></td>
                                                </tr>
                                                <!--end space height -->

                                                <tr>
                                                    <td width="100%" valign="top" align="left" class="image-170px">
                                                        <a href="" style="text-decoration: none;">
                                                            <img editable="" label="image-5 TB38" src="{{URL::asset('src/imgs/products/'.$product->product_main_image,env('SSL'), true)}}" width="170" alt="image5_280x210" style="max-width:170px; display:block !important; " border="0" hspace="0" vspace="0">
                                                        </a>
                                                    </td>
                                                </tr>

                                                <!--start space height -->
                                                <tr>
                                                    <td height="20"></td>
                                                </tr>
                                                <!--end space height -->

                                                <tr>
                                                    <td>
                                                        <table border="0" cellspacing="0" cellpadding="0" align="left">
                                                            <tr>
                                                                <td style="font-size: 18px; line-height: 22px; font-family:Roboto,Open Sans, Arial,Tahoma, Helvetica, sans-serif; color:#555555; font-weight:300; text-align:left;">
                                                                    <span style="color: #555555; font-weight: 300;">
                                                                        <a href="#" style="text-decoration: none; color: rgb(85, 85, 85); font-weight: 300;">
                                                                            <singleline label="heading left (layout-5) TB39">{{$product->product_name}}</singleline>
                                                                        </a>
                                                                    </span>
                                                                </td>
                                                            </tr>

                                                            <!--start space height -->
                                                            <tr>
                                                                <td height="15"></td>
                                                            </tr>
                                                            <!--end space height -->

                                                            <tr>
                                                                <td style="font-size: 13px; line-height: 22px; font-family:Roboto,Open Sans, Arial,Tahoma, Helvetica, sans-serif; color:#a3a2a2; font-weight:300; text-align:left; ">

                                                                    <multiline label="content left (layout-5) TB40">
                                                                    </multiline>

                                                                </td>
                                                            </tr>

                                                            <!--start space height -->
                                                            <tr>
                                                                <td height="20"></td>
                                                            </tr>
                                                            <!--end space height -->

                                                            <tr>
                                                                <td valign="top" width="auto">
                                                                    <!-- start button -->
                                                                    <table border="0" align="left" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td width="auto" align="center" valign="middle" height="32" style=" background-color:{{$mainColor}};  border-radius:1px; background-clip: padding-box;font-size:16px; font-family:Roboto,Open Sans, Arial,Tahoma, Helvetica, sans-serif; text-align:center;  color:#ffffff; font-weight: 300; padding-left:18px; padding-right:18px; ">

                                                                                <span style="color: #ffffff; font-weight: 800;">
                                                                                    <a href="{{URL::asset('produtos/'.$product->link, env('SSL'), true)}}" style="text-decoration: none; color:#ffffff; font-weight: 300;">
                                                                                        <singleline label="button left (layout-5) TB41">Ver </singleline>
                                                                                    </a>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <!-- end button -->

                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </td>
                                                </tr>

                                                <!--start space height -->
                                                <tr>
                                                    <td height="20" class="col-underline"></td>
                                                </tr>
                                                <!--end space height -->

                                            </table>
                                            <!-- end content left -->

                                            <!-- start space width  -->
                                            <table class="remove" width="1" border="0" cellpadding="0" cellspacing="0" align="left" style="font-size: 0;line-height: 0;border-collapse: collapse;">
                                                <tr>
                                                    <td width="0" height="3" style="font-size: 0;line-height: 0;border-collapse: collapse;">
                                                        <p style="padding-left: 20px;">&nbsp;</p>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- end space width  -->
                                        @endforeach
                                    @endif
                                </td>
                            </tr>
                            <!-- end image and content -->
                            <tr>
                                <td valign="top" width="auto" align="center">
                                    <!-- start button -->
                                    <table border="0" align="center" cellpadding="0" cellspacing="0">
                                        <tr>
                                            @if (isset($productListMainLink))
                                                <td width="auto" align="center" valign="middle" height="32" style=" background-color:{{$mainColor}};  border-radius:1px; background-clip: padding-box;font-size:18px; font-family:Roboto,Open Sans, Arial,Tahoma, Helvetica, sans-serif; text-align:center;  color:#ffffff; font-weight: 300; padding-left:24px; padding-right:24px; padding:10px;">
                                                    <span style="color: #ffffff; font-weight: 300;">
                                                        <a href="{{$productListMainLink->link}}" style="text-decoration: none; color:#ffffff; font-weight: 300;">
                                                            <singleline label="button left (layout-1/2) TB14">{{$productListMainLink->name}}</singleline>
                                                        </a>
                                                    </span>
                                                </td>
                                            @endif
                                        </tr>
                                    </table>
                                    <!-- end button -->
                                </td>

                            </tr>

                            <!--start space height -->
                            <tr>
                                <td height="20" class="col-underline"></td>
                            </tr>
                            <!--end space height -->
                        </table>
                        <!-- end LAYOUT-5 container width 560px -->
                    </td>
                </tr>
            </table>
            <!-- end LAYOUT-5 container width 600px -->
            <br><br><br>
        </td>
    </tr>
    <!-- END LAYOUT-5  -->
</layout>
