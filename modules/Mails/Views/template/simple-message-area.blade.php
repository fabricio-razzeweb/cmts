<layout label='layout-1/1'>
    <tr>
        <td align="center" valign="top" class="fix-box">

            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #fff;">
                <tr>
                    <td valign="top" style="text-align:center; padding:30px; color:#333;">
                        <h2>{{$title}}</h2> <br>
                        {!!$content!!}
                    </td>
                </tr>
            </table>
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #fff;">
                <tr> <br><br><br>
                    @if ($link)
                        <td width="180" align="center" valign="middle" height="32" style=" background-color:{{$config['mainColor']}};
                            border-radius:1px; background-clip: padding-box;font-size:18px;
                            font-family:Roboto,Open Sans, Arial,Tahoma, Helvetica, sans-serif; text-align:center;
                            color:#ffffff; font-weight: 300;  margin-bottom: 40px; padding:10px; padding-left:30px; padding-right:30px; border-radius: 20px">

                            <span style="color: #ffffff; font-weight: 300;">
                                <a href="{{$link['link']}}" style="text-decoration: none; color:#ffffff; font-weight: 300;">
                                    <singleline label="button left (layout-1/2) TB14">{{$link['label']}}</singleline>
                                </a>
                            </span>
                        </td>
                        <br><br><br>
                    @endif
                </tr>
            </table>
        </td>
    </tr>

</layout>
