<!--START FOOTER LAYOUT-->
<tr>
    <td valign="top">
        <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">


            <!-- START CONTAINER  -->
            <tr>
                <td align="center" valign="top">

                    <!-- start footer container -->
                    <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container">

                        <tr>
                            <td valign="top">


                                <!-- start footer -->
                                <table width="560" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width">

                                    <!-- start space -->
                                    <tr>
                                        <td valign="top" height="20">
                                        </td>
                                    </tr>
                                    <!-- end space -->

                                    <tr>
                                        <td valign="middle">

                                            <table align="left" border="0" cellspacing="0" cellpadding="0" class="container2">

                                                <tr>
                                                    <!--<td align="center" valign="top">
                                                        <a href="{{$config['enterpriseInfo']['website']}}" style="text-decoration: none;"><img editable="" label="footer logo TB120" src="{{$config['enterpriseInfo']['logo']}}" width="114" style="max-width:114px;" alt="Logo" border="0" hspace="0" vspace="0">
                                                        </a>
                                                    </td>-->

                                                </tr>

                                                <!-- start space -->
                                                <tr>
                                                    <td valign="top" class="increase-Height-20">
                                                    </td>
                                                </tr>
                                                <!-- end space -->

                                            </table>

                                            <!--start content nav -->
                                            <table border="0" align="right" cellpadding="0" cellspacing="0" class="container2">


                                                <!--start call us -->
                                                <tr>
                                                    <td valign="middle" align="center">

                                                        <table align="right" border="0" cellpadding="0" cellspacing="0" class="clear-align" style="height:100%;">
                                                            <tr>
                                                                <td style="font-size: 13px;  line-height: 18px; color: {{$config['mainColor']}};  font-weight:300; text-align: center; font-family:Roboto,Open Sans,Arail,Tahoma, Helvetica, Arial, sans-serif;">


                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <!--end call us -->

                                            </table>
                                            <!--end content nav -->

                                        </td>
                                    </tr>

                                    <!-- start space -->
                                    <tr>
                                        <td valign="top" height="20">
                                        </td>
                                    </tr>
                                    <!-- end space -->

                                </table>
                                <!-- end footer -->
                            </td>
                        </tr>
                    </table>
                    <!-- end footer container -->

                </td>
            </tr>


            <!-- END CONTAINER  -->

        </table>
    </td>
</tr>
<!--END FOOTER ​LAYOUT-->

<tr>
    <td align="center" valign="top" style="background-color:{{$config['mainColor']}};">
        <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color:{{$config['mainColor']}};">
            <tr>
                <td valign="top">
                    <table width="560" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color:{{$config['mainColor']}};">

                        <!--start space height -->
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <!--end space height -->

                        <tr>
                            <!-- start COPY RIGHT content -->
                            <td valign="top" style="font-size: 13px; line-height: 22px; font-family:Roboto,Open Sans, Arial,Tahoma, Helvetica, sans-serif; color:#ffffff; font-weight:300; text-align:center; ">
                                <singleline label="company (footer) TB121">{{$config['enterpriseInfo']['copyright']}}</singleline>
                            </td>
                            <!-- end COPY RIGHT content -->
                        </tr>

                        <!--start space height -->
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <!--end space height -->


                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
<!--  END FOOTER COPY RIGHT -->
