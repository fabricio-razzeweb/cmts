@if (isset($bigImage))
    <layout label='header image'>
        <!--START IMAGE HEADER LAYOUT-->
        <tr>
            <td align="center" valign="top" class="fix-box">

                <!-- start HEADER LAYOUT-container width 600px -->
                <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width">
                    <tr>
                        <td valign="top" class="image-100-percent">

                            <img editable="" label="header image TB16" src="{{$bigImage}}" width="600" alt="header-image" style="display:block !important;  max-width:600px;">

                        </td>
                    </tr>
                </table>
                <!-- end HEADER LAYOUT-container width 600px -->
            </td>
        </tr>

        <!--END IMAGE HEADER LAYOUT-->
    </layout>
@endif
