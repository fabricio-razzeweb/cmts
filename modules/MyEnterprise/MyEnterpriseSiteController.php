<?php
namespace Modules\MyEnterprise;

use Illuminate\Http\Request;
use Modules\MyEnterprise\MyEnterprise;
use Illuminate\Routing\Controller as BaseController;
use Modules\DigitalMarketing\ViewCounter\ViewCounter;

class MyEnterpriseSiteController extends BaseController
{
    protected $model;

    public function __construct(MyEnterprise $model, ViewCounter $viewCounter)
    {
        $this->model = $model;
        $this->viewCounter = $viewCounter;
    }

    public function index(Request $request)
    {
        $myEnterprise = $this->model->first();
        $this->viewCounter->add($request->path());
        return view('MyEnterprise::Site.about-us', compact('myEnterprise'));
    }
}
