@extends('WebSite::layouts.default')
@section('content')
    <section class="section section-default">
        <div class="section-title text-center">
            <h3 class="section-title-item">Histórico</h3>
        </div>
        <p>
            {!!$myEnterprise->history!!}
        </p>
    </section>
@endsection
