<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyEnterpriseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_enterprise', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email', 60)->nullable();
            $table->string('location', 256)->nullable();
            $table->string('keywords', 256)->nullable();
            $table->text('history')->nullable();
            $table->string('facebook', 256)->nullable();
            $table->string('youtube', 256)->nullable();
            $table->string('instagram', 256)->nullable();
            $table->string('color_primary', 14)->nullable();
            $table->string('color_secondary', 14)->nullable();
            $table->string('color_tertiary', 14)->nullable();
            $table->string('phone', 30)->nullable();
            $table->string('logo', 256)->default('logo.png');
            $table->datetime('deleted_at')->nullable();
            //$table->integer('users_id')->unsigned();
            //$table->foreign('users_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('my_enterprise');
    }
}
