<?php
namespace Modules\MyEnterprise;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helpers\TextAdapter;
use ImageManager;

class MyEnterprise extends Model
{

    protected $table = "my_enterprise";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone',
        'email',
        'location',
        'keywords',
        'history',
        'facebook',
        'youtube',
        'instagram',
        'color_primary',
        'color_secondary',
        'color_tertiary',
        'logo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function getFieldsNames()
    {
      return $this->fillable;
    }

}
