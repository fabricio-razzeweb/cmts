<?php
namespace Modules\MyEnterprise;

use Illuminate\Http\Request;
use Modules\MyEnterprise\MyEnterprise;
use Modules\Hospital\Doctors\Doctors;
use Modules\Core\Http\Controller as Controller;
use SEOMeta;
use OpenGraph;
use SEO;
use URL;
use DB;
class MyEnterpriseController extends Controller
{
    protected $archivesDirectoryType="imgs/";
    protected $archivesFinalDirectory="enterprise";
    protected $nameElementUI="Informações";
    protected $table="MyEnterprise";
    protected $model;

    public function __construct()
    {
        $this->model = new MyEnterprise;
        $this->doctors = DB::table('users')->where('users_type_id',2);
    }

    public function getOneForSite()
    {
        $enterprise = $this->model->first();
        $doctors = $this->doctors->get();

        $posts = $this->model->paginate(10);

        SEOMeta::setTitle(env('APP_NAME')." - "."Sobre Nós");
        SEOMeta::setDescription("Conheça a equipe de Especialistas em Mastologia que vem fazendo sucesso na região sudoeste, levando um atendimento de qualidade a milhares de conquistenses");
        SEOMeta::addKeyword(explode(" ","Imasto, Mamografia, Cancêr de Mama, Mastologia, Clínicas Mastologia em Vitória da Conquista, clínica, hospital, clínicas, hospitais, centro médico, especialistas, diretório médicos, saúde, médico, medicina privada, medicos, Artigos"));

        OpenGraph::setDescription("Conheça a equipe de Especialistas em Mastologia que vem fazendo sucesso na região sudoeste, levando um atendimento de qualidade a milhares de conquistenses");
        OpenGraph::setTitle(env('APP_NAME')." - "."Sobre Nós");
        OpenGraph::setUrl(URL::asset('artigos', env('SSL')));
        OpenGraph::addProperty('type', 'article');
        OpenGraph::addProperty('locale', 'pt-br');

        return view("Site::pages.about")
                ->with(['doctors' => $doctors,
                        'enterprise' => $enterprise]);
    }

    /**
    * Method prepareDataForTransfer()
    * This method prepare data for write and save of data
    * @param $request this is request of user (Object soon of Illuminate/Requests)
    * @param $element this is the element powered in consult of DateTimeImmutable
    * @return $element whith atributes necessary of writing in database
    */
    protected function prepareDataForTransfer($request, $element=null)
    {
        // If is Insert request
        if($element==null){

            // Create Element Ex: User, Category
            $element = $this->model;

        }

        //Check is file is seted
        if ($request->hasFile('photo')) {

            // Delete Old Photo
            parent::deleteArchive($element->photo);

            //Make this upload image
            $element->photo=$this
            ->uploadImage($request, $request->input('title'), 'photo');

            $element->photo_thumb=$this
            ->uploadImage($request, $element->photo, 'photo_thumb',true);

        }

        $fields = $this->model->getFieldsNames();
        for ($i=0; $i < count($fields); $i++) {
            $field = $fields[$i];
            $element->$field = $request->input("$field");
        }
        return $element;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $myEnterprise = $this->model->get();
        // Return response for client
        return response()->json(compact('myEnterprise'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\MyEnterpriseRequest   $request
    * @return \Illuminate\Http\Response
    */
    public function store(MyEnterpriseRequestStore  $request)
    {
        return parent::basicStore($request);
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        return parent::basicShow($id);
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
        return $this->fethOne($id);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\MyEnterpriseRequest   $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        return parent::basicUpdate($request, $id);
    }


    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        // Delete element of DB
        return parent::delete($id);
    }
}
