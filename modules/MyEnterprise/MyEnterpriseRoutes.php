<?php
Route::group(['middleware'=>['web']],function()
{
    Route::group(['prefix'=>'admin/dashboard','middleware'=>['auth_common_users']],function ()
    {
        Route::group(['prefix'=>'api','middleware'=>['jwt.auth']],function(){
            Route::put('my_enterprise/{id}', 'Modules\MyEnterprise\MyEnterpriseController@update');
            Route::get('my_enterprise', 'Modules\MyEnterprise\MyEnterpriseController@index');
        });
    });
    Route::get("historico","Modules\MyEnterprise\MyEnterpriseSiteController@index");
});
