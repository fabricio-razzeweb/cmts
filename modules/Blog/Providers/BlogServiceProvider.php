<?php

namespace Modules\Blog\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Modules\Blog\Posts\Posts;
use Modules\Blog\Posts\Policies\PostsPolicy;

class BlogServiceProvider extends ServiceProvider
{
    public $migrationsList = [
        'Categorys',
        'Posts',
        'Slides'
    ];

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Posts::class => PostsPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        foreach ($this->migrationsList as $migrationDir) {
                $this->loadMigrationsFrom(__DIR__.'/../'.$migrationDir.'/Migrations/');
        }

        $this->registerPolicies();
    }
}
