<?php

namespace Modules\Blog\Posts\Requests;

use Modules\Core\Http\JWTRequest as JWTRequest;

class PostsRequestUpdate extends JWTRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:256',
            'keywords' => 'required|max:256',
            'content' => 'required',
            'categorys_id' => 'required|integer',
        ];
    }

    public function messages()
    {
      return [
        'users_id.required'=>"Você precisa fazer login para executar está ação",
        'name.min' => 'Preencha o campo de '."Nome",
        'name.required' => 'Preencha o campo de '."Nome",
      ];
    }

}



 ?>
