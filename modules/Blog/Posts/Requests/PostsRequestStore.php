<?php

namespace Modules\Blog\Posts\Requests;

use Modules\Core\Http\JWTRequest as JWTRequest;

class PostsRequestStore extends JWTRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:256',
            'keywords' => 'required|max:256',
            'content' => 'required',
            'categorys_id' => 'required|integer',
            'photo' => 'required|image',
        ];
    }

    public function messages()
    {
      return [];
    }

}



 ?>
