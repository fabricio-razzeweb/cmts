<?php

namespace Modules\Blog\Posts;

use Modules;
use Modules\Blog\Posts\Requests\PostsRequestStore as PostsRequestStore;
use Modules\Blog\Posts\Requests\PostsRequestUpdate as PostsRequestUpdate;
use Illuminate\Http\Request;
use Modules\Blog\Posts\Posts;
use Modules\Blog\Categorys\Categorys;
use Modules\Core\Http\Controller as Controller;
use Modules\Core\Helpers\TextAdapter;
use Users;

class PostsController extends Controller
{
    protected $archivesDirectoryType="imgs/";
    protected $archivesFinalDirectory="posts";
    protected $nameElementUI="Artigo";
    protected $table="posts";
    protected $model;

    public function __construct()
    {
        $this->model = new Posts;
    }

    protected function uploadImage($request, $archivePrefixName, $archiveRequestName, $isThumb=false)
    {

        $uploadPath = $this->uploadPath.$this->archivesDirectoryType;
        $uploadPath .= $this->archivesFinalDirectory;

        if ($request->hasFile($archiveRequestName)) {

            $imageFile = $request->file($archiveRequestName);

            $image = $this->model
            ->uploadImage($imageFile,$uploadPath,$archivePrefixName,'png',$isThumb);

            return $image;
        }

        return false;

    }

    /**
    * Method prepareDataForTransfer()
    * This method prepare data for write and save of data
    * @param $request this is request of user (Object soon of Illuminate/Requests)
    * @param $element this is the element powered in consult of DateTimeImmutable
    * @return $element whith atributes necessary of writing in database
    */
    protected function prepareDataForTransfer($request, $element=null)
    {
        // If is Insert request
        if($element==null){

            // Create Element Ex: User, Category
            $element = $this->model;

        }

        //Check is file is seted
        if ($request->hasFile('photo')) {

            // Delete Old Photo
            parent::deleteArchive($element->photo);

            //Make this upload image
            $element->photo=$this
            ->uploadImage($request, $request->input('title'), 'photo');

            $element->photo_thumb=$this
            ->uploadImage($request, $element->photo, 'photo_thumb',true);

        }

        $fields = $this->model->getFieldsNames();
        for ($i=0; $i < count($fields); $i++) {
            $field = $fields[$i];
            $element->$field = $request->input("$field");
        }

        $element->users_id = Users::getAuthUser()->id;

        return $element;
    }



    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {

        // If request is one search?
        if($request->has('search')){
            //Cath search
            $search = $request->get('search');

            if($search=="*"){
                // Search all itens
                $Posts=$this->model->get();

            }else{

                // Search object in database
                $Posts = $this->model->where([["title", "LIKE", "%{$search}%"],
                ["content", "LIKE", "%{$search}%"]])
                ->get();
            }
        }else{
            // If normal request return itens paginate for id
            $Posts = $this->model->orderBy('id','desc')->paginate(15);
        }
        // Return response for client
        return response()->json(compact('Posts'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\PostsRequest   $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request  $request)
    {
        //This is data for transfer
        $post = $this->prepareDataForTransfer($request);
        $post->link = ' ';

        //Calling MODEL and insert data in the tables
        $post->save();

        //return response to client
        $execution=["status" => true,
                    "msg" => "$this->nameElementUI, cadastrado com Sucesso!",
                    'id' => $post->id];

        $post = $this->model->find($post->id);
        $post->link = TextAdapter::generateLink($post->title)."/$post->id";

        $post->save();

        //event(new Modules\Blog\Events\NewPostEvent($post));

        if ($request->has('tags') &&  $request->tags!='') {

            if (substr_count(',', $request->tags)>0) {
                $tags = explode(',', $request->tags);

                $post->tags()->sync($tags);
            }

        }

        return $this->responseForClient($execution,"/lista");
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $element = $this->model->find($id);

        return response()->json(compact('element'));
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
        return $this->fethOne($id);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\PostsRequest   $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(PostsRequestUpdate $request, $id)
    {
        //Find This element for edit
        $element=$this->model->find((int)$id);

        //This is data for transfer
        $element = $this->prepareDataForTransfer($request,$element);
        $element->link=TextAdapter::generateLink($element->title)."/$element->id";

        //Calling MODEL and update data in the tables
        $executionStatus = $element->save();

        //Make Response to client
        if ($executionStatus) {

            $msg = "$this->nameElementUI, editado com Sucesso!";

        }else{

            $msg = "Desculpe ocorreu um erro inesperado";

        }

        if ($request->has('tags') &&  $request->tags!='') {

            if (substr_count(',', $request->tags)>0) {
                $tags = explode(',', $request->tags);

                $post->tags()->sync($tags);
            }

        }

        $execution=["status"=>$executionStatus,"msg"=>$msg];

        return $this->responseForClient($execution,"/$id");
    }



    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        return parent::delete($id);
    }
}
