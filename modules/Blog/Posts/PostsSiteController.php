<?php

namespace Modules\Blog\Posts;

use Illuminate\Http\Request;
use Modules\Blog\Posts\Posts;
use Modules\Blog\Categorys\Categorys;
use Modules\Core\Http\Controller as Controller;
use Modules\DigitalMarketing\ViewCounter\ViewCounter;
use Modules\Core\Helpers\TextAdapter;
use OpenGraph;
use URL;
use Users;

class PostsSiteController extends Controller
{

    public function __construct(ViewCounter $viewCounter)
    {
        $this->model = new Posts;
        $this->categorys = new Categorys;
        $this->authors = new Users;
        $this->viewCounter = $viewCounter;
    }

    public function getAllForSite(Request $request)
    {
        $posts = $this->model->paginate(10);
        $categorys = $this->categorys->get();
        $this->viewCounter->add($request->path());

        return view('WebSite::pages.articles')
                ->with([
                        'posts'=>$posts,
                        'categorys'=>$categorys
                    ]);

    }

    public function getAllForCategoryInSite(Request $request)
    {
        $posts = $this->model
        ->where('categorys_id',$request->category_id)
        ->paginate(10);

        $categorys = $this->categorys->get();
        $this->viewCounter->add($request->path());

<<<<<<< HEAD
        return view('WebSite::pages.articles')
=======
        return view('Site::pages.articles')
>>>>>>> 7cfabc84cad7f6a6e785927b03642a6d7d8f3743
                ->with([
                        'posts'=>$posts,
                        'categorys'=>$categorys
                    ]);

    }

    public function getOneForSite(Request $request)
    {
        $post = $this->model->with(['category'])->find($request->id);
        $categorys = $this->categorys->get();
        $posts = $this->model->where('id', '!=', $post->id)->get();
        $author = $this->authors->find($post->users_id);

<<<<<<< HEAD
        $this->viewCounter->add($request->path());

=======
>>>>>>> 7cfabc84cad7f6a6e785927b03642a6d7d8f3743
        return view('WebSite::pages.article')
                ->with([
                        'posts'=>$posts,
                        'post'=>$post,
                        'categorys'=>$categorys,
                        'author'=>$author,
                    ]);
    }
}
