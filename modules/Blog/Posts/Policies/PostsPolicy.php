<?php

namespace Modules\Blog\Posts\Policies;

use Modules\Users\Users;
use Modules\Blog\Posts\Posts;
use Illuminate\Auth\Access\HandlesAuthorization;
use DB;

class PostsPolicy
{
    use HandlesAuthorization;

    public function like(Posts $post)
    {
        $likes = DB::table('posts_likes')
        ->where('users_id', Users::getAuthUserByJWT()->id)
        ->where('posts_id', $post->id)
        ->count();

        return ($likes === 0);
    }
}
