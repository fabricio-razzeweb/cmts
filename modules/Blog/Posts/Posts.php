<?php

namespace Modules\Blog\Posts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Modules\Core\Helpers\TextAdapter;
use ImageManager;
use Modules\Users\Users;

class Posts extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'title',
      'keywords',
      'content',
      'categorys_id',
      'link',
      'users_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function getFieldsNames()
    {
        return $this->fillable;
    }

    public function uploadImage($imageFile, $path, $imageName, $extensionForSave = 'png', $isThumb)
    {
        $image = $imageFile;

        $archivePrefixName = TextAdapter::generateLink($imageName);

        if ($isThumb) {
            $widthForSave = 200;
            $optimizeSize = 20;
            $imageName = explode(".", $imageName)[0];
            $imageName = $imageName."-thumb.";
        } else {
            $widthForSave = 500;
            $optimizeSize = 10;
            $imageName = $archivePrefixName."-".md5(date("y-M-d:h:m:s")).".";
        }

        $imageName .= "$extensionForSave";

        $imageSave = ImageManager::make($image)
        ->resize($widthForSave, null, function ($constraint) {

            $constraint->aspectRatio();
        })
        ->encode($extensionForSave, $optimizeSize)
        ->save($path."/".$imageName, $optimizeSize);

        return $imageName;
    }

    // public function tags()
    // {
    //     return $this->belongsToMany(\Modules\Blog\Tags\Tags::class);
    // }

    public function category()
    {
        return $this->belongsTo(\Modules\Blog\Categorys\Categorys::class, 'categorys_id');
    }

    public function user()
    {
        return $this->belongsTo(\Modules\Users\Users::class, 'users_id');
    }

    public function countLikes()
    {
        $this->likes = DB::table('posts_likes')
        ->where('posts_id', $this->id)
        ->count();

        $this->liked = !policy($this)->like($this);

        return $this;
    }

    /**
     * This method like this post
     * @return boolean with result of execution
     */
    public function likePost()
    {
        $user = Users::getAuthUserByJwt();

        if (policy($this)->like($this)) {

            return DB::table('posts_likes')
            ->insert(['posts_id' => $this->id,
                        'users_id' => $user->id]);

        }else{

            return false;

        }
    }

    /**
     * This method add view in this post
     * @return boolean with result of execution
     */
    public function addViewInPost()
    {
        $user = Users::getAuthUserByJwt();

        if ($user) {
            $userId = $user->id;
        } else {
            $userId = 0;
        }

        DB::table('posts_views')->insert([
            'posts_id' => $this->id,
            'users_id' => $userId,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        return $this;
    }

    public function countViews()
    {
        $this->views = DB::table('posts_views')
        ->where('posts_id', $this->id)
        ->count();

        return $this;
    }
}
