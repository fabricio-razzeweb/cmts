<?php

namespace Modules\Blog\Posts;

use Illuminate\Http\Request;
use Modules\Blog\Posts\Posts;
use Modules\Blog\Categorys\Categorys;
use Modules\Core\Http\Controller as Controller;
use Modules\Core\Helpers\TextAdapter;
use Users;

class PostsAppController extends Controller
{
    protected $resourcesPath;
    protected $nameElementUI = "Artigo";
    protected $table="posts";
    protected $model;

    public function __construct()
    {
        $this->resourcesPath = env('APP_URL')."src/imgs/posts/";
        $this->model = new Posts;
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {

        // If request is one search?
        if($request->has('search')){
            //Cath search
            $search = $request->get('search');

            if($search=="*"){
                // Search all itens
                $posts=$this->model->get();

            }else{

                // Search object in database
                $posts = $this->model->with('user')
                ->where([
                    ["title", "LIKE", "%{$search}%"],
                    ["content", "LIKE", "%{$search}%"]
                ])
                ->paginate(15);
            }
        }else{
            // If normal request return itens paginate for id
            $posts = $this->model->with('user')
            ->orderBy('id','desc')->paginate(15);
        }

        foreach ($posts as $post) {
            $post->countLikes()->countViews();
            $post->photo = $this->resourcesPath.$post->photo;
            $post->created_at_edited = $post->created_at->format('d M Y');
        }

        // Return response for client
        return response()->json(compact('posts'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {}

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\PostsRequest   $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request  $request)
    {}

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $post = $this->model
                    ->with('tags')
                    ->find($id)
                    ->addViewInPost()
                    ->countViews()
                    ->countLikes();

        $post->photo = $this->resourcesPath.$post->photo;

        return response()->json(compact('post'));
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
        return $this->fethOne($id);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\PostsRequest   $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(PostsRequestUpdate $request, $id)
    {}

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $this->model->find($id)
        ->tags()->delete();

        // Delete element of DB
        return parent::delete($id);
    }

    /**
    * Like An specifed post
    * @param int id
    * @return \Illuminate\Http\Response
    */
    public function likePostById($id)
    {
        $execution = [];

        $execution['status'] = $this->model
                                    ->find($id)
                                    ->likePost();

        return response()->json(compact('execution'));
    }

}
