<?php
namespace Modules\Blog\Slides;

use Modules\Core\Support\Contracts\RepositoryInterface;
use Modules\Core\Support\BaseRepository;
use Modules\Blog\Slides\Slides;

class SlidesRepository extends BaseRepository implements RepositoryInterface
{

    /**
    * This attribute is the model of repository
    * @var Modules\Modules\Blog\Slides
    */
    protected $modelClass;

    public function __construct(Slides $model)
    {
        $this->modelClass = $model;
    }
}
