<?php

namespace Modules\Blog\Slides\Requests;

use Modules\Core\Http\JWTRequest as JWTRequest;

class SlidesRequestUpdate extends JWTRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:256',
        ];
    }

    public function messages()
    {
      return [];
    }

}



 ?>
