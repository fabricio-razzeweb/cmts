<?php

namespace Modules\Blog\Slides\Requests;

use Modules\Core\Http\JWTRequest as JWTRequest;

class SlidesRequestStore extends JWTRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'photo' => 'required|image',
        ];
    }

    public function messages()
    {
      return [];
    }

}



 ?>
