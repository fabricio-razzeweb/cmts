<?php

namespace Modules\Blog\Slides;

use Modules\Blog\Slides\Requests\SlidesRequestStore as SlidesRequestStore;
use Modules\Blog\Slides\Requests\SlidesRequestUpdate as SlidesRequestUpdate;
use Illuminate\Http\Request;
use Modules\Blog\Slides\Slides;
use Modules\Core\Support\RestAPI\BaseControllerRestAPI;
use Modules\Blog\Slides\SlidesRepository;

class SlidesController extends BaseControllerRestAPI
{
    protected $model;
    protected $nameElementUI = "Slides";
    protected $resourceName = "slide";
    protected $resourceGroupName = "slides";

    public function __construct(Slides $model, SlidesRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    public function store(SlidesRequestStore $request)
    {
        return parent::genericStore($request);
    }

    public function update(SlidesRequestUpdate $request, int $id)
    {
        return parent::genericUpdate($request, $id);
    }
}
