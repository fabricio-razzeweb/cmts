<?php

namespace Modules\Blog\Slides;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Modules\Core\Helpers\TextAdapter;
use ImageManager;
use Modules\Core\Support\SavePhoto;

class Slides extends Model
{
    use SoftDeletes, SavePhoto;

    public $timestamps = false;
    protected $resourcesPath = "src/imgs/slides/";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'title',
      'photo',
      'btn_content',
      'btn_link',
      'users_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function getPhotoAttribute($value)
    {
        return env('APP_URL').'/'.$this->resourcesPath.$value;
    }

    public function prepareDataToSave(array $data)
    {
        if (isset($data['photo'])) {
             $data['photo'] = $this->savePhoto($data['photo'], $data['title']);
        }

        $this->fill($data);

        return $this;
    }
}
