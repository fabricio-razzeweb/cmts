<?php
namespace Modules\Blog\Categorys;

use Modules\Core\Support\Contracts\RepositoryInterface;
use Modules\Core\Support\BaseRepository;
use Modules\Blog\Categorys\Categorys;

class CategorysRepository extends BaseRepository implements RepositoryInterface
{

    /**
    * This attribute is the model of repository
    * @var Modules\Modules\Blog\Categorys
    */
    protected $modelClass;

    public function __construct(Categorys $model)
    {
        $this->modelClass = $model;
    }
}
