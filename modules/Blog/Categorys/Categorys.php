<?php
namespace Modules\Blog\Categorys;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Modules\Core\Support\PrepareDataToSave;

class Categorys extends Model
{
    use SoftDeletes, PrepareDataToSave;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'name',
      'users_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function posts()
    {
        return $this->hasMany(\Modules\Blog\Posts\Posts::class, 'categorys_id');
    }
}
