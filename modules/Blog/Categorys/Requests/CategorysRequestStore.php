<?php
namespace Modules\Blog\Categorys\Requests;

use Modules\Core\Http\JWTRequest as JWTRequest;

class CategorysRequestStore extends JWTRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'name' => 'required|max:80|min:5',
          'users_id' => 'required',
        ];
    }

    public function messages()
    {
      return [
        'users_id.required'=>"Você precisa fazer login para executar está ação",
        'name.min' => 'Preencha o campo de Nome',
        'name.required' => 'Preencha o campo de Nome',
      ];
    }

}
