<?php
namespace Modules\Blog\Categorys;

use Modules\Blog\Categorys\Requests\CategorysRequestStore;
use Modules\Blog\Categorys\Requests\CategorysRequestUpdate;
use Illuminate\Http\Request;
use Modules\Blog\Categorys\Categorys;
use Modules\Core\Support\RestAPI\BaseControllerRestAPI;
use Modules\Blog\Categorys\CategorysRepository;

class CategorysController extends BaseControllerRestAPI
{
    protected $model;
    protected $nameElementUI = "Categorias";
    protected $resourceName = "category";
    protected $resourceGroupName = "categorys";

    public function __construct(Categorys $model, CategorysRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    public function store(CategorysRequestStore $request)
    {
        return parent::genericStore($request);
    }

    public function update(CategorysRequestUpdate $request, int $id)
    {
        return parent::genericUpdate($request, $id);
    }
}
