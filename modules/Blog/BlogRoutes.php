<?php
Route::group(['middleware'=>['web']],function()
{

    Route::get('admin','Modules\Users\LoginController@index');
    Route::get('noticias', 'Modules\Blog\Posts\PostsSiteController@getAllForSite');
    Route::get("noticias/{title}/{id}","Modules\Blog\Posts\PostsSiteController@getOneForSite");

    Route::post('admin/auth',['as'=>'authUser', 'uses'=>'Modules\Users\LoginController@auth']);

    Route::group(['prefix'=>'admin/dashboard','middleware'=>['auth_common_users']],function ()
    {

        Route::get('','Modules\Users\UsersDashboardController@index');
        Route::get('logout','Modules\Users\LoginController@logout');

        Route::group(['prefix'=>'api','middleware'=>['jwt.auth']],function(){

            //Route::resource('tags', 'Modules\Blog\Tags\TagsController');
            Route::resource('categorys', 'Modules\Blog\Categorys\CategorysController');
            Route::resource('posts', 'Modules\Blog\Posts\PostsController');
            Route::resource('depoiments', 'Modules\Blog\Depoiments\DepoimentsController');
            Route::resource('services', 'Modules\Blog\Services\ServicesController');
            Route::resource('slides', 'Modules\Blog\Slides\SlidesController');
            Route::resource('gallerys', 'Modules\Blog\Gallerys\GallerysController');
            Route::resource('messages', 'Modules\Messages\MessagesController');
            Route::resource('takeDoubts', 'Modules\takeDoubts\takeDoubtsController');
            Route::resource('users', 'Modules\Users\UsersController');
            Route::get('users/options/register', 'Modules\Users\UsersController@getOptions');

        });

    });

});
