<?php
namespace Modules\Blog\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Blog\Posts\Posts;

class NewPostEvent
{
    use SerializesModels;

    public $post;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Posts $post)
    {
        $this->post = $post;
    }
}
