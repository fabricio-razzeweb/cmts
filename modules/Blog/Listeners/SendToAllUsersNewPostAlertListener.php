<?php
namespace Modules\Blog\Listeners;

use Modules\Blog\Events\NewPostEvent;
use Modules\Newsletter\Jobs\NewsletterNotificationsJobs;
use Carbon\Carbon;
use Modules\Newsletter\NewsletterNotificationsUsersInQueue;
use Modules\Newsletter\NewsletterNotifications;
use Modules\Users\Users;

class SendToAllUsersNewPostAlertListener
{
    protected function fireQueueForSendNotificationsOfNewsletter($newsletter)
    {
        $horaryOfSend = new Carbon($newsletter->sended_at);

        $job = (new NewsletterNotificationsJobs($newsletter))
                ->onQueue('newsletter_notifications')
                ->delay($horaryOfSend);

        return dispatch($job);
    }

    protected function processUsersIdsLists(array $usersIdsLists, int $notificationsId)
    {
        $userListInQueue = [];

        foreach ($usersIdsLists as $userId) {
            $userInQueue = new NewsletterNotificationsUsersInQueue();
            $userInQueue->users_id = $userId;
            $userInQueue->notifications_id = $notificationsId;

            $userListInQueue[] = $userInQueue;
        }

        return $userListInQueue;
    }

    public function storeNewsletterListAndSend()
    {
        $execution = [];

        $usersModel = new Users;
        $newsletterModel = new NewsletterNotifications;

        $usersLists = $usersModel
        ->where('users_type_id', Users::USER_DEFAULT_TYPES['users'])
        ->get();

        $usersIdsLists = [];

        foreach ($usersLists as $user) {
            $usersIdsLists[] = $user->id;
        }

        $newsletter = $newsletterModel->fill([
            'title' => $this->post->title,
            'content' => "Veja agora!",
        ]);

        $formatedSendedAt = Carbon::now()->addMinute()
        ->format('Y-m-d H:i:s');

        $newsletter->sended_at = $formatedSendedAt;
        $newsletter->is_email = true;
        $newsletter->is_pushnotification = true;

        $saveNewsletterExecution = $newsletter->save();

        $usersIdsLists = $this->processUsersIdsLists($usersIdsLists, $newsletter->id);
        $registerUsersInQueueExecution = $newsletter->usersInQueue()
        ->saveMany($usersIdsLists);

        $this->fireQueueForSendNotificationsOfNewsletter($newsletter);

        $execution['status'] = $saveNewsletterExecution && $registerUsersInQueueExecution;

        if ($execution['status']) {
            $execution['message'] = "Ordem de envio cadastrada com sucesso!";
        } else {
            $execution['message'] = "Ocorreu um erro ao cadastrar a ordem de envio";
        }

        return $execution;
    }

    /**
    * Handle the event.
    *
    * @param  NewPostEvent  $event
    * @return void
    */
    public function handle(NewPostEvent $event)
    {
        $this->post = $event->post;
        return $this->storeNewsletterListAndSend();
    }
}
