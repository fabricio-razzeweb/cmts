<?php

namespace Modules\Core\Support\Tests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use JWTAuth;
use Modules\Users\Users;
use Carbon\Carbon;

abstract class HttpRoutesAPITests extends TestCase
{

    protected $basePath;
    protected $userAuthId = 1;

    /**
     * this method get auth user
     * @return instance of User
     */
    public function getAuthUser():Users
    {
        return Users::find($this->userAuthId);
    }

    /**
     * Return request headers needed to interact with the API.
     *
     * @return Array array of headers.
     */
    protected function setHeaders($user = null):array
    {
        $headers = ['Accept' => 'application/json'];

        if (!is_null($user)) {
            $token = JWTAuth::fromUser($user);
            JWTAuth::setToken($token);
            $headers['Authorization'] = 'Bearer '.$token;
        }

        return $headers;
    }

    public function getHeaders():array
    {
       return $this->setHeaders($this->getAuthUser());
    }

    /**
     * Test if route exists
     */
    public function testCheckIfRouteExits()
    {
        $this->get($this->basePath, $this->getHeaders())
        ->assertStatus(200);
    }
}
