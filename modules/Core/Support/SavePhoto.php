<?php
namespace Modules\Core\Support;

use Modules\Core\Support\Storage\ImageStorageManager;

trait SavePhoto
{
    public function savePhoto($photo, string $prefixName, int $width = null, int $height = null, int $conpressionLevel = 30)
    {
        $storageManager = new ImageStorageManager;

        $exec =  $storageManager->setFile($photo)
                ->setFilePrefixName($prefixName);

        if ($width && $height) {
            $exec->setDimensions($width, $height);
        }

        return $exec->setQualityOfCompresion($conpressionLevel)
        ->setPathForStore($this->resourcesPath)
        ->setExtension('png')
        ->store();
    }
}
