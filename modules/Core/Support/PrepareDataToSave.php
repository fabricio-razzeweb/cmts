<?php
namespace Modules\Core\Support;


trait PrepareDataToSave
{
    public function prepareDataToSave(array $data)
    {
        return $this->fill($data);
    }
}
