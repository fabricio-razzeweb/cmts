<?php
namespace Modules\Core\Support\Contracts;

interface RepositoryInterface
{

    public function find(int $id, $columns = ['*'], $callback = null);

    public function findBy(array $conditionals, array $columns = ['*']);

    public function getWhere(array $conditionals, array $columns = ['*']);

    public function all(array $columns = ['*'], $callback = null);

    public function list(int $paginate, array $colunms = ['*'], $callback = null);

    public function save(array $data):bool;

    public function update(int $id, array $data):bool;

    public function delete(int $id):bool;

    public function search(array $queryParams);

    public function newQuery();
}
