<?php
namespace Modules\Core\Support\RestAPI;

use Modules\Core\Http\Controller as Controller;
use Illuminate\Http\Request;
use Modules\Core\Http\JWTRequest;
use Modules\Core\Support\RestAPI\GenericIndexQueryable;
use Modules\Core\Support\RestAPI\GenericStoreRequest;
use Modules\Core\Support\RestAPI\GenericUpdateRequest;
use Modules\Core\Support\RestAPI\GenericShowRequest;
use Modules\Core\Support\RestAPI\GenericDeleteRequest;

abstract class BaseControllerRestAPI extends Controller
{
    use GenericIndexQueryable,
        GenericStoreRequest,
        GenericUpdateRequest,
        GenericShowRequest,
        GenericDeleteRequest;

    protected $model;
    protected $repository;
    protected $nameElementUI;
    protected $resourceName;
    protected $resourceGroupName;
    protected $defaultExecutionMsgs = [
            'update' => "Editado(a) com sucesso!",
            'store' => "Salvo(a) com sucesso!",
            'delete' => "Deletado Com sucesso!",
            'error' => "Desculpe ocorreu um erro ao executar está atividade"
    ];

    protected function sendResponse(array $result)
    {
        return response()->json($result);
    }

    protected function checkIfUserLoggedHasPermissionForQueryableURLs()
    {
        return true;
    }
}
