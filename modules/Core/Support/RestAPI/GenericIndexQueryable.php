<?php
namespace Modules\Core\Support\RestAPI;

use Illuminate\Http\Request;

trait GenericIndexQueryable{

    protected function processURLQuery(Request $request)
    {
        $querys = explode("&", $request->where);

        $querysProcessed = [];

        foreach ($querys as $params) {
            $querysProcessed[] = explode('|', $params);
        }

        return $querysProcessed;
    }

    protected function processSearchURLQuery(Request $request)
    {
        $searchQuery = $request->search;
        $searchQueryExploded = explode('|', utf8_encode($searchQuery));

        $searchQueryExplodedColunms = $searchQueryExploded[0];
        $searchQueryExplodedValue = $searchQueryExploded[1];

        return ['columns' => $searchQueryExplodedColunms, 'value' => $searchQueryExplodedValue];
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $result = [];
        $userCanEntryQuerysInURL = $this->checkIfUserLoggedHasPermissionForQueryableURLs();

        if ($userCanEntryQuerysInURL && $request->has('where')) {

            $conditionalsProcessed = $this->processURLQuery($request);
            $executionResult = $this->repository->getWhere($conditionalsProcessed);

        } if ($userCanEntryQuerysInURL && $request->has('search')) {

            $queryProcessed = $this->processSearchURLQuery($request);
            $executionResult = $this->repository->search($queryProcessed);

        } if (!$request->has('search') && !$request->has('where')) {
             $executionResult = $this->repository
             ->newQuery()
             ->orderBy('id', 'desc')
             ->paginate(20);
        }

        $result["$this->resourceGroupName"] = $executionResult;

        return $this->sendResponse($result);
    }

}
