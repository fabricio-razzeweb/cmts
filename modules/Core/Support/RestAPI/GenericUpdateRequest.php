<?php
namespace Modules\Core\Support\RestAPI;

use Illuminate\Http\Request;

trait GenericUpdateRequest{

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request   $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function genericupdate(Request $request, int $id)
    {
        $execution = [];
        $execution['status'] = $this->repository->update($id, $request->all());

        if ($execution['status']) {
            $execution['msg'] = $this->defaultExecutionMsgs['update'];
        }else{
            $execution['msg'] = "Desculpe ocorreu um erro ao executar está atividade";
        }

        return $this->sendResponse(compact('execution'));
    }
}
