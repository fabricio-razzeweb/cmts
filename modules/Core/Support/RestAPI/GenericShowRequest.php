<?php
namespace Modules\Core\Support\RestAPI;

trait GenericShowRequest{

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show(int $id)
    {
        $result = [];
        $result["$this->resourceName"] = $this->repository->find($id);

        return $this->sendResponse($result);
    }

}
