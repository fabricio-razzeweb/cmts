<?php
namespace Modules\Core\Support\RestAPI;

trait GenericDeleteRequest{

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(int $id)
    {
        $execution = [];
        $execution['status'] = $this->repository->delete($id);

        return $this->sendResponse(compact('execution'));
    }
}
