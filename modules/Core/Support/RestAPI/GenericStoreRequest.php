<?php
namespace Modules\Core\Support\RestAPI;

use Illuminate\Http\Request;

trait GenericStoreRequest{

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function genericStore(Request $request)
    {
        $execution = [];
        $execution['status'] = $this->repository->save($request->all());

        if ($execution['status']) {
            $execution['msg'] = $this->defaultExecutionMsgs['store'];
        }else{
            $execution['msg'] = "Desculpe ocorreu um erro ao executar está atividade";
        }

        return $this->sendResponse(compact('execution'));
    }
}
