<?php
namespace Modules\Core\Support\Storage;

use Intervention\Image\Facades\Image as ImageManager;
use Modules\Core\Helpers\TextAdapter;

/*
 * Class ImageStorageManager
 * @author Fabricio Magalhães Sena
 * Está Classe foi criada para Minificar e Guardar Imagens
 * Documentação em http://image.intervention.io/
*/

class ImageStorageManager
{
    public $file;
    public $filePrefixName;
    public $width;
    public $height;
    public $path;
    public $qualityOfCompresion;
    public $extension;

    public function setFile($imageFile)
    {
        $this->file = $imageFile;

        return $this;
    }

    public function setFilePrefixName(string $filePrefixName)
    {
        $this->filePrefixName = $filePrefixName;

        return $this;
    }

    public function setDimensions(int $width = 0, int $height = 0)
    {
        $this->width = $width;
        $this->height = $height;

        return $this;
    }

    public function setQualityOfCompresion(int $qualityOfCompresion)
    {
        $this->qualityOfCompresion = $qualityOfCompresion;

        return $this;
    }

    public function setPathForStore(string $path)
    {
        $this->path = $path;

        return $this;
    }

    public function setExtension(string $extension = "png")
    {
        $this->extension = (isset($this->file->extension))? $this->file->extension() : $extension;

        return $this;
    }

    public function generateImageName()
    {
        $this->fileName = TextAdapter::generateLink($this->filePrefixName)."-".md5(date("y-M-d:h:m:s")).".".$this->extension;

        return $this;
    }

    public function processAndstore()
    {
        $this->generateImageName();

        $imageSave = ImageManager::make($this->file);// Pega imagem;

        if ($this->width) {
            $imageSave = $imageSave->resize($this->width, null, function ($constraint) {
                $constraint->aspectRatio(); //Mantem aspecto Radial
            })->crop($this->width, $this->height);
        }

        $imageSave
        ->encode($this->extension, $this->qualityOfCompresion) // Converte e minifica
        ->save(public_path($this->path.$this->fileName) , $this->qualityOfCompresion); // Minifica e Salva

        return $this->fileName;
    }

    public function store()
    {
        return $this->processAndstore();
    }
}
