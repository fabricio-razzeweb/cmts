<?php
namespace Modules\Core\Support;

use Modules\Core\Support\Contracts\RepositoryInterface;

abstract class BaseRepository implements RepositoryInterface
{

   /**
    * This attribute is the model of repository
    * @var Object
    */
    protected $modelClass;

    protected function associateModelWithEssentialRelationship($model = null)
    {
        if (isset($model)) {
            return $model;
        } else {
            return $this->modelClass;
        }
    }

    public function find(int $id, $columns = ['*'], $callback = null)
    {

        $element = $this->associateModelWithEssentialRelationship($this->modelClass)
        ->where($this->modelClass->getTable().".id", $id)
        ->select($columns);

        if ($callback) {
            $element = $callback($element);
        }

        $element = $element->first();

        return $element;
    }

    public function findBy(array $conditionals, array $columns = ['*'])
    {
        return $this->associateModelWithEssentialRelationship($this->modelClass)
        ->where($conditionals)->select($columns)->first();
    }

    public function getWhere(array $conditionals, array $columns = ['*'])
    {
        return $this->associateModelWithEssentialRelationship($this->modelClass)
        ->select($columns)->where($conditionals)->get();
    }

    public function all(array $columns = ['*'], $callback = null)
    {
        $elements = $this->associateModelWithEssentialRelationship($this->modelClass);

        if ($callback) {
            $elements = $callback($elements);
        }

        return $elements->get($columns);
    }

    public function list(int $paginate, array $colunms = ['*'], $callback = null)
    {
        $elements = $this->all($colunms,
        function ($query) {

            if ($callback) {
                $query = $callback($query);
            }

            $query->paginate($paginate);
        });

        return $elements;
    }

    public function save(array $data):bool
    {
        return $this->modelClass->prepareDataToSave($data)->save();
    }

    public function update(int $id, array $data):bool
    {
        return $this->modelClass->findOrFail($id)->prepareDataToSave($data)->save();
    }

    public function delete(int $id):bool
    {
        return $this->modelClass->find($id)->delete();
    }

    public function search(array $queryParams)
    {
        $query = $this->associateModelWithEssentialRelationship($this->modelClass);

        $selectAllItens = ($queryParams['value'] === "*");

        if ($selectAllItens) {
            $query->where('id', '>', 0);
        } else {
            if (count($queryParams['columns']) > 1) {
                foreach ($queryParams['columns'] as $columnName) {
                    $query = $query->orWhere($columnName, 'LIKE', $queryParams['value']);
                }
            } else {
                $query = $query->orWhere($queryParams['columns'], 'LIKE', $queryParams['value']);
            }

        }

        return $query->get();
    }

    public function newQuery()
    {
        return $this->modelClass;
    }

    public function getModel()
    {
        return $this->modelClass;
    }
}
