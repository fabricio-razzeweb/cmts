<?php

namespace Modules\Core;

use Illuminate\Support\ServiceProvider;

class ModulesProvider extends ServiceProvider
{

    protected $modulesDirectoryLocation = __DIR__;
    protected $pathDistance = "/../";
    /**
    * Bootstrap the application services.
    *
    * @return void
    */
    public function boot()
    {
        // For each of the registered modules, include their routes and Views
        $modules = config("modules.modules");

        while (list(,$module) = each($modules)) {

            // Load the routes for each of the modules
            if(file_exists($this->modulesDirectoryLocation.$this->pathDistance.$module.'/'.ucfirst($module).'Routes.php')) {
                include $this->modulesDirectoryLocation.$this->pathDistance.$module.'/'.ucfirst($module).'Routes.php';
            }


            // Load the views
            if(is_dir($this->modulesDirectoryLocation.$this->pathDistance.$module.'/Views')) {
                $this->loadViewsFrom($this->modulesDirectoryLocation.$this->pathDistance.$module.'/Views', $module);
            }

            // Load the migrations for each of the modules
            if(is_dir($this->modulesDirectoryLocation.$this->pathDistance.$module.'/Migrations')) {
                $this->publishes( [$this->modulesDirectoryLocation.$this->pathDistance.$module.'/Migrations'=>database_path("migrations")],'migrations');
            }

        }

    }

    /**
    * Register the application services.
    *
    * @return void
    */
    public function register()
    {
        //
    }
}
