<?php

namespace Modules\Core\Http;

use Illuminate\Foundation\Http\FormRequest;

class JWTRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

}
