<?php

namespace Modules\Core\Http\Auth;

use Closure;
use Config;
use Illuminate\Support\Facades\Auth;
use Modules\Users\Users;

abstract class AuthenticateMiddlewareWithJWTMultiAuth
{
    /**
    * The auth guard of user
    *
    * @var string
    */
    protected $guardForCheck;

    /**
    * The url for redirect user if not logged or not accepted
    *
    * @var string
    */
    protected $loginUrl;

    /**
    * The url for redirect user if logged
    *
    * @var string
    */
    protected $urlRedirectToAfterAuth;

    /**
    * The class of user authenticated
    *
    * @var string
    */
    protected $usersClassType;

    /**
    * The type of User for check
    *
    * @var string
    */
    protected $usersTypeIdAccepted;

    /**
    * Method configJWTAuth()
    *
    * @return void
    */
    public function configJWTAuth()
    {
        Config::set('jwt.user', $this->usersClassType);
        Config::set('auth.providers.users.model', $this->usersClassType);
    }

    /**
    * Method checkUserIsAcceptedAfterLogged()
    * This method check user is accepted
    * @param $user is istance of Users Object
    * @return boolean
    */
    public function checkUserIsAcceptedAfterLogged($user)
    {
        return ($user->users_type_id === $this->usersTypeIdAccepted);
    }

    /**
    * Method guard()
    * return guard is used for check user is logged
    * @return object
    */
    public function guard()
    {
        $userModel = new $this->usersClassType;

        return Auth::guard($userModel->guardName);
    }

    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @param  string|null  $guard
    * @return mixed
    */
    public function handle($request, Closure $next, $guard = null)
    {
        $execution = [];

        //If user is logged
        if ($this->guard()->check() &&
            $this->checkUserIsAcceptedAfterLogged($this->guard()->user())) {

            $this->configJWTAuth();

            if ($this->guard()->user()->is_active === 1) {
                $execution['alertMsg'] = 'Por favor confirme seu email!';
            }

            return $next($request)
                        ->with($execution);

        }else{
            $execution['msgError'] = "Área restrita, por favor faça login para poder acessa-la";
        }

        return redirect($this->loginUrl)
        ->with($execution);;
    }
}
