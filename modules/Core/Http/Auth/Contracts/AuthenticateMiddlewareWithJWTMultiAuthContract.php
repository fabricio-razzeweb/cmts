<?php
namespace Modules\Core\Http\Auth\Contracts;

use Modules\Users\Users;
use Closure;
use Illuminate\Support\Facades\Auth;

interface AuthenticateMiddlewareWithJWTMultiAuthContract{

    /**
    * Method configJWTAuth()
    *
    * @return void
    */
    public function configJWTAuth();

    /**
    * Method checkUserIsAcceptedAfterLogged()
    * This method check user is accepted
    * @param $user is istance of Users Object
    * @return void
    */
    public function checkUserIsAcceptedAfterLogged($user);

    /**
    * Method guard()
    * return guard is used for check user is logged
    * @return void
    */
    public function guard();

    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @param  string|null  $guard
    * @return mixed
    */
    public function handle($request, Closure $next, $guard = null);
}
