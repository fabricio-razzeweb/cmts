<?php

namespace Modules\Core\Http\Auth;

use Modules\Users\Users;

use JWTAuth;//use in jwt
use Tymon\JWTAuth\Exceptions\JWTException;//use in jwt

class JWTAuthController
{

    public static function generateToken($user)
    {
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::fromUser($user)) {
                return false;
            }else{
                //Auth::attempt($credentials);
            }
        } catch (JWTException $e) {

            // something went wrong whilst attempting to encode the token
            return false;
        }
        return $token;
    }

    public function destroyToken()
    {
        # code...
    }

}
