<?php

namespace Modules\Core\Http\Auth;


use Auth;
use JWTAuth;//use in jwt
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;//use in jwt
use Modules\Core\Http\Controller as Controller;

class AuthController extends Controller
{

    public function Index()
    {
        return view("admin.admin");
    }


    protected function login(Request $request)
    {

        // grab credentials from the request
        $credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }else{
                //Auth::attempt($credentials);
            }
        } catch (JWTException $e) {

            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $user=Auth::user();
        $user=[
            'name'=>$user['name'],
            'photo'=>$user['photo'],
            'id'=>$user['id']
        ];
        // all good so return the token
        return response()->json(compact('token','user'));


        //$credentials = ['email' => $data['email'], 'password' =>$data['password'] ];
        // if (Auth::attempt($credentials)) {
        // Authentication passed...
        //   return redirect()->intended('dashboard');
        // }
        // return "Não autorizado";
    }


    // public function login(Request $request)
    //{
    //$dataToAuth = array('email' => $request->email, 'password' =>$request->password);
    /// return $this->authenticate($request);
    /// }

    public function logout()
    {
        $execution = JWTAuth::invalidate(JWTAuth::getToken());
        if (!$execution) {
            return response()->json(['msg'=>'Falhar ao deslogar!']);
        }else{
            return response()->json(['msg'=>'Obrigado por sua visita!']);
        }
        //Auth::logout();
        //return redirect('auth');
    }
}

?>
