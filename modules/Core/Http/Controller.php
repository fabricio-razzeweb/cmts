<?php

namespace Modules\Core\Http;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use DB;
use File;
use Modules\Core\Helpers\TextAdapter;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    // This is common path for upload archives
    public $uploadPath="src/";
    // This is default aplication type of request
    protected $aplicationType="api";

    public function viewPost($typeDirectory)
    {
      return view($typeDirectory.$this->viewDirectory."post".$this->viewFileEndName);
    }

    public function viewList($typeDirectory)
    {
      $dataElement = $this->fethAll();
      return view($typeDirectory.$this->viewDirectory."list".$this->viewFileEndName,["dataElement"=>$dataElement]);
    }

    public function viewOne($id,$typeDirectory)
    {
      $dataElement = $this->fethOne($id);
      return view($typeDirectory.$this->viewDirectory."view".$this->viewFileEndNameName,["dataElement"=>$dataElement]);
    }

    /**
    * Method responseForClient()
    * @param $execution your type is array and consist in the response of client of API
    * @param $commonAPIPath this is the path of route URL with sending of Client for continuos flow of aplication Ex:"list/checkeds/"
    * @return JSON of execution message and state
    *  |--> OBS: This return happen when the attribute '$this->aplicationType' of child of this current class have value "api"
    *    |--> OBS: '$this->aplicationType' having default value "api" in current class with extends of childs
    * @return redirect command for commun System user
    *  |--> OBS: The URL for redirect consist in union of @param $commonAPIPath and '$this->URLprefix'
    *    |--> OBS: '$this->URLprefix' is attribute of child this class, this attribute represent the prefix path of url, for Ex: "Dashboard/" or "Site"
    */
    protected function responseForClient($execution, $commonAPIPath)
    {
      // If this
      if($this->aplicationType=="api"){
        // Print execution result and message
        return response()->json(compact('execution'));
      }else{
        // Redirect for route
        return redirect($this->URLprefix.$commonAPIPath);
      }
    }

    /**
    * Method deleteArchive()
    * @param $fileName this is file for delete
    * @var filePath this is path of archive for delete is formed for:
    *  |--> @attr $this->uploadPath this is main path for upload
    *  |--> @attr $this->archivesDirectory this is archive of type with files ex: imgs, Music
    *  |--> @attr $this->archivesFinalDirectory this is archive for store files
    * @return true if archive delete
    * @return false if archive not deleted
    */
    public function deleteArchive($fileName)
    {
      // Destination Path of archive Ex: src/imgs/products/ .. file.png
      $filePath = $this->uploadPath.$this->archivesDirectoryType.strtolower($this->archivesFinalDirectory)."/".$fileName;
      // If file exists in the system
      if(File::exists($filePath)){
        // If true -> delete file
        File::delete($filePath);
        return true;
      }
      return false;
    }

    /**
      * Method uploadArchive()
      * This method upload archive for directory and this is determinated for controller of Element Ex: ("ProductController,PostController,News");
      * @param $request this param is Request of controller;
      * @param $archivePrefixName this param is sufix name of Archive Ex:(name-34243424.png);
      * @var in fuction $archiveName this param is name of Archive;
      * @var in fuction $directoryName this param is name of Directory for upload;
      * @var in fuction $archiveExt this param is name of Extension of archive;
      * @return $fileName
      * Powered by: Fabricio Magalhães - RazzeWeb - Crazy Developer =D ;
    */
    protected function uploadArchive($request,$archivePrefixName){
      // Catch configs for upload
      $dataArchive=$this->ConfigForUpload();
      // Header of Function
      // Get the name of archive in temporary storage $_FILES[]
      $archiveName=$dataArchive['archiveRequestName'];
      // Get name of directory name this directory is the type of archive Ex: pdf, img, music ... etc
      $directoryName=$dataArchive['directoryNameType'];
      // Get Archive Extension
      $archiveExt=$dataArchive['archiveExt'];
      // Construct file prefix name clear invalid characters
      $archivePrefixName = TextAdapter::generateLink($archivePrefixName);
      // Make a hash for complement name, this action prevent with one archive have same name with others archives
      $fileName = $archivePrefixName."-".md5(date("y-M-d:h:m:s")).".$archiveExt";
      // Destination Path of archive Ex: src/imgs/products/ .. file.png
      $destinationPath=$this->uploadPath."$directoryName".strtolower($this->archivesFinalDirectory)."/";
      // Catch file for upload
      $file = $request->file($archiveName);
      // Moving Archive for Directory
      $file->move($destinationPath, $fileName);
      //Return File Name
      return $fileName;
    }

    protected function upload( $archiveName,$directoryName,$archiveExt,$request,$archivePrefixName)
    {
      // Construct file prefix name clear invalid characters
      $archivePrefixName = TextAdapter::generateLink($archivePrefixName);
      // Make a hash for complement name, this action prevent with one archive have same name with others archives
      if ($archiveExt!=false) {
        $fileName = $archivePrefixName."-".md5(date("y-M-d:h:m:s")).".$archiveExt";
      }else{
        $file = $request->file($archiveName);
        $fileOriginalName = explode('.', $_FILES['file']['name']);
        $ext = end($fileOriginalName);
        $fileName =$fileOriginalName[0]."-". md5(date('h:i:s')).".$ext";
      }
      // Destination Path of archive Ex: src/imgs/products/ .. file.png
      $destinationPath=$this->uploadPath."$directoryName".strtolower($this->archivesFinalDirectory)."/";
      // Catch file for upload
      $file = $request->file($archiveName);
      // Moving Archive for Directory
      $file->move($destinationPath, $fileName);
      //Return File Name
      return $fileName;
    }

     protected function basicIndex($request)
     {
       // If request is one search?
       if($request->get('search')){
         //Cath search
         $search = $request->get('search');
         // Search object in database
         $elements = $this->model->where("name", "LIKE", "%{$search}%")->get();
       }else{
         // If normal request return itens paginate for id
         $elements = $this->model->orderBy('id','desc')->paginate(15);
       }
       // Return response for client
       return response()->json(compact('elements'));
     }

    protected function basicStore($request)
    {
      //This is data for transfer
      $elementData = $this->prepareDataForTransfer($request);
      //Calling MODEL and insert data in the tables
      $elementData->save();
      //return response to client
      $execution=["status"=>true,
                  "msg"=>"$this->nameElementUI, cadastrado com Sucesso!",
                  'id'=>$elementData->id
                  ];
      return $this->responseForClient($execution,"/lista");
    }

    protected function basicStoreGetId($request)
    {
      //This is data for transfer
      $elementData = $this->prepareDataForTransfer($request);
      //Calling MODEL and insert data in the tables
      $elementData->save();
      //return id of element
      return $elementData->id;
    }

    public function basicUpdate($request, $id)
    {
      //Find This element for edit
      $element=$this->model->find((int)$id);

      //This is data for transfer
      $element = $this->prepareDataForTransfer($request,$element);
      //Calling MODEL and update data in the tables
      $executionStatus = $element->save();

      //Make Response to client
      if ($executionStatus) {

        $msg = "$this->nameElementUI, editado com Sucesso!";

      }else{

        $msg = "Desculpe ocorreu um erro inesperado";

      }


      //return response to client
      $execution=["status"=>$executionStatus,"msg"=>$msg];
      return $this->responseForClient($execution,"/$id");

    }

    public function basicUpdateGetId($request, $id)
    {
      //Find This element for edit
      $element=$this->model->find((int)$id);

      //This is data for transfer
      $element = $this->prepareDataForTransfer($request,$element);
      //Calling MODEL and update data in the tables
      $executionStatus = $element->save();

      //return id of element
      return $element->id;

    }

    public function basicShow($id)
    {
        $element = $this->model->where('id',$id)->get();
        return response()->json(compact('element'));
    }

    protected function delete($id){

      //Find This element end delete
      $this->model->where('id', '=', $id)->delete();

      if($this->aplicationType=="api"){

        $execution=["status"=>true];
        return response()->json(compact('execution'));

      }else {

        return redirect("$this->URLprefix/listar");

      }

    }

    protected function softDelete($model,$id){
      $executionStatus = $model->find($id)->delete();

      if ($executionStatus) {
        $msg="Deletado com Sucesso!";
      }else{
        $msg="Ocorreu um erro ao deletar";
      }

      $execution=["status"=>$executionStatus, 'msg'=>$msg];
      return response()->json(compact('execution'));
    }
}
