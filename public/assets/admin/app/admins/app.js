var app = angular
.module('dashboard',
["ngRoute",
"ngMask",
"ngSanitize",
"ui.tinymce",
'mwl.calendar',
"oc.lazyLoad",
'datetimepicker',
'chart.js'])
.constant("config",{

  API_URL: "dashboard/api/",
  KEY: "asdfh21@sky234685!%27e4#$asra4$123@@!##!213zdsuas",
  KEY_OF_KEY: "!@#@sas!149086",
  VIEWS_DIRECTORY: "../assets/admin/app/admins/modules/",
  VIEWS_DIRECTORY_ROOT: "../assets/admin/app/core/",
  VIEWS_OF_TYPE_OF_USER: "../assets/admin/app/admins/UI/",
  WEB_SOCKET_PATH: document.location.origin + ":6969"

});

app.run(['$location', '$rootScope', function($location, $rootScope) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {

        if (current.hasOwnProperty('$$route')) {
            $rootScope.title = current.$$route.title;
            $(document).ready(function(){
              $('.preloader').removeClass('on').fadeOut('slow');
            });
        }
    });
}]);

app.directive('dropzone', function () {
  return function (scope, element, attrs) {
    var config, dropzone;

    config = scope[attrs.dropzone];

    // create a Dropzone for the element with the given options
    dropzone = new Dropzone(element[0], config.options);

    // bind the given event handlers
    angular.forEach(config.eventHandlers, function (handler, event) {
      dropzone.on(event, handler);
    });
  };
});

app.filter('dateFormat', function myDateFormat($filter){
  return function(text){
    if (text!=undefined) {
      var  tempdate= new Date(text.replace(/-/g,"/"));
      return $filter('date')(tempdate, "dd/MM/yyyy");
    }
  }
});


app.filter('dateFormatDay', function myDateFormat($filter){
  return function(text){
    if (text!=undefined) {
      var  tempdate= new Date(text);
      return tempdate.getDate();
    }
  }
});

app.filter('dateFormatMount', function myDateFormat($filter){
  return function(text){
    if (text!=undefined) {
      var  tempdate= new Date(text);
      return tempdate.getMonth()+1;
    }
  }
});

app.filter('dateFormatYear', function myDateFormat($filter){
  return function(text){
    if (text!=undefined) {
      var  tempdate= new Date(text);
      return tempdate.getFullYear();
    }
  }
});

app.filter('numberFixedLen', function () {
        return function (n, len) {
            var num = parseInt(n, 10);
            len = parseInt(len, 10);
            if (isNaN(num) || isNaN(len)) {
                return n;
            }
            num = ''+num;
            while (num.length < len) {
                num = '0'+num;
            }
            return num;
        };
    });
