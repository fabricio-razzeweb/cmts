app.config(function($routeProvider,config){

  //***********courses Routes course***********//
  $routeProvider.when("/dashboard/cursos/novo",{
    templateUrl: config.VIEWS_DIRECTORY+"courses/views/coursesPost.view.html",
    controller: "coursesPostCrtl",
    title: "Novo Curso",
    authenticated: true,
    coursetype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      }
    }
  });

  //***********courses Routes LIST***********//
  $routeProvider.when("/dashboard/cursos",{
    templateUrl: config.VIEWS_DIRECTORY+"courses/views/coursesList.view.html",
    controller: "coursesListCrtl",
    title: "Meus Cursos",
    authenticated: true,
    coursetype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      },
      elementsRequest:function (coursesAPIService) {
        return coursesAPIService.getAll();
      }
    }
  });

  //***********courses Routes SHOW***********//
  $routeProvider.when("/dashboard/cursos/:id",{
    templateUrl: config.VIEWS_DIRECTORY+"courses/views/coursesOne.view.html",
    controller: "coursesOneCrtl",
    title: "Editar Curso",
    authenticated: true,
    coursetype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      },
      element:function (coursesAPIService,$route) {
        return coursesAPIService.getOne($route.current.params.id);
      }
    }
  });


});
