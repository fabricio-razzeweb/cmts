app.factory("coursesAPIService", function ($http,authService,config) {

    var coursesAPI={};

    coursesAPI.fillable = [
        'title',
        'keywords',
        'description',
        'photo',
        'tags',
        'users_id',
    ];

    coursesAPI.preparedDataToTransfer=function (dataElement,method) {
      var data = new FormData;

      for (var i = 0; i < this.fillable.length; i++) {
        if (dataElement[this.fillable[i]]!=undefined ||
                dataElement[this.fillable[i]]!=null) {
                data.append(this.fillable[i],dataElement[this.fillable[i]]);
        }
      }

      if(method=="PUT"){
        data.append("_method","PUT");
      }

      data.append("users_id", authService.getUserId());

      return data;
    };

    coursesAPI.path =function(){
  		var course = config.API_URL+"courses";//<<CHANGE
  		return course;
  	};

    coursesAPI.inputRequestServiceFactory=function (method,element,id=undefined) {
      var requestConfig = authService.authRequest();
      if (id==undefined) {var path =this.path();}else{var path =this.path()+"/"+id;}
      return $http.post(path,this.preparedDataToTransfer(element,method),requestConfig)//;
    };

    coursesAPI.outputRequestServiceFactory=function (elementId=null,page=null,searchItem=null) {
      var requestConfig = authService.authRequest();
      //Request requiriments adjust for request, elementId and pages
      if(elementId!=null){elementId= "/"+elementId}else{elementId="";};
      if(page!=null){requestConfig['params']={page: page}};
      if(searchItem!=null){requestConfig['params']={search: searchItem}};
      return $http.get(this.path()+elementId,requestConfig).then(function (response) {
                          return response.data;
                      })//;
    };

    coursesAPI.post = function (element) {
        var execution = authService.checkRequest(this.inputRequestServiceFactory("POST",element),this);
        return execution;
    };

    coursesAPI.put = function (element,idElement) {
        var execution = authService.checkRequest(this.inputRequestServiceFactory("PUT",element,idElement),this);
        return execution;
    };

    coursesAPI.getAll=function (page=null) {
        var execution = authService.checkRequest(this.outputRequestServiceFactory(null,page),this);
        return execution;
    };

    coursesAPI.getOne=function (idElement=null) {
        var execution =authService.checkRequest(this.outputRequestServiceFactory(idElement),this);
        return execution;
    };

    coursesAPI.getSearch=function (searchItem) {
        var execution = authService.checkRequest(this.outputRequestServiceFactory(null,null,searchItem),this);
        return execution;
    };

    coursesAPI.delete=function (idElement) {
      return $http.delete(this.path()+"/"+idElement,{
                        transformRequest: angular.identity,
                        headers: {'Authorization': 'Bearer'+authService.getUserInfo("tk")},
                        data: {_method:"delete"},
                      });
    }

    coursesAPI.getInfo=function (infoUrl) {
      var requestConfig = authService.authRequest();
      return $http.get(this.path()+infoUrl,requestConfig).then(function (response) {
                          return response.data;
                      })//;
    };

    return coursesAPI;
})
