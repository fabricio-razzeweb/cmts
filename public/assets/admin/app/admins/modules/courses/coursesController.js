app.controller("coursesPostCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'groupReader',
'coursesAPIService',
function($scope,config,$location,authService,preloader,groupReader,coursesAPIService) {

    //Get User Information
    $scope = authService.userInformationToView($scope);

    $scope.course = {};

    $scope.save=function () {

        preloader.show();

        coursesAPIService
        .post($scope.course)
        .then(function(data,status,header){
            $location.path('dashboard/cursos');
            preloader.hide();
        });

    };

}])

app.controller("coursesListCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'deleter',
'listReader',
'coursesAPIService',
'elementsRequest',
function($scope,config,$location,authService,preloader,deleter,listReader,coursesAPIService,elementsRequest) {

    var modelName = 'courses';
    var defaultAPIservice = coursesAPIService;
    var nameOfGroupOfElements = 'courses';

    $scope.nameRouteforControllerElement = 'cursos';

    //Get User Information
    $scope = authService.userInformationToView($scope);
    $scope = listReader.readAndPaginate($scope,nameOfGroupOfElements,modelName,elementsRequest);

    $scope.deleteItemId=0;

    $scope.defineOrderTable=function(type){

        $scope.critOfOrder = type;
        $scope.orderOfcrit = !$scope.orderOfcrit;

    }

    $scope.goToPostItem=function () {

        preloader.show();
        $location.path("dashboard/"+$scope.nameRouteforControllerElement+"/novo");

    }


    $scope.recharge=function (page=null,itemSearch=null) {

        if (page==null) {

            if (itemSearch==null) {
                var request = defaultAPIservice.getAll();
            }else{
                var request = defaultAPIservice.getSearch(itemSearch);
            }

        }else{

            if (itemSearch==null) {
                var request = defaultAPIservice.getAll(page);
            }

        }
        request.then(function (response) {

            var elements = [];

            if (page==null) {

                if (itemSearch==null) {

                    elements = response[modelName].data;

                }else{

                    elements = response[modelName];

                }

            }else{

                elements = response[modelName].data;

            }

            $scope[nameOfGroupOfElements] = elements;

            preloader.hide();
        });

    }


    $scope.searchItem=function () {

        $scope.recharge(null,$scope.search);

    }

    $scope.goToPage=function(page){

        $(".preloader").fadeIn();
        $scope.recharge(page);

    }

    $scope.deleteItens=function () {

        return deleter.deleteAllInChest($scope,nameOfGroupOfElements,defaultAPIservice,'selected','id')

    }

    $scope.deleteItem=function(itemId){

        return deleter.deleteOne($scope,defaultAPIservice,itemId);

    }

}])

app.controller("coursesOneCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'deleter',
'groupReader',
'coursesAPIService',
'element',
function($scope,config,$location,authService,preloader,deleter,groupReader,coursesAPIService,element) {
    $scope = authService.userInformationToView($scope);
    $scope.photoOn = true;
    $scope.course = element.course;
    var defaultAPIservice = coursesAPIService;

    $scope.save=function () {
        preloader.show();

        coursesAPIService
        .put($scope.course, $scope.course.id)
        .success(function(data,status,header){
            coursesAPIService
            .getOne($scope.course.id)
            .then(function(response){
                $scope.course = response.course;
                preloader.hide();
            });
        });
    };

    $scope.deleteItem=function(){
        return deleter.deleteOne($scope,defaultAPIservice,itemId);
    }

}])
