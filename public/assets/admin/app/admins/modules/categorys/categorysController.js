app.controller("categorysListCrtl",
['$scope',
  'config',
  '$location',
  'authService',
  'preloader',
  'deleter',
  'listReader',
  'categorysAPIService',
  'elementsRequest',
  function($scope,config,$location,authService,preloader,deleter,
            listReader,categorysAPIService,elementsRequest) {

    var modelName = 'categorys';
    var defaultAPIservice = categorysAPIService;
    var nameOfGroupOfElements = 'categorys';

    $scope.nameRouteforControllerElement = 'categorias';

    //Get User Information
    $scope = authService.userInformationToView($scope);
    $scope = listReader.readAndPaginate($scope, nameOfGroupOfElements, modelName, elementsRequest);

    $scope.deleteItemId = 0;

    $scope.defineOrderTable=function(type){

      $scope.critOfOrder = type;
      $scope.orderOfcrit = !$scope.orderOfcrit;

    }

    //Set new item form post
    $scope.category = {};

    $scope.save=function (category = null) {

      //Show Preloader
      preloader.show();

      if (!category) {
        //Post the element of controller
        var request = categorysAPIService.post($scope.category);
      }else{
         //Put the element of controller
        var request = categorysAPIService.put(category, category.id);
      }

      //If request is success
      request.success(function(data,status,header){
          $scope.category = {};
          $scope.categoryPostShow = false;
          //Down Preloader
          $scope.recharge();
      });

    };

    $scope.goToPostItem=function () {
      $scope.categoryPostShow = (!$scope.categoryPostShow);
    }

    $scope.recharge=function (page=null,itemSearch=null) {

      if (page==null) {

        if (itemSearch==null) {
          var request = defaultAPIservice.getAll();
        }else{
          var request = defaultAPIservice.getSearch(itemSearch);
        }

      }else{

        if (itemSearch==null) {
          var request = defaultAPIservice.getAll(page);
        }

      }
      request.then(function (response) {

          var elements = [];

          if (page==null) {

            if (itemSearch==null) {

              elements = response[modelName].data;

            }else{

              elements = response[modelName];

            }

          }else{

            elements = response[modelName].data;

          }

          $scope[nameOfGroupOfElements] = elements;

          preloader.hide();
      });

    }


    $scope.searchItem=function () {

      $scope.recharge(null,$scope.search);

    }

    $scope.goToPage=function(page){

      $(".preloader").fadeIn();
      $scope.recharge(page);

    }

    $scope.deleteItens=function () {

      return deleter.deleteAllInChest($scope,nameOfGroupOfElements,defaultAPIservice,'selected','id')

    }

    $scope.deleteItem=function(itemId){

      return deleter.deleteOne($scope,defaultAPIservice,itemId);

    }

}])

app.controller("categorysOneCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'deleter',
'categorysAPIService',
'element',
function($scope,config,$location,authService,preloader,deleter,categorysAPIService,element) {

  var defaultAPIservice = categorysAPIService;

  //Get User Information
  $scope = authService.userInformationToView($scope);

  $scope.category = element.element[0];

  $scope.save=function () {

    preloader.show();
    var request = categorysAPIService.put($scope.category,$scope.category.id)
      .success(function(data,status,header){
          preloader.hide();
        });

  };

  $scope.deleteItem=function(){

    return deleter.deleteOne($scope,defaultAPIservice,itemId);

  }

}])
