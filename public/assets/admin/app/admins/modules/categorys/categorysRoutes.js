app.config(function($routeProvider,config){

  //***********categorys Routes POST***********//
  $routeProvider.when("/dashboard/categorias/novo",{
    templateUrl: config.VIEWS_DIRECTORY+"categorys/views/categorysPost.view.html",
    controller: "categorysPostCrtl",
    title: "Nova Tag",
    authenticated: true,
    usertype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      }
    }
  });

  //***********categorys Routes LIST***********//
  $routeProvider.when("/dashboard/categorias",{
    templateUrl: config.VIEWS_DIRECTORY+"categorys/views/categorysList.view.html",
    controller: "categorysListCrtl",
    title: "Meus categorys",
    authenticated: true,
    usertype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      },
      elementsRequest:function (categorysAPIService) {
        return categorysAPIService.getAll();
      }
    }
  });

  //***********categorys Routes SHOW***********//
  $routeProvider.when("/dashboard/categorias/:id",{
    templateUrl: config.VIEWS_DIRECTORY+"categorys/views/categorysOne.view.html",
    controller: "categorysOneCrtl",
    title: "Editar Tag",
    authenticated: true,
    usertype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      },
      element:function (categorysAPIService,$route) {
        return categorysAPIService.getOne($route.current.params.id);
      }
    }
  });


});
