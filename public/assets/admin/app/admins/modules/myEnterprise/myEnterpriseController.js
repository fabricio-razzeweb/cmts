app.controller("myEnterpriseCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'deleter',
'groupReader',
'myEnterpriseAPIService',
'element',
function($scope,config,$location,authService,preloader,deleter,groupReader,myEnterpriseAPIService,element) {

    var defaultAPIservice = myEnterpriseAPIService;

    $scope.photoOn = true;

    //Get User Information
    $scope = authService.userInformationToView($scope);

    if (element.myEnterprise[0]) {
        $scope.myEnterprise = element.myEnterprise[0];
    }else{
        $scope.myEnterprise = {};
        $scope.myEnterprise.id = 1;
    }

    $scope.save=function () {

        preloader.show();

        var request = myEnterpriseAPIService.put($scope.myEnterprise,$scope.myEnterprise.id)

        .success(function(data,status,header){
            preloader.hide();
        });

    };

}])
