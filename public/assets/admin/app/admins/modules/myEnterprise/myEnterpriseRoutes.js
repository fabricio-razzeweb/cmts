app.config(function($routeProvider,config){

  //***********myEnterprise Routes SHOW***********//
  $routeProvider.when("/dashboard/informacoes",{
    templateUrl: config.VIEWS_DIRECTORY+"myEnterprise/views/myEnterpriseOne.view.html",
    controller: "myEnterpriseCrtl",
    title: "Editar Informações",
    authenticated: true,
    posttype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      },
      element:function (myEnterpriseAPIService) {
        return myEnterpriseAPIService.getAll();
      }
    }
  });

});
