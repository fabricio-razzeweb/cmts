app.config(function($routeProvider,config){


    //***********locations Routes LIST***********//
    $routeProvider.when("/dashboard/instalacoes",{
        templateUrl: config.VIEWS_DIRECTORY+"locations/views/locationsList.view.html",
        controller: "locationsListCrtl",
        title: "Minhas localizações",
        authenticated: true,
        usertype: 'admin',
        resolve:{
            load: function (preloader){
                preloader.show();
            },
            elementsRequest:function (locationsAPIService) {
                return locationsAPIService.getAll();
            }
        }
    });

    //***********locations Routes SHOW***********//
    $routeProvider.when("/dashboard/instalacoes/:id",{
        templateUrl: config.VIEWS_DIRECTORY+"locations/views/locationsOne.view.html",
        controller: "locationsOneCrtl",
        title: "Editar Localização",
        authenticated: true,
        usertype: 'admin',
        resolve:{
            load: function (preloader){
                preloader.show();
            },
            element:function (locationsAPIService,$route) {
                return locationsAPIService.getOne($route.current.params.id);
            }
        }
    });


});
