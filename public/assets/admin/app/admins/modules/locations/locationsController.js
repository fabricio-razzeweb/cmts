app.controller("locationsListCrtl",
['$scope',
  'config',
  '$location',
  'authService',
  'preloader',
  'deleter',
  'listReader',
  'locationsAPIService',
  'elementsRequest',
  function($scope,config,$location,authService,preloader,deleter,
            listReader,locationsAPIService,elementsRequest) {

    var modelName = 'locations';
    var defaultAPIservice = locationsAPIService;
    var nameOfGroupOfElements = 'locations';

    $scope.nameRouteforControllerElement = 'localizacoes';

    //Get User Information
    $scope = authService.userInformationToView($scope);
    $scope = listReader.readAndPaginate($scope, nameOfGroupOfElements, modelName, elementsRequest);

    $scope.deleteItemId = 0;

    $scope.defineOrderTable=function(type){

      $scope.critOfOrder = type;
      $scope.orderOfcrit = !$scope.orderOfcrit;

    }

    //Set new item form post
    $scope.location = {};

    $scope.save=function (location = null) {

      //Show Preloader
      preloader.show();

      if (!location) {
        //Post the element of controller
        var request = locationsAPIService.post($scope.location);
      }else{
         //Put the element of controller
        var request = locationsAPIService.put(location, location.id);
      }

      //If request is success
      request.success(function(data,status,header){
          $scope.location = {};
          $scope.locationPostShow = false;
          //Down Preloader
          $scope.recharge();
      });

    };

    $scope.goToPostItem=function () {
      $scope.locationPostShow = (!$scope.locationPostShow);
    }

    $scope.recharge=function (page=null,itemSearch=null) {

      if (page==null) {

        if (itemSearch==null) {
          var request = defaultAPIservice.getAll();
        }else{
          var request = defaultAPIservice.getSearch(itemSearch);
        }

      }else{

        if (itemSearch==null) {
          var request = defaultAPIservice.getAll(page);
        }

      }
      request.then(function (response) {

          var elements = [];

          if (page==null) {

            if (itemSearch==null) {

              elements = response[modelName].data;

            }else{

              elements = response[modelName];

            }

          }else{

            elements = response[modelName].data;

          }

          $scope[nameOfGroupOfElements] = elements;

          preloader.hide();
      });

    }


    $scope.searchItem=function () {

      $scope.recharge(null,$scope.search);

    }

    $scope.goToPage=function(page){

      $(".preloader").fadeIn();
      $scope.recharge(page);

    }

    $scope.deleteItens=function () {

      return deleter.deleteAllInChest($scope,nameOfGroupOfElements,defaultAPIservice,'selected','id')

    }

    $scope.deleteItem=function(itemId){

      return deleter.deleteOne($scope,defaultAPIservice,itemId);

    }

}])

app.controller("locationsOneCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'deleter',
'locationsAPIService',
'element',
function($scope,config,$location,authService,preloader,deleter,locationsAPIService,element) {

  var defaultAPIservice = locationsAPIService;

  //Get User Information
  $scope = authService.userInformationToView($scope);

  $scope.location = element.locations;

  $scope.save=function () {

    preloader.show();
    var request = locationsAPIService.put($scope.location,$scope.location.id)
      .success(function(data,status,header){
          preloader.hide();
        });

  };

  $scope.deleteItem=function(){

    return deleter.deleteOne($scope,defaultAPIservice,itemId);

  }

}])
