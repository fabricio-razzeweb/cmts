app.controller("postsPostCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'groupReader',
'postsAPIService',
'categorys',
function($scope,config,$location,authService,preloader,groupReader,postsAPIService,categorys) {

    //Get User Information
    $scope = authService.userInformationToView($scope);

    $scope.post = {};
    $scope.categorys = categorys.categorys;

    $scope.save=function () {

        preloader.show();

        postsAPIService
        .post($scope.post)
        .then(function(data,status,header){
            $location.path('dashboard/posts');
            preloader.hide();
        });

    };

}])

app.controller("postsListCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'deleter',
'listReader',
'postsAPIService',
'elementsRequest',
function($scope,config,$location,authService,preloader,deleter,listReader,postsAPIService,elementsRequest) {

    var modelName = 'Posts';
    var defaultAPIservice = postsAPIService;
    var nameOfGroupOfElements = 'posts';

    $scope.nameRouteforControllerElement = 'posts';

    //Get User Information
    $scope = authService.userInformationToView($scope);
    $scope = listReader.readAndPaginate($scope,nameOfGroupOfElements,modelName,elementsRequest);

    $scope.deleteItemId=0;

    $scope.defineOrderTable=function(type){

        $scope.critOfOrder = type;
        $scope.orderOfcrit = !$scope.orderOfcrit;

    }

    $scope.goToPostItem=function () {

        preloader.show();
        $location.path("dashboard/"+$scope.nameRouteforControllerElement+"/novo");

    }


    $scope.recharge=function (page=null,itemSearch=null) {

        if (page==null) {

            if (itemSearch==null) {
                var request = defaultAPIservice.getAll();
            }else{
                var request = defaultAPIservice.getSearch(itemSearch);
            }

        }else{

            if (itemSearch==null) {
                var request = defaultAPIservice.getAll(page);
            }

        }
        request.then(function (response) {

            var elements = [];

            if (page==null) {

                if (itemSearch==null) {

                    elements = response[modelName].data;

                }else{

                    elements = response[modelName];

                }

            }else{

                elements = response[modelName].data;

            }

            $scope[nameOfGroupOfElements] = elements;

            preloader.hide();
        });

    }


    $scope.searchItem=function () {

        $scope.recharge(null,$scope.search);

    }

    $scope.goToPage=function(page){

        $(".preloader").fadeIn();
        $scope.recharge(page);

    }

    $scope.deleteItens=function () {

        return deleter.deleteAllInChest($scope,nameOfGroupOfElements,defaultAPIservice,'selected','id')

    }

    $scope.deleteItem=function(itemId){

        return deleter.deleteOne($scope,defaultAPIservice,itemId);

    }

}])

app.controller("postsOneCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'deleter',
'groupReader',
'postsAPIService',
'element',
'categorys',
function($scope,config,$location,authService,preloader,deleter,groupReader,postsAPIService,element,categorys) {
    $scope = authService.userInformationToView($scope);
    $scope.photoOn = true;
    $scope.post = element.element;
    $scope.categorys = categorys.categorys;
    var defaultAPIservice = postsAPIService;

    $scope.save=function () {
        preloader.show();

        postsAPIService
        .put($scope.post, $scope.post.id)
        .success(function(data,status,header){

            postsAPIService
            .getOne($scope.post.id)
            .then(function(response){
                $scope.post = response.element;
                preloader.hide();
            });
        });
    };

    $scope.deleteItem=function(){
        return deleter.deleteOne($scope,defaultAPIservice,itemId);
    }

}])
