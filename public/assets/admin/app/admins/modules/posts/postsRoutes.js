app.config(function($routeProvider,config){

  //***********posts Routes POST***********//
  $routeProvider.when("/dashboard/posts/novo",{
    templateUrl: config.VIEWS_DIRECTORY+"posts/views/postsPost.view.html",
    controller: "postsPostCrtl",
    title: "Novo Artigo",
    authenticated: true,
    posttype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      },
      categorys:function (categorysAPIService) {
        return categorysAPIService.getSearch("name|*");
      }
    }
  });

  //***********posts Routes LIST***********//
  $routeProvider.when("/dashboard/posts",{
    templateUrl: config.VIEWS_DIRECTORY+"posts/views/postsList.view.html",
    controller: "postsListCrtl",
    title: "Meus Artigos",
    authenticated: true,
    posttype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      },
      elementsRequest:function (postsAPIService) {
        return postsAPIService.getAll();
      }
    }
  });

  //***********posts Routes SHOW***********//
  $routeProvider.when("/dashboard/posts/:id",{
    templateUrl: config.VIEWS_DIRECTORY+"posts/views/postsOne.view.html",
    controller: "postsOneCrtl",
    title: "Editar Artigo",
    authenticated: true,
    posttype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      },
      element:function (postsAPIService,$route) {
        return postsAPIService.getOne($route.current.params.id);
      },
      categorys:function (categorysAPIService) {
        return categorysAPIService.getSearch("name|*");
      }
    }
  });


});
