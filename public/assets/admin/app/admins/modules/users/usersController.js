app.controller("usersPostCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'usersAPIService',
'options',
function ($scope,config,$location,authService,preloader,usersAPIService,options) {

    //Get User Information
    $scope = authService.userInformationToView($scope);

    $scope.usersTypes = options.usersTypes;

    //Set new item form post
    $scope.user = {address:{}};

    $scope.passwordOn = true;

    $scope.save=function () {

        //Show Preloader
        preloader.show();
        //Post the element of controller
        var request = usersAPIService.post($scope.user);
        //If request is success
        request.success(function (data,status,header) {
            //Set element id
            $location.path('/dashboard/usuarios')
            //Down Preloader
            preloader.hide();
        });

    };

}])

app.controller("usersListCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'deleter',
'listReader',
'usersAPIService',
'elementsRequest',
function ($scope,config,$location,authService,preloader,deleter,listReader,usersAPIService,elementsRequest) {

    var modelName = 'elements';
    var defaultAPIservice = usersAPIService;
    var nameOfGroupOfElements = 'users';

    $scope.nameRouteforControllerElement = 'usuarios';

    //Get User Information
    $scope = authService.userInformationToView($scope);
    $scope = listReader.readAndPaginate($scope,nameOfGroupOfElements,modelName,elementsRequest);

    $scope.deleteItemId=0;

    $scope.defineOrderTable=function (type) {

        $scope.critOfOrder = type;
        $scope.orderOfcrit = !$scope.orderOfcrit;

    }

    $scope.goToPostItem=function () {

        preloader.show();
        $location.path("dashboard/"+$scope.nameRouteforControllerElement+"/novo");

    }


    $scope.recharge=function (page=null,itemSearch=null) {

        if (page==null) {

            if (itemSearch==null) {
                var request = defaultAPIservice.getAll();
            }else{
                var request = defaultAPIservice.getSearch(itemSearch);
            }

        }else{

            if (itemSearch==null) {
                var request = defaultAPIservice.getAll(page);
            }

        }
        request.then(function (response) {

            var elements = [];

            if (page==null) {

                if (itemSearch==null) {

                    elements = response[modelName].data;

                }else{

                    elements = response[modelName];

                }

            }else{

                elements = response[modelName].data;

            }

            $scope[nameOfGroupOfElements] = elements;

            preloader.hide();
        });

    }


    $scope.searchItem=function () {

        $scope.recharge(null,$scope.search);

    }

    $scope.goToPage=function (page) {

        $(".preloader").fadeIn();
        $scope.recharge(page);

    }

    $scope.deleteItens=function () {

        return deleter.deleteAllInChest($scope,nameOfGroupOfElements,defaultAPIservice,'selected','id')

    }

    $scope.deleteItem=function (itemId) {

        return deleter.deleteOne($scope,defaultAPIservice,itemId);

    }

}])

app.controller("usersOneCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'deleter',
'usersAPIService',
'element',
'options',
function ($scope,config,$location,authService,preloader,deleter,usersAPIService,element,options) {

    var defaultAPIservice = usersAPIService;

    $scope.photoOn = true;
    $scope = authService.userInformationToView($scope);
    $scope.user = element.element;
    $scope.usersTypes = options.usersTypes;
    $scope.exam = {
        'archive':'',
        'name':'',
        'clients_id': $scope.user.id
    };
    $scope.clientsExams = [];

    $scope.save = function () {

        preloader.show();
        var request = usersAPIService.put($scope.user,$scope.user.id)
        .success(function (data,status,header) {
            preloader.hide();
        });

    };

    $scope.deleteItem = function () {
        return deleter.deleteOne($scope,defaultAPIservice,itemId);
    }

    $scope.rechargeExams = function () {
        usersAPIService
        .getExams($scope.user.id)
        .then(function (response) {
            $scope.clientExams = response.exams;
        });
    }

    $scope.sendExam = function () {
        preloader.show();

        usersAPIService
        .sendExam($scope.exam)
        .then(function (response) {
            if (response.data.execution.status) {
                $scope.rechargeExams();
            }
            preloader.hide();
        });
    }

    $scope.deleteExam = function (examId) {
        preloader.show();

        usersAPIService
        .deleteExam(examId)
        .then(function (response) {
            if (response.data.execution.status) {
                $scope.rechargeExams();
            }
            preloader.hide();
        });
    }

    $scope.rechargeExams();
}])
