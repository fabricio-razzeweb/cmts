app.factory("usersAPIService", function ($http, authService, config) {

    var usersAPI = {};
    //This attr is fields of element
    usersAPI.fillable = [
        'name',
        'email',
        'password',
        'phone',
        'cpf',
        'is_active',
        'users_type_id',
        'cpf',
        'age',
        'photo',
        'date_birth',
        'sex'
    ];
    /*
     * Method preparedDataToTransfer()
     * @param dataElement - this param is data for send to server
     * @param method - this param is method of request
     * @return data - is used for send to server
     */
    usersAPI.preparedDataToTransfer = function (dataElement, method) {
        var data = new FormData;
        // Read the attr array fillable for add in data for send to server
        for (var i = 0; i < this.fillable.length; i++) {
            // Check if attr exist
            if (dataElement[this.fillable[i]] != undefined ||
                dataElement[this.fillable[i]] != null) {
                // Add value to attr
                data.append(this.fillable[i], dataElement[this.fillable[i]]);
            }
        }
        if (method == "PUT") {
            data.append("_method", "PUT");
        }

        if (dataElement.address) {
            if (dataElement.address.neighborhood) {
                data.append('neighborhood', dataElement.address.neighborhood);
            }
            if (dataElement.address.street) {
                data.append('street', dataElement.address.street);
            }
            if (dataElement.address.number) {
                data.append('number', dataElement.address.number);
            }
            if (dataElement.address.complement) {
                data.append('complement', dataElement.address.complement);
            }
        }

        data.append("users_id", authService.getUserId());
        // return data to send
        return data;
    };

    usersAPI.path = function () {
        var course = config.API_URL + "users"; //<<CHANGE
        return course;
    };

    usersAPI.inputRequestServiceFactory = function (method, element, id = undefined) {
        var requestConfig = authService.authRequest();
        if (id == undefined) {
            var path = this.path();
        } else {
            var path = this.path() + "/" + id;
        }
        return $http.post(path, this.preparedDataToTransfer(element, method), requestConfig)
    };

    usersAPI.outputRequestServiceFactory = function (elementId = null, page = null, searchItem = null) {
        var requestConfig = authService.authRequest();
        //Request requiriments adjust for request, elementId and pages
        if (elementId != null) {
            elementId = "/" + elementId
        } else {
            elementId = "";
        };
        if (page != null) {
            requestConfig['params'] = {
                page: page
            }
        };
        if (searchItem != null) {
            requestConfig['params'] = {
                search: searchItem
            }
        };
        return $http.get(this.path() + elementId, requestConfig).then(function (response) {
            return response.data;
        })
    };

    usersAPI.post = function (element) {
        var execution = authService.checkRequest(this.inputRequestServiceFactory("POST", element), this);
        return execution;
    };

    usersAPI.put = function (element, idElement) {
        var execution = authService.checkRequest(this.inputRequestServiceFactory("PUT", element, idElement), this);
        return execution;
    };

    usersAPI.getAll = function (page = null) {
        var execution = authService.checkRequest(this.outputRequestServiceFactory(null, page), this);
        return execution;
    };

    usersAPI.getOne = function (idElement = null) {
        var execution = authService.checkRequest(this.outputRequestServiceFactory(idElement), this);
        return execution;
    };

    usersAPI.getSearch = function (searchItem) {
        var execution = authService.checkRequest(this.outputRequestServiceFactory(null, null, searchItem), this);
        return execution;
    };

    usersAPI.delete = function (idElement) {
        return $http.delete(this.path() + "/" + idElement, {
            transformRequest: angular.identity,
            headers: {
                'Authorization': 'Bearer' + authService.getUserInfo("tk")
            },
            data: {
                _method: "delete"
            },
        });
    }

    usersAPI.getInfo = function (infoUrl) {
        var requestConfig = authService.authRequest();
        return $http.get(this.path() + infoUrl, requestConfig).then(function (response) {
            return response.data;
        })
    };

    usersAPI.getAllClients = function () {
        var requestConfig = authService.authRequest();

        return authService.checkRequest(
            $http.get(config.API_URL + "clients", requestConfig)
            .then(function (response) {
                return response.data;
            })
        );
    }

    usersAPI.sendExam = function (exam) {
        console.log(exam);
        var examData = new FormData;
        examData.append('archive', exam.archive);
        examData.append('name', exam.name);
        examData.append('clients_id', exam.clients_id);

        var path = config.API_URL + 'clients/exams';

        return $http.post(path, examData, authService.authRequest());
    }

    usersAPI.deleteExam = function (examId) {

        var path = config.API_URL + 'clients/exams/' + examId;

        return $http.delete(path, authService.authRequest());
    }

    usersAPI.getExams = function (clientId) {
        let url = config.API_URL + 'clients/exams/' + clientId;

        return authService.checkRequest(
            $http.get(url, authService.authRequest())
            .then(function (response) {
                return response.data;
            })
        );
    }

    return usersAPI;
})
