app.config(function($routeProvider,config){

    //***********users Routes POST***********//
    $routeProvider.when("/dashboard/usuarios/novo",{
        templateUrl: config.VIEWS_DIRECTORY+"users/views/usersPost.view.html",
        controller: "usersPostCrtl",
        title: "Novo Usuário",
        authenticated: true,
        usertype: 'admin',
        resolve:{
            load: function (preloader){
                preloader.show();
            },
            options: function (usersAPIService) {
                return usersAPIService.getInfo('/options/register').then(function(response) {
                    return response.options;
                });
            }
        }
    });

    //***********users Routes LIST***********//
    $routeProvider.when("/dashboard/usuarios",{
        templateUrl: config.VIEWS_DIRECTORY+"users/views/usersList.view.html",
        controller: "usersListCrtl",
        title: "Meus Usuários",
        authenticated: true,
        usertype: 'admin',
        resolve:{
            load: function (preloader){
                preloader.show();
            },
            elementsRequest:function (usersAPIService) {
                return usersAPIService.getAll();
            }
        }
    });

    //***********users Routes SHOW***********//
    $routeProvider.when("/dashboard/usuarios/:id",{
        templateUrl: config.VIEWS_DIRECTORY+"users/views/usersOne.view.html",
        controller: "usersOneCrtl",
        title: "Editar Usuário",
        authenticated: true,
        usertype: 'admin',
        resolve:{
            load: function (preloader){
                preloader.show();
            },
            element:function (usersAPIService,$route) {
                return usersAPIService.getOne($route.current.params.id);
            },
            options: function (usersAPIService) {
                return usersAPIService.getInfo('/options/register').then(function(response) {
                    return response.options;
                });
            }
        }
    });


});
