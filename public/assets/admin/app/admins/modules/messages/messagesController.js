app.controller("messagesListCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'deleter',
'listReader',
'messagesAPIService',
'elementsRequest',
function($scope,config,$location,authService,preloader,deleter,listReader,messagesAPIService,elementsRequest) {

    var modelName = 'Messages';
    var defaultAPIservice = messagesAPIService;
    var nameOfGroupOfElements = 'messages';

    $scope.nameRouteforControllerElement = 'mensagens';

    //Get User Information
    $scope = authService.userInformationToView($scope);
    $scope = listReader.readAndPaginate($scope,nameOfGroupOfElements,modelName,elementsRequest);

    $scope.deleteItemId=0;

    $scope.defineOrderTable=function(type){

        $scope.critOfOrder = type;
        $scope.orderOfcrit = !$scope.orderOfcrit;

    }

    $scope.recharge=function (page=null,itemSearch=null) {

        if (page==null) {

            if (itemSearch==null) {
                var request = defaultAPIservice.getAll();
            }else{
                var request = defaultAPIservice.getSearch(itemSearch);
            }

        }else{

            if (itemSearch==null) {
                var request = defaultAPIservice.getAll(page);
            }

        }
        request.then(function (response) {

            var elements = [];

            if (page==null) {

                if (itemSearch==null) {

                    elements = response[modelName].data;

                }else{

                    elements = response[modelName];

                }

            }else{

                elements = response[modelName].data;

            }

            $scope[nameOfGroupOfElements] = elements;

            preloader.hide();
        });

    }


    $scope.searchItem=function () {

        $scope.recharge(null,$scope.search);

    }

    $scope.goToPage=function(page){

        $(".preloader").fadeIn();
        $scope.recharge(page);

    }

    $scope.deleteItens=function () {

        return deleter.deleteAllInChest($scope,nameOfGroupOfElements,defaultAPIservice,'selected','id')

    }

    $scope.deleteItem=function(itemId){

        return deleter.deleteOne($scope,defaultAPIservice,itemId);

    }

}])

app.controller("messagesOneCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'deleter',
'messagesAPIService',
'element',
function($scope,config,$location,authService,preloader,deleter,messagesAPIService,element) {

    var defaultAPIservice = messagesAPIService;

    //Get User Information
    $scope = authService.userInformationToView($scope);

    $scope.message = element.element[0];

    $scope.send=function (answer) {
        $scope.message.answer = answer;
        //Show Preloader
        preloader.show();
        //Post the element of controller
        var request = messagesAPIService.put($scope.message, $scope.message.id);
        //If request is success
        request.success(function(data,status,header){
            //Down Preloader
            preloader.hide();
        });

    };

}])
