app.config(function($routeProvider,config){

  //***********messages Routes LIST***********//
  $routeProvider.when("/dashboard/mensagens",{
    templateUrl: config.VIEWS_DIRECTORY+"messages/views/messagesList.view.html",
    controller: "messagesListCrtl",
    title: "Minhas Mensagens",
    authenticated: true,
    usertype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      },
      elementsRequest:function (messagesAPIService) {
        return messagesAPIService.getAll();
      }
    }
  });

  //***********messages Routes SHOW***********//
  $routeProvider.when("/dashboard/mensagens/:id",{
    templateUrl: config.VIEWS_DIRECTORY+"messages/views/messagesOne.view.html",
    controller: "messagesOneCrtl",
    title: "Ver Mensagem",
    authenticated: true,
    usertype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      },
      element:function (messagesAPIService,$route) {
        return messagesAPIService.getOne($route.current.params.id);
      }
    }
  });


});
