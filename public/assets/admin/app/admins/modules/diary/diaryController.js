app.controller("diaryListCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'deleter',
'listReader',
'diaryAPIService',
'elementsRequest',
function($scope,config,$location,authService,preloader,deleter,
    listReader,diaryAPIService,elementsRequest) {

        var modelName = 'events';
        var defaultAPIservice = diaryAPIService;
        var nameOfGroupOfElements = 'events';

        $scope.nameRouteforControllerElement = 'agenda';

        //Get User Information
        $scope = authService.userInformationToView($scope);
        $scope = listReader.readAndPaginate($scope, nameOfGroupOfElements, modelName, elementsRequest);

        $scope.deleteItemId = 0;

        $scope.defineOrderTable=function(type){
            $scope.critOfOrder = type;
            $scope.orderOfcrit = !$scope.orderOfcrit;
        }

        $scope.diary = {};

        $scope.save=function (event = null) {

            preloader.show();

            if (!event.id) {
                var request = diaryAPIService.post(event);
            }else{
                var request = diaryAPIService.put(event, event.id);
            }

            request.success(function(data,status,header){
                $scope.event = {};
                $scope.eventPostShow = false;
                $scope.recharge(1);
            });

        };

        $scope.goToPostItem=function () {
            $scope.eventPostShow = (!$scope.eventPostShow);
        }

        $scope.recharge=function (page=null, itemSearch=null) {

            if (page==null) {

                if (itemSearch==null) {
                    var request = defaultAPIservice.getAll();
                }else{
                    var request = defaultAPIservice.getSearch(itemSearch);
                }

            }else{

                if (itemSearch==null) {
                    var request = defaultAPIservice.getAll(page);
                }

            }
            request.then(function (response) {

                var elements = [];

                if (page==null) {

                    if (itemSearch==null) {

                        elements = response[modelName].data;

                    }else{

                        elements = response[modelName];

                    }

                }else{

                    elements = response[modelName].data;

                }

                $scope[nameOfGroupOfElements] = elements;

                preloader.hide();
            });

        }

        $scope.searchItem=function () {
            $scope.recharge(null,$scope.search);
        }

        $scope.goToPage=function(page){
            $(".preloader").fadeIn();
            $scope.recharge(page);
        }

        $scope.deleteItens=function () {
            return deleter.deleteAllInChest($scope,nameOfGroupOfElements,defaultAPIservice,'selected','id')
        }

        $scope.deleteItem=function(itemId){
            return deleter.deleteOne($scope,defaultAPIservice,itemId);
        }

    }
])
