app.config(function($routeProvider,config){

    //***********diarys Routes LIST***********//
    $routeProvider.when("/dashboard/agenda",{
        templateUrl: config.VIEWS_DIRECTORY+"diary/views/diaryList.view.html",
        controller: "diaryListCrtl",
        title: "Agenda",
        authenticated: true,
        usertype: 'admin',
        resolve:{
            load: function (preloader){
                preloader.show();
            },
            elementsRequest:function (diaryAPIService) {
                return diaryAPIService.getAll();
            }
        }
    });

});
