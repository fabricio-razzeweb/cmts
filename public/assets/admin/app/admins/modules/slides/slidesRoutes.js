app.config(function($routeProvider,config){

  //***********slides Routes POST***********//
  $routeProvider.when("/dashboard/slides/novo",{
    templateUrl: config.VIEWS_DIRECTORY+"slides/views/slidesPost.view.html",
    controller: "slidesPostCrtl",
    title: "Novo Slide",
    authenticated: true,
    posttype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      }
    }
  });

  //***********slides Routes LIST***********//
  $routeProvider.when("/dashboard/slides",{
    templateUrl: config.VIEWS_DIRECTORY+"slides/views/slidesList.view.html",
    controller: "slidesListCrtl",
    title: "Meus Slides",
    authenticated: true,
    posttype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      },
      elementsRequest:function (slidesAPIService) {
        return slidesAPIService.getAll();
      }
    }
  });

  //***********slides Routes SHOW***********//
  $routeProvider.when("/dashboard/slides/:id",{
    templateUrl: config.VIEWS_DIRECTORY+"slides/views/slidesOne.view.html",
    controller: "slidesOneCrtl",
    title: "Editar Slide",
    authenticated: true,
    posttype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      },
      element:function (slidesAPIService,$route) {
        return slidesAPIService.getOne($route.current.params.id);
      }
    }
  });


});
