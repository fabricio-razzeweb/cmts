app.controller("slidesPostCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'groupReader',
'slidesAPIService',
function($scope,config,$location,authService,preloader,groupReader,slidesAPIService) {

    $scope = authService.userInformationToView($scope);
    $scope.slide = {};

    $scope.save=function () {

        preloader.show();
        var request = slidesAPIService.post($scope.slide);
        request.then(function(data,status,header){
            $location.path('dashboard/slides');
            preloader.hide();
        });

    };

}])

app.controller("slidesListCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'deleter',
'listReader',
'slidesAPIService',
'elementsRequest',
function($scope,config,$location,authService,preloader,deleter,listReader,slidesAPIService,elementsRequest) {

    var modelName = 'slides';
    var defaultAPIservice = slidesAPIService;
    var nameOfGroupOfElements = 'slides';

    $scope.nameRouteforControllerElement = 'slides';

    //Get User Information
    $scope = authService.userInformationToView($scope);
    $scope = listReader.readAndPaginate($scope,nameOfGroupOfElements,modelName,elementsRequest);

    $scope.deleteItemId=0;

    $scope.defineOrderTable=function(type){

        $scope.critOfOrder = type;
        $scope.orderOfcrit = !$scope.orderOfcrit;

    }

    $scope.goToPostItem=function () {

        preloader.show();
        $location.path("dashboard/"+$scope.nameRouteforControllerElement+"/novo");

    }


    $scope.recharge=function (page=null,itemSearch=null) {

        if (page==null) {

            if (itemSearch==null) {
                var request = defaultAPIservice.getAll();
            }else{
                var request = defaultAPIservice.getSearch(itemSearch);
            }

        }else{

            if (itemSearch==null) {
                var request = defaultAPIservice.getAll(page);
            }

        }
        request.then(function (response) {

            var elements = [];

            if (page==null) {

                if (itemSearch==null) {

                    elements = response[modelName].data;

                }else{

                    elements = response[modelName];

                }

            }else{

                elements = response[modelName].data;

            }

            $scope[nameOfGroupOfElements] = elements;

            preloader.hide();
        });

    }


    $scope.searchItem=function () {

        $scope.recharge(null,$scope.search);

    }

    $scope.goToPage=function(page){

        $(".preloader").fadeIn();
        $scope.recharge(page);

    }

    $scope.deleteItens=function () {

        return deleter.deleteAllInChest($scope,nameOfGroupOfElements,defaultAPIservice,'selected','id')

    }

    $scope.deleteItem=function(itemId){

        return deleter.deleteOne($scope,defaultAPIservice,itemId);

    }

    $scope[nameOfGroupOfElements].reverse();

}])

app.controller("slidesOneCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'deleter',
'groupReader',
'slidesAPIService',
'element',
function($scope,config,$location,authService,preloader,deleter,groupReader,slidesAPIService,element) {

    var defaultAPIservice = slidesAPIService;

    $scope.photoOn = true;

    //Get User Information
    $scope = authService.userInformationToView($scope);

    $scope.slide = element.slide;
    console.log(element);
    $scope.save=function () {

        preloader.show();

        var request = slidesAPIService.put($scope.slide, $scope.slide.id)
        .success(function(data,status,header){

            slidesAPIService.getOne($scope.slide.id)
            .then(function(response){
                $scope.slide = response.slide;
            });

            preloader.hide();
        });

    };

    $scope.deleteItem=function(){

        return deleter.deleteOne($scope,defaultAPIservice,itemId);

    }

}])
