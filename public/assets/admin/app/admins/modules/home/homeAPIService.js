app.factory("homeAPIService", function ($http,authService,config) {
    var homeAPIService = {};

    homeAPIService.path = function(){
        var course = config.API_URL;
        return course;
    };

    homeAPIService.getInfoDashboardHome = function (dateBegin = null, dateEnd = null) {
        var requestConfig = {
            transformRequest: angular.identity,
            headers: {'authorization': 'Bearer '+authService.getUserInfo("tk")}
        };

        var url = this.path()+"bussiness_analyse";

        if (dateBegin && dateEnd) {
            url += "?date_begin="+dateBegin+"&&date_end="+dateEnd;
        }

        return  $http.get(url, requestConfig);
    };

    return homeAPIService;
})
