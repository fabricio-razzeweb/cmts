app.controller("homeCrtl",[
'$scope',
'config',
'$location',
'authService',
'homeAPIService',
'bussinessAnalyse',
'preloader',
function($scope, config, $location, authService, homeAPIService, bussinessAnalyse, preloader) {

  //Get User Information
  $scope = authService.userInformationToView($scope);
  $scope.bussinessAnalyse = bussinessAnalyse;
  $scope.dateBegin = null;
  $scope.dateEnd = null;

  $scope.getAnalyse = function () {
      preloader.show();

      var dateIsSeted = $scope.dateBegin && $scope.dateEnd;
      var dateIsNotEmpty = $scope.dateBegin != '' && $scope.dateEnd != '';

      if (dateIsSeted && dateIsNotEmpty) {
         var request = homeAPIService.getInfoDashboardHome($scope.dateBegin, $scope.dateEnd);
      } else {
         var request = homeAPIService.getInfoDashboardHome();
      }

      request.then(function (response) {
          $scope.bussinessAnalyse = response.data.resultsOfAnalyse;
          preloader.hide();
      });
  }

}])
