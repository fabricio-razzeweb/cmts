app.config(function($routeProvider,config){


    //***********Dashboard Acess***********//
    $routeProvider.when("/",{
        templateUrl: config.VIEWS_DIRECTORY+"home/views/home.view.html",
        controller: "homeCrtl",
        title: "Dashboard",
        authenticated: true,
        usertype: 'admin',
        resolve:{
            bussinessAnalyse: function (homeAPIService) {

                return homeAPIService.getInfoDashboardHome()
                .then(function(response){
                    return response.data.resultsOfAnalyse;
                })

            }
        }
    });


});
