app.factory('alerter',function (config) {

    var alerter={};
    alerter.config=config;

    alerter.alert=function(msg) {
        $(".alertModal-msg")
        .text("")
        .append(msg);
        $("#alertModal").modal('show');
        $(".preloader").fadeOut('slow');

    };


    return alerter;
});
