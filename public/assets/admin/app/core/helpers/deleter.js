app.factory('deleter',function (config,alerter,preloader,authService) {

  var deleter={};
  deleter.config=config;

  deleter.deleteOne=function ($scope,APIservice,itemId) {
    preloader.show();
    var request = APIservice.delete(itemId);
    return request.then(function (response) {
        return $scope.recharge();
    });
  }

  deleter.deleteAllInChest=function($scope,nameOfGroupOfElements,APIservice,markOfDelete='selected',keyForDelete='id') {

    preloader.show();

    var elementsForRead = $scope[nameOfGroupOfElements];
    var i = 0;

    while ( i <= elementsForRead.length-1) {

      if(elementsForRead[i][markOfDelete]){

          APIservice.delete(elementsForRead[i][keyForDelete])
            .then(function successCallback(response) {

            if (!response.data.state.status==true) {

              return  alerter.alert("Desculpe, Ocorreu um erro inesperado!");

            }

          });

      }

      i++;

      if(i == elementsForRead.length){
        return $scope.recharge($scope);
      }

    }

  };

  return deleter;
});
