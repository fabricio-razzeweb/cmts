app.factory('listReader',function (config,alerter,preloader) {

  var listReader={};
  listReader.config=config;

  listReader.createPaginationForView=function ($scope) {

    var pageViews = [];

    for (var i = 1; i <= $scope.lastPage; i++) {

      pageViews.push({page: i});

    }

    $scope.pages = pageViews;

    return $scope;
  }


  listReader.readAndPaginate=function($scope,nameOfGroupOfElements,modelName,requestItens) {

    $scope[nameOfGroupOfElements]=requestItens[modelName]['data'];
    $scope.totalItens=requestItens[modelName]['total'];
    $scope.currentPage=requestItens[modelName]['current_page'];
    $scope.lastPage=requestItens[modelName]['last_page'];

    $scope = this.createPaginationForView($scope);

    return $scope;

  };

  listReader.rechargeList =function ($scope,page=null,itemSearch=null,defaultAPIservice,nameOfGroupOfElements,modelName) {

    if (page==null) {

      if (itemSearch==null) {
        var request = defaultAPIservice.getAll();
      }else{
        var request = defaultAPIservice.getSearch(itemSearch);
      }

    }else{

      if (itemSearch==null) {
        var request = defaultAPIservice.getAll(page);
      }

    }
    request.then(function (response) {

        var elements = [];

        if (page==null) {

          if (itemSearch==null) {

            elements = response[modelName].data;

          }else{

            elements = response[modelName];

          }

        }else{

          elements = response[modelName].data;

        }
        $scope[nameOfGroupOfElements] = elements;

    });

    preloader.hide();

  }

  listReader.readGroupAndGetOneAttrInAll = function(group,attrName){
      var value = "";
      var complement = "";

      for (var i = 0; i <= group.length-1; i++) {

          if (i == group.length-1) {
              complement = "";
          }else{
              complement = ",";
          }

          if (group[i][attrName]!=null && group[i][attrName]!=undefined) {
              value += group[i][attrName]+complement;
          }else{
              value += "0"+complement;
          }

      }

      console.log(value);
      return value.toString();
  };

  return listReader;
});
