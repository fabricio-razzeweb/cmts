app.factory('preloader',function (config,alerter) {

  var preloader={};
  preloader.config=config;

  preloader.show=function(username=null) {

      if (!$(".preloader").hasClass('on')) {
        $(".username-preloader").append(username);
        $(".preloader").addClass('on')
        $(".preloader").fadeIn();
      }

  };

  preloader.hide=function(username=null) {
      
    $(".preloader").removeClass('on');
    $(".preloader").fadeOut('slow');

  };


  return preloader;
});
