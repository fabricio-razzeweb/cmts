app.factory('groupReader',function (config) {

    var groupReader={};
    groupReader.config=config;

    groupReader.selectWhereIs=function(group,parameter,valueOfParameter,valueForInsert) {
        var newGroup = [];

        for (var i = 0; i <=group.length-1; i++) {
            if(group[i][parameter]===valueOfParameter){
                newGroup.push(group[i][valueForInsert]);
            }
        }

        return newGroup;
    };

    // Clear Chest
    groupReader.clearChest = function(group,parameter,valueOfParameter){

        var newGroup = [];

        for (var i = 0; i <=group.length-1; i++) {
            if(group[i][parameter]===valueOfParameter){
                group[i][parameter]=undefined;
                newGroup.push(group[i]);
            }
        }

        return newGroup;

    }

    groupReader.readAndSelectElementInGroups = function(group,groupForReflection,parameter,parameterValue,key,keyReflection){

        for (var i = 0; i <=group.length-1; i++) {
            for (var x = 0; x <=groupForReflection.length-1; x++) {
                if(groupForReflection[x][keyReflection]==group[i][key]){
                    group[i][parameter]=parameterValue;
                }
            }
        }

        return group;
    }

    return groupReader;
});
