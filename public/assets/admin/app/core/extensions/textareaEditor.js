
angular.module('ui.tinymce', [])
    .value('uiTinymceConfig', {
		
			selector: 'textarea',
			height: 500,
			theme: 'modern',
			language: 'pt_BR',
			plugins: [
			 'advlist autolink lists link image charmap print anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code'
					],
			toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify',
			menu: {
			    edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
			    insert: {title: 'Insert', items: 'link media | template hr'},
			    format: {title: 'Format', items: 'bold italic underline'}
			  },
			content_style: "p {margin: 5px; font-size: 14px; color: #333;} h1,h2,h3,h4,h5,h6 {margin: 10px; color: #111;}",
			formats: {
			    underline: {inline : 'span', 'classes' : 'underline', exact : true},
			    strikethrough: {inline : 'del'},
			    forecolor: {inline : 'span', classes : 'forecolor', styles : {color : '%value'}},
			    hilitecolor: {inline : 'span', classes : 'hilitecolor', styles : {backgroundColor : '%value'}}
			  }


    })
    .directive('uiTinymce', ['uiTinymceConfig', function(uiTinymceConfig) {
    uiTinymceConfig = uiTinymceConfig || {};
    var generatedIds = 0;
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ngModel) {
            var expression, options, tinyInstance;
            // generate an ID if not present
            if (!attrs.id) {
                attrs.$set('id', 'uiTinymce' + generatedIds++);
            }
            options = {
                // Update model when calling setContent (such as from the source editor popup)
                setup: function(ed) {
                    ed.on('init', function(args) {
                        ngModel.$render();
                    });
                    // Update model on button click
                    ed.on('ExecCommand', function(e) {
                        ed.save();
                        ngModel.$setViewValue(elm.val());
                        if (!scope.$$phase) {
                            scope.$apply();
                        }
                    });
                    // Update model on keypress
                    ed.on('KeyUp', function(e) {
                        console.log(ed.isDirty());
                        ed.save();
                        ngModel.$setViewValue(elm.val());
                        if (!scope.$$phase) {
                            scope.$apply();
                        }
                    });
                },
                mode: 'exact',
                elements: attrs.id
            };
            if (attrs.uiTinymce) {
                expression = scope.$eval(attrs.uiTinymce);
            } else {
                expression = {};
            }
            angular.extend(options, uiTinymceConfig, expression);
            setTimeout(function() {
                tinymce.init(options);
            });


            ngModel.$render = function() {
                if (!tinyInstance) {
                    tinyInstance = tinymce.get(attrs.id);
                }
                if (tinyInstance) {
                    tinyInstance.setContent(ngModel.$viewValue || '');
                }
            };
        }
    };
}]); 

