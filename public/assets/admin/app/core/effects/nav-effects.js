		//MENU LIST EFFECT
		$(window).ready(function() {

				/*****************CONFIG*******************/
				/********RESIZE SECTION*********/

				var size = 96;
				var height_of_header = $("header").height();
				var height_of_window = $(window).height();
				var height_of_nav = $("nav.menu").height();
				var one_pct_of_hw    = height_of_window/100;
				var porcent_of_nav_for_adatp =  height_of_nav/one_pct_of_hw;
				/*******************NAV ADAPTING*******************/
				/****************EFFECTS******************/

				/***************SECTION RESIZE**************/

				function section_resize(size) {
					var size = size;
					$("section").css({
						"width":size+"%",
						"float" :"right"
					});


				}


				function nav_ajust(height){

					$("nav.menu").css({
						"height":height+"%"
					});

				}
				/*******MENU-NAV-SHOW***********/
				$("#btn-menu").click(function (event) {
					event.preventDefault();

					var width = $(window).width();
					var direction = "";

					if (width<1023) {

						direction = "right";
						$("div.background").toggle(500);
					}else{

						direction = "left";
						section_resize(size);
					}

					var nav_on = $("nav.menu").hasClass("on");

					if (nav_on == false) {

						$("nav.menu").toggle(direction).addClass("on");

					}
					if (nav_on == true) {

						$("nav.menu").toggle(direction).removeClass("on");

						if (width<1023) {

							section_resize(100);

						}else{

							section_resize(100);
						}


					}


				})

				/****************NAV AJUST********************/
				nav_ajust(porcent_of_nav_for_adatp);
				/********************MENU EFFECTS**********************/
				//NAV-LATERAL-ITEM-EFFECT
 				$(".nav-item a.father").click(function (event) {
					event.preventDefault();
					$(this).parent().find(".sub-nav-list").toggle("slow");
 				})
 				//NAV-MENU-ITEM-EFFECT
	 			$(".menu-item a.father").click(function (event) {
					event.preventDefault();
					$(this).parent().find(".sub-menu-list").toggle("slow");
	 			})

				$(".preloader-on, .sub-menu-item, .sub-nav-item").click(function () {
					$(".preloader").fadeIn();
				})

		})
