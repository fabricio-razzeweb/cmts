$(window).ready(function () {
	$(".btn-del").click(function (event) {
		event.preventDefault();
		$(".alert-box.del,.background.background-transparent.black").toggle("slow");
	});

	$(".btn-no-del").click(function (event) {
		event.preventDefault();
		$(".alert-box.del,.background.background-transparent.black").toggle("slow");
	});
});
