app.directive('uiDefaultInput', function() {
  return {
    restrict: 'E',
    scope: {
      nameOfInput:'=nameOfInput',
      ngModel:'=ngModel',
      type:'=type',
      placeholder:'=placeholder',
      required:'=required',
      class:'=class',
    },
    templateUrl: onfig.VIEWS_DIRECTORY_ROOT+'interface/defaultInput.view.html',
  };
});
