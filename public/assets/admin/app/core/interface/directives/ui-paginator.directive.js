app.directive("uiPaginator",function(config){
  return{
    restrict: 'E',
    templateUrl:config.VIEWS_DIRECTORY_ROOT+'interface/paginator.view.html'
  };
});
