app.directive("uiAlertBoxDeleteOne",function(config){
  return{
    restrict: 'E',
    templateUrl:config.VIEWS_DIRECTORY_ROOT+'interface/alertBoxDeleteOne.view.html'
  };
});
