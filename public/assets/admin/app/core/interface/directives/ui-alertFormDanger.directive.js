app.directive("uiAlertFormDanger",function(config){
  return{
    restrict: 'E',
    scope:{
      typeOf:'=typeOf',
      show:'=show',
      message:'=message',
    },
    templateUrl:config.VIEWS_DIRECTORY_ROOT+'interface/alertFormDanger.view.html'
  };
});
