app.directive("uiAlertFormWarning",function(config){
  return{
    restrict: 'E',
    templateUrl:config.VIEWS_DIRECTORY_ROOT+'interface/alertFormWarning.view.html'
  };
});
