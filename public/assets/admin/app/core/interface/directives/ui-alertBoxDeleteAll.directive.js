app.directive("uiAlertBoxDeleteAll",function(config){
  return{
    restrict: 'E',
    templateUrl:config.VIEWS_DIRECTORY_ROOT+'interface/alertBoxDeleteAll.view.html'
  };
});
