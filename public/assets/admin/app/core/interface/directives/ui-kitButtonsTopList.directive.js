app.directive("uiKitButtonsTopList",function(config){
  return{
    restrict: 'E',
    templateUrl:config.VIEWS_DIRECTORY_ROOT+'interface/kitButtonsTopList.view.html'
  };
});
