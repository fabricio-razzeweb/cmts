app.directive("uiLoad",function(config){
  return{
    restrict: 'E',
    templateUrl:config.VIEWS_DIRECTORY_ROOT+'interface/load.view.html'
  };
});
