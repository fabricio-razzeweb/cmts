app.directive("uiFooter",function(config){
  return{
    restrict: 'E',
    templateUrl:config.VIEWS_DIRECTORY_ROOT+'interface/footer.view.html'
  };
});
