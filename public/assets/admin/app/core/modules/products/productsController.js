app.controller("productsPostCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'groupReader',
'productsAPIService',
'tags',
'categorys',
'dimensions',
'daysDisponibilitys',
function($scope,config,$location,authService,preloader,groupReader,productsAPIService,tags,categorys,dimensions,daysDisponibilitys) {

    $scope.dimensions = dimensions.ProductsDimensions;
    $scope.tags = tags.Tags;
    $scope.categorys = categorys.Categorys;
    $scope.daysDisponibilitys = daysDisponibilitys.ProductsDaysDisponibilitys;
    $scope.subCategorys = {};
    $scope.photoIsRequired = true;

    //Get User Information
    $scope = authService.userInformationToView($scope);
    //Set new item form post
    $scope.product = {};

    $scope.rechargeImages=function(){
        productsAPIService.getImagesOfProduct($scope.product.id)
        .then(function(response){
            $scope.productImages = response.imagesOfProduct;
        });
    }

    $scope.deleteImage=function(imageId){
        productsAPIService.deleteImagesOfProduct($scope.product.id,imageId)
        .then(function(response){
            $scope.rechargeImages();
        });
    }

    $scope.dropzonePhoto = {
        'options': { // passed into the Dropzone constructor
            'url': 'dashboard/api/products/image',
            'paramName': 'image',
            'headers': {'Content-Type': undefined,
            'Authorization': 'Bearer'+authService.getUserInfo("tk")},
            'dictDefaultMessage': "Arraste as fotos aqui ou clique aqui para adicionar"+
            " <br><br><a class='btn btn-success'><i class='fa fa-picture-o' aria-hidden='true'></i> Adicionar Foto</a>",
        },
        'eventHandlers': {
            'sending': function (file, xhr, formData) {
                formData.append("product_id",$scope.product.id);
                formData.append("_token",CSRFPROTECTION);

            },
            'success': function (file, response) {
                $scope.rechargeImages($scope.product.id);
            }
        }
    };

    $scope.checkGroupOfTags=function () {
        $scope.tagsInProduct = groupReader.selectWhereIs($scope.tags,'selected',true,'id');
        return $scope.tagsInProduct;
    }

    $scope.save=function () {

        $scope.product.tags=$scope.checkGroupOfTags().toString();
        //Show Preloader
        preloader.show();
        if ($scope.product.id>0) {
            //Put the element of controller
            var request = productsAPIService.put($scope.product,$scope.product.id);
        }else{
            //Post the element of controller
            var request = productsAPIService.post($scope.product);
        }
        //If request is success
        request.success(function(data,status,header){
            $scope.saved = true;
            //Set element id
            $scope.product.id = data.execution.id;
            //Show Archives Drop Zone
            $("#product-form")
            .removeClass("col-md-offset-1")
            .removeClass("col-md-10")
            .addClass('col-md-12')
            .find(".box-body, .box-footer")
            .hide('slow');

            $("#photo-drop, #photo-list")
            .removeClass("col-md-6")
            .addClass("col-md-12");

            $("#archives-area")
            .show("slow")
            .removeClass("col-md-6")
            .addClass("col-md-12");

            $("#product-form .box-header")
            .click(function(){

                $("#product-form .box-body, #product-form .box-footer")
                .toggle('slow');

            });

            productsAPIService.getOne($scope.product.id)
            .then(function(response){
                $scope.product = response.product;
            });
            //Down Preloader
            preloader.hide();
        });

    };

    $scope.simulateSell=function(){
        $scope.product.valueOfSeller = ($scope.product.price*0.891);
    };

    $scope.getSubCategorysOfCategory = function(){
        var category = $scope.product.category_id
        productsAPIService.getOptions("categorys/"+category+"/subcategorys")
        .then(function(response){
                $scope.subCategorys = response.SubCategorys;
        });
    }
}])

app.controller("productsListCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'deleter',
'alerter',
'listReader',
'productsAPIService',
'elementsRequest',
function($scope,config,$location,authService,preloader,deleter,alerter,listReader,productsAPIService,elementsRequest) {

    var modelName = 'Products';
    var defaultAPIservice = productsAPIService;
    var nameOfGroupOfElements = 'products';
    $scope.nameRouteforControllerElement = 'produtos';

    //Get User Information
    $scope = authService.userInformationToView($scope);
    $scope = listReader.readAndPaginate($scope,nameOfGroupOfElements,modelName,elementsRequest);

    $scope.deleteItemId=0;

    $scope.defineOrderTable=function(type){

        $scope.critOfOrder = type;
        $scope.orderOfcrit = !$scope.orderOfcrit;

    }

    $scope.goToPostItem=function () {

        preloader.show();
        $location.path("dashboard/"+$scope.nameRouteforControllerElement+"/novo");

    }


    $scope.recharge=function (page=null,itemSearch=null) {

        if (page==null) {

            if (itemSearch==null) {
                var request = defaultAPIservice.getAll();
            }else{
                var request = defaultAPIservice.getSearch(itemSearch);
            }

        }else{

            if (itemSearch==null) {
                var request = defaultAPIservice.getAll(page);
            }

        }
        request.then(function (response) {

            var elements = [];

            if (page==null) {

                if (itemSearch==null) {

                    elements = response[modelName].data;

                }else{

                    elements = response[modelName];

                }

            }else{

                elements = response[modelName].data;

            }

            $scope[nameOfGroupOfElements] = elements;

            preloader.hide();
        });

    }


    $scope.searchItem=function () {

        $scope.recharge(null,$scope.search);

    }

    $scope.goToPage=function(page){

        $(".preloader").fadeIn();
        $scope.recharge(page);

    }

    $scope.deleteItens=function () {

        return deleter.deleteAllInChest($scope,nameOfGroupOfElements,defaultAPIservice,'selected','id')

    }

    $scope.deleteItem=function(itemId){

        return deleter.deleteOne($scope,defaultAPIservice,itemId);

    }

    $scope.duplicateItem=function(itemId){
        defaultAPIservice.duplicate(itemId)
        .then(function(response){
            $scope.recharge();
            alerter.show(reponse.execution.msg);
        });
    }
}])

app.controller("productsOneCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'groupReader',
'deleter',
'productsAPIService',
'element',
'tags',
'categorys',
'comments',
'dimensions',
'daysDisponibilitys',
function($scope,config,$location,authService,preloader,groupReader,deleter,productsAPIService,element,tags,categorys,comments,dimensions,daysDisponibilitys) {

    var defaultAPIservice = productsAPIService;

    //Get User Information
    $scope = authService.userInformationToView($scope);

    $scope.product = element.product;
    $scope.tags = tags.Tags;
    $scope.categorys = categorys.Categorys;
    $scope.daysDisponibilitys = daysDisponibilitys.ProductsDaysDisponibilitys;
    $scope.dimensions = dimensions.ProductsDimensions;
    $scope.photoIsRequired = false;
    $scope.comment = {};
    $scope.comments = comments.comments;
    //Get User Information
    $scope = authService.userInformationToView($scope);

    $scope.rechargeImages=function(){
        productsAPIService.getImagesOfProduct($scope.product.id)
        .then(function(response){
            $scope.productImages = response.imagesOfProduct;
        });
    }

    $scope.rechargeImages();

    $scope.readAndSelectTags = function(){
        return $scope.tags = groupReader.readAndSelectElementInGroups($scope.tags,$scope.product.tags,'selected',true,'id','id');
    }

    $scope.readAndSelectTags();

    $scope.deleteImage=function(imageId){
        productsAPIService.deleteImagesOfProduct($scope.product.id,imageId)
        .then(function(response){
            $scope.rechargeImages();
        });
    }

    $scope.dropzonePhoto = {
        'options': { // passed into the Dropzone constructor
            'url': 'dashboard/api/products/image',
            'paramName': 'image',
            'headers': {'Content-Type': undefined,
            'Authorization': 'Bearer'+authService.getUserInfo("tk")},
            'dictDefaultMessage': "Arraste as fotos aqui ou clique aqui para adicionar"+
            " <br><br><a class='btn btn-success'><i class='fa fa-picture-o' aria-hidden='true'></i> Adicionar Foto</a>",
        },
        'eventHandlers': {
            'sending': function (file, xhr, formData) {
                formData.append("product_id",$scope.product.id);
                formData.append("_token",CSRFPROTECTION);

            },
            'success': function (file, response) {
                $scope.rechargeImages($scope.product.id);
            }
        }
    };

    $scope.checkGroupOfTags=function () {
        $scope.tagsInProduct = groupReader.selectWhereIs($scope.tags,'selected',true,'id');
        return $scope.tagsInProduct;
    }

    $scope.save=function () {
        preloader.show();

        $scope.product.tags=$scope.checkGroupOfTags().toString();

        if ($scope.product.addMoreDaysInAdd!=true) {
            //For day of finhished ad not update
            $scope.product.products_time_disponibility_id = undefined;
        }
        
        var request = productsAPIService.put($scope.product,$scope.product.id)
        .success(function(data,status,header){

            productsAPIService.getOne($scope.product.id)
            .then(function(response){
                $scope.product = response.product;
                preloader.hide();
            });

        });

    };

    $scope.deleteItem=function(){

        return deleter.deleteOne($scope,defaultAPIservice,itemId);

    }

    $scope.showArchivesArea = function(){
        $(document).ready(function(){
            $("#main-form")
            .removeClass("col-md-10")
            .removeClass("col-md-offset-1")
            .addClass("col-md-6");
            $("#archives-area").show("slow");
        });
    };

    $scope.showArchivesArea();

    $scope.postComment=function () {

        preloader.show();

        $scope.comment.product_id = $scope.product.id;

        var request = defaultAPIservice.postComment($scope.comment);

        request.success(function(response){
            defaultAPIservice.getInfo("/"+$scope.product.id+"/comments")
            .then(function (response) {
                $scope.comments = response.comments;
            })
            preloader.hide();
        });

    };

    $scope.deleteComment=function (commentId) {

        preloader.show();

        var request = defaultAPIservice.deleteComment(commentId, $scope.product.id);

        request.success(function(data,status,header){
            defaultAPIservice.getInfo("/"+$scope.product.id+"/comments")
            .then(function (response) {
                $scope.comments = response.comments;
            })
            preloader.hide();
        });

    };

    $scope.reportComment=function (commentId) {

        preloader.show();

        var request = defaultAPIservice.reportComment(commentId, $scope.product.id);

        request.success(function(response){
            alerter.alert(response.execution.msg);
            defaultAPIservice.getInfo("/"+$scope.product.id+"/comments")
            .then(function (response) {
                $scope.comments = response.comments;
            })
            preloader.hide();
        });

    };

    $scope.simulateSell=function(){
        $scope.product.valueOfSeller = ($scope.product.price*0.891);
    };

    $scope.getSubCategorysOfCategory = function(){
        var category = $scope.product.category_id
        productsAPIService.getOptions("categorys/"+category+"/subcategorys")
        .then(function(response){
                $scope.subCategorys = response.SubCategorys;
        });
    }

    $scope.getSubCategorysOfCategory();

    $scope.ajustCommentArea = function(){
        $(document).ready(function(){
            $("#comments-list")
            .removeClass("col-md-6")
            .addClass("col-md-12");
        });
    };

    $scope.ajustCommentArea();

}])
