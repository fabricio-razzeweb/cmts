app.config(function($routeProvider,config){

  //***********products Routes POST***********//
  $routeProvider.when("/dashboard/produtos/novo",{
    templateUrl:"../assets/admin/app/core/modules/products/views/productsPost.view.html",
    controller: "productsPostCrtl",
    title: "Nova Produto",
    authenticated: true,
    usertype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      },
      checkUserIsValid: function(completeRegisterAPIService,$location,alerter){
          completeRegisterAPIService.checkUserIsValid()
          .then(function(response){
              let userIsValid = response.execution.status;
              if (!userIsValid) {
                  return $location.path("dashboard/register/complete");
              }else if(response.execution.msg){
                  alerter.alert(response.execution.msg);
              }
          });
      },
      tags: function (tagsAPIService){
        return tagsAPIService.getSearch("*");
      },
      categorys: function (productsAPIService){
        return productsAPIService.getOptions("categorys?search=*");
      },
      daysDisponibilitys: function (productsAPIService){
        return productsAPIService.getOptions("days_disponibility?search=*");
      },
      dimensions: function (productsAPIService){
        return productsAPIService.getOptions("dimensions");
      }
    }
  });

  //***********products Routes LIST***********//
  $routeProvider.when("/dashboard/produtos",{
    templateUrl: "../assets/admin/app/core/modules/products/views/productsList.view.html",
    controller: "productsListCrtl",
    title: "Meus Produtos",
    authenticated: true,
    usertype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      },
      elementsRequest:function (productsAPIService) {
        return productsAPIService.getAll();
      }
    }
  });

  //***********products Routes SHOW***********//
  $routeProvider.when("/dashboard/produtos/:id",{
    templateUrl: "../assets/admin/app/core/modules/products/views/productsOne.view.html",
    controller: "productsOneCrtl",
    title: "Editar Produto",
    authenticated: true,
    usertype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      },
      element:function (productsAPIService,$route) {
        return productsAPIService.getOne($route.current.params.id);
      },
      tags: function (tagsAPIService){
        return tagsAPIService.getSearch("*");
      },
      categorys: function (productsAPIService){
        return productsAPIService.getOptions("categorys?search=*");
      },
      daysDisponibilitys: function (productsAPIService){
        return productsAPIService.getOptions("days_disponibility?search=*");
      },
      comments: function (productsAPIService,$route){
        return productsAPIService.getInfo("/"+$route.current.params.id+"/comments");
      },
      dimensions: function (productsAPIService){
        return productsAPIService.getOptions("dimensions");
      }
    }
  });


});
