app.factory("productsAPIService", function ($http,authService,config) {

    var productsAPI={};
    //This attr is fields of element
    productsAPI.fillable = [
      'name',
      'description',
      'main_image',
     // 'size',
      'price',
      'quantity',
      'sub_categorys_id',
      'product_dimensions_id',
      'users_id',
      'products_time_disponibility_id',
      'tags',
    ];
    /*
    * Method preparedDataToTransfer()
    * @param dataElement - this param is data for send to server
    * @param method - this param is method of request
    * @return data - is used for send to server
    */
    productsAPI.preparedDataToTransfer=function (dataElement,method) {
      var data = new FormData;
      // Read the attr array fillable for add in data for send to server
      for (var i = 0; i < this.fillable.length; i++) {
        // Check if attr exist
        if (dataElement[this.fillable[i]]!=undefined ||
        dataElement[this.fillable[i]]!=null) {
          // Add value to attr
          data.append(this.fillable[i],dataElement[this.fillable[i]]);
        }
      }
      if(method=="PUT"){
        data.append("_method","PUT");
      }
      data.append("users_id",authService.getUserId());
      // return data to send
      return data;
    };

    productsAPI.path =function(){
  		var course = config.API_URL+"products";//<<CHANGE
  		return course;
  	};

    productsAPI.inputRequestServiceFactory=function (method,element,id=undefined) {
      var requestConfig = authService.authRequest();
      if (id==undefined) {var path =this.path();}else{var path =this.path()+"/"+id;}
      return $http.post(path,this.preparedDataToTransfer(element,method),requestConfig)//;
    };

    productsAPI.outputRequestServiceFactory=function (elementId=null,page=null,searchItem=null) {
      var requestConfig = authService.authRequest();
      //Request requiriments adjust for request, elementId and pages
      if(elementId!=null){elementId= "/"+elementId}else{elementId="";};
      if(page!=null){requestConfig['params']={page: page}};
      if(searchItem!=null){requestConfig['params']={search: searchItem}};
      return $http.get(this.path()+elementId,requestConfig).then(function (response) {
                          return response.data;
                      })//;

    };

    productsAPI.post = function (element) {
        var execution = authService.checkRequest(this.inputRequestServiceFactory("POST",element),this);
        return execution;
    };

    productsAPI.put = function (element,idElement) {
        var execution = authService.checkRequest(this.inputRequestServiceFactory("PUT",element,idElement),this);
        return execution;
    };

    productsAPI.getAll=function (page=null) {
        var execution = authService.checkRequest(this.outputRequestServiceFactory(null,page),this);
        return execution;
    };

    productsAPI.getOne=function (idElement=null) {
        var execution =authService.checkRequest(this.outputRequestServiceFactory(idElement),this);
        return execution;
    };

    productsAPI.getSearch=function (searchItem) {
        var execution = authService.checkRequest(this.outputRequestServiceFactory(null,null,searchItem),this);
        return execution;
    };

    productsAPI.delete=function (idElement) {
      var requestConfig = authService.authRequest();
      return authService.checkRequest($http.delete(this.path()+"/"+idElement,requestConfig));
    }

    productsAPI.duplicate=function (itemId){
      var requestConfig = authService.authRequest();
      return authService.checkRequest($http.get(this.path()+"/"+itemId+"/duplicate",requestConfig));
    }

    productsAPI.getOptions=function (option){
      var requestConfig = authService.authRequest();
      return $http.get(this.path()+"/options/"+option,requestConfig)
      .then(function (response) {
                          return response.data;
                      });
    }

    productsAPI.getInfo=function (infoUrl) {
      var requestConfig = authService.authRequest();
      return $http.get(this.path()+infoUrl,requestConfig).then(function (response) {
                          return response.data;
                      })//;
    };

    productsAPI.getImagesOfProduct=function (elementId){
      var requestConfig = authService.authRequest();
      return $http.get(this.path()+"/"+elementId+"/images",requestConfig).then(function (response) {
                          return response.data;
                      })//;
    }

    productsAPI.deleteImagesOfProduct=function (idElement,photoId) {
      var path = this.path()+"/"+idElement+"/image/"+photoId;
      return authService.checkRequest($http.delete(path,
                      { transformRequest: angular.identity,
                        headers: {'Authorization': 'Bearer'+authService.getUserInfo("tk")},
                        data: {_method:"delete"},
                      }));
    }

    productsAPI.postComment=function (comment) {
      var data = new FormData;
      data.append('product_id',comment.product_id);
      data.append('user_id',authService.getUserId());
      data.append('content',comment.content);

      var requestConfig = authService.authRequest();
      return authService.checkRequest($http.post(this.path()+"/"+comment.product_id+"/comments",data,requestConfig));
    }

    productsAPI.deleteComment=function (idElement,productId) {
      var requestConfig = authService.authRequest();
      requestConfig.data = {_method:"delete"};
      return authService.checkRequest($http.delete(this.path()+"/"+productId+"/comments/"+idElement,requestConfig));
    }

    productsAPI.reportComment=function (idElement,productId) {
      var data = new FormData;
      data.append('product_id',productId);
      data.append('comment_id',idElement);

      var requestConfig = authService.authRequest();
      return authService.checkRequest($http.post(this.path()+"/"+productId+"/comments/report",data,requestConfig));
    }

    return productsAPI;
})
