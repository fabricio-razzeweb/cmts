app.controller("completeRegisterCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'deleter',
'alerter',
'listReader',
'completeRegisterAPIService',
'user',
'userAddresses',
'neighborhoods',
'UsersAddressAPIService',
'issuers',
function($scope,config,$location,authService,preloader,deleter,alerter,listReader,completeRegisterAPIService,user,userAddresses,neighborhoods,UsersAddressAPIService,issuers) {

    //Get User Information
    $scope = authService.userInformationToView($scope);

    $scope.user = user;
    $scope.issuers = issuers.issuers;
    $scope.newUserAddress = {};

    $scope.checkZipCode = function(zipCode){
        UsersAddressAPIService.checkZipCode(zipCode)
        .then(function(response){
            let status = response.execution.status;
            if (!status) {
                alerter.alert("Oppss... Desculpe, este CEP não é válido digite outro por favor");
            }else{
                let data = response.execution.data;
                $scope.newUserAddress.street = data.street;
                $scope.newUserAddress.neighborhoods_id = data.neighborhoods_id;
            }
        });
    }

    $scope.userAddresses = userAddresses.usersAddress.data;
    $scope.neighborhoods = neighborhoods.Neighborhoods;

    $scope.saveAddress = function(address){
        UsersAddressAPIService.post(address)
        .then(function(response){
            UsersAddressAPIService.getAll()
            .then(function(response){
                $scope.userAddresses = response.usersAddress.data;
                $('#newAddressModal').modal('hide');
            });
            alerter.alert(response.execution.msg);
        });
    };

    $scope.myAccount = {};

    $scope.selectAddress = function(address){
        $scope.isValid = ($scope.myAccount.rg!=null &&
                            $scope.myAccount.birth_date!=null &&
                            $scope.myAccount.issuer!=null &&
                            $scope.myAccount.issue_date!=null);

        $scope.selectedUserAddress = address;
        $("#chooseAddressModal").modal('show');

    };

    $scope.setAddressInOrder = function () {
        defaultAPIservice.setSellerAddress($scope.selectedOrder, $scope.selectedUserAddress.id)
        .then(function(response){
            $scope.recharge();
            $(".modal").modal('hide');
        });
    };

    $scope.save = function(){
        if ($scope.selectedUserAddress!=null && $scope.myAccount!=null) {
            let data = {
                'rg':$scope.myAccount.rg,
                'issuer':$scope.myAccount.issuer,
                'issue_date':$scope.myAccount.issue_date,
                'birth_date':$scope.myAccount.birth_date,
                'users_address_id':$scope.selectedUserAddress.id,
            }

            preloader.show();

            completeRegisterAPIService
            .post(data)
            .then(function(response){
                alerter.alert(response.execution.msg);

                $location.path('dashboard/produtos/novo');
                preloader.hide();
            },function(response){
                if(response.execution.msg){
                    alerter.alert(response.execution.msg);
                }
                preloader.hide();
            });
        }
    }
}])
