app.factory("completeRegisterAPIService", function ($http,authService,config) {

    var completeRegisterAPIService={};
    //This attr is fields of element
    completeRegisterAPIService.fillable = [
        'rg',
        'issuer',
        'issue_date',
        'birth_date',
        'cep',
        'street',
        'neighborhoods_id',
        'number',
        'complement',
        'users_address_id',
    ];

    /*
    * Method preparedDataToTransfer()
    * @param dataElement - this param is data for send to server
    * @param method - this param is method of request
    * @return data - is used for send to server
    */
    completeRegisterAPIService.preparedDataToTransfer=function (dataElement,method) {
      var data = new FormData;
      // Read the attr array fillable for add in data for send to server
      for (var i = 0; i < this.fillable.length; i++) {
        // Check if attr exist
        if (dataElement[this.fillable[i]]!=undefined ||
        dataElement[this.fillable[i]]!=null) {
          // Add value to attr
          data.append(this.fillable[i],dataElement[this.fillable[i]]);
        }
      }
      if(method=="PUT"){
        data.append("_method","PUT");
      }
      data.append("users_id",authService.getUserId());
      // return data to send
      return data;
    };

    completeRegisterAPIService.path =function() {
  		var course = config.API_URL+"register/user/validate";//<<CHANGE
  		return course;
  	};

    completeRegisterAPIService.inputRequestServiceFactory=function (method,element,id=undefined) {
      var requestConfig = authService.authRequest();
      if (id==undefined) {var path =this.path();}else{var path =this.path()+"/"+id;}
      return $http.post(path,this.preparedDataToTransfer(element,method),requestConfig);
    };

    completeRegisterAPIService.outputRequestServiceFactory=function (elementId=null,page=null,searchItem=null) {
      var requestConfig = authService.authRequest();
      //Request requiriments adjust for request, elementId and pages
      if(elementId!=null){elementId= "/"+elementId}else{elementId="";};
      if(page!=null){requestConfig['params']={page: page}};
      if(searchItem!=null){requestConfig['params']={search: searchItem}};
      return $http.get(this.path()+elementId,requestConfig).then(function (response) {
                          return response.data;
                      })//;

    };

    completeRegisterAPIService.post = function (element) {
        var execution = authService.checkRequest(this.inputRequestServiceFactory("POST",element),this);
        return execution;
    };

    completeRegisterAPIService.put = function (element,idElement) {
        var execution = authService.checkRequest(this.inputRequestServiceFactory("PUT",element,idElement),this);
        return execution;
    };

    completeRegisterAPIService.getAll = function (page=null) {
        var execution = authService.checkRequest(this.outputRequestServiceFactory(null,page),this);
        return execution;
    };

    completeRegisterAPIService.getOne=function (idElement=null) {
        var execution =authService.checkRequest(this.outputRequestServiceFactory(idElement),this);
        return execution;
    };

    completeRegisterAPIService.getSearch=function (searchItem) {
        var execution = authService.checkRequest(this.outputRequestServiceFactory(null,null,searchItem),this);
        return execution;
    };

    completeRegisterAPIService.delete=function (idElement) {
      return $http.delete(this.path()+"/"+idElement,{
                        transformRequest: angular.identity,
                        headers: {'Authorization': 'Bearer'+authService.getUserInfo("tk")},
                        data: {_method:"delete"},
                      });
    }

    completeRegisterAPIService.getOptions=function (optionUrl) {
      var requestConfig = authService.authRequest();
      return $http.get(this.path()+"/options/"+optionUrl,requestConfig).then(function (response) {
                          return response.data;
                      });

    };

    completeRegisterAPIService.checkUserIsValid = function(){
        var requestConfig = authService.authRequest();
        return $http.get(this.path(),requestConfig).then(function (response) {
                            return response.data;
                        });
    };

    return completeRegisterAPIService;
})
