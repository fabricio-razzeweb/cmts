app.config(function($routeProvider,config){

    //***********completeRegister Routes POST***********//
    $routeProvider.when("/dashboard/register/complete",{
        templateUrl: "../assets/admin/app/core/modules/completeRegister/views/completeRegister.view.html",
        controller: "completeRegisterCrtl",
        title: "Meus Dados",
        authenticated: true,
        usertype: 'admin',
        resolve:{
            load: function (preloader){
                preloader.show();
            },
            user:function(myAccountAPIService){
                return myAccountAPIService.getOne();
            },
            userAddresses:function(UsersAddressAPIService){
                return UsersAddressAPIService.getAll();
            },
            neighborhoods:function(UsersAddressAPIService){
                return UsersAddressAPIService.getOptions('neighborhoods?search=*');
            },
            issuers:function(completeRegisterAPIService){
                return completeRegisterAPIService.getOptions('issuers');
            }
        }
    });


});
