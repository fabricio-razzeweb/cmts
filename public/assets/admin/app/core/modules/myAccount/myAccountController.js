

app.controller("myAccountCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'deleter',
'alerter',
'myAccountAPIService',
'element',
function($scope,config,$location,authService,preloader,deleter,alerter,myAccountAPIService,element) {


  var defaultAPIservice = myAccountAPIService;

  //Get User Information
  $scope = authService.userInformationToView($scope);

  $scope.user = element.user;
  $scope.oldMail = element.user.email;

  $scope.save=function () {

    preloader.show();
    var request = myAccountAPIService.put($scope.user)
      .success(function(data,status,header){
          preloader.hide();
        });

  };

  $scope.checkMail=function () {
    
    if ($scope.oldMail!=$scope.user.email) {
      var request = myAccountAPIService.checkMail($scope.user.email)
        .success(function(response){

            if (response.execution.status==false) {

              alerter.alert(response.execution.msg);
              $scope.user.email="";

            }

          });
    }

  };

}])
