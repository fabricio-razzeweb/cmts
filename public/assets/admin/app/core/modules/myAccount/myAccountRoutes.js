app.config(function($routeProvider,config){

  //***********users Routes POST***********//
  $routeProvider.when("/dashboard/minha_conta",{
    templateUrl: "../assets/admin/app/core/modules/myAccount/views/myAccount.view.html",
    controller: "myAccountCrtl",
    title: "Minha Conta",
    authenticated: true,
    usertype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      },
      element:function(myAccountAPIService){
        return myAccountAPIService.getOne();
      }
    }
  });


});
