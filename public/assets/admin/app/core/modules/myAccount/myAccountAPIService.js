app.factory("myAccountAPIService", function ($http,authService,config) {

    var myAccountAPI={};
    //This attr is fields of element
    myAccountAPI.fillable = [
      'name',
      'email',
      'password',
      'cpf',
      'photo',
      'phone',
      'birth_date',
      'oldPassword',
    ];
    /*
    * Method preparedDataToTransfer()
    * @param dataElement - this param is data for send to server
    * @param method - this param is method of request
    * @return data - is used for send to server
    */
    myAccountAPI.preparedDataToTransfer=function (dataElement,method) {
      var data = new FormData;
      // Read the attr array fillable for add in data for send to server
      for (var i = 0; i < this.fillable.length; i++) {
        // Check if attr exist
        if (dataElement[this.fillable[i]]!=undefined ||
        dataElement[this.fillable[i]]!=null) {
          // Add value to attr
          data.append(this.fillable[i],dataElement[this.fillable[i]]);
        }
      }
      if(method=="PUT"){
        data.append("_method","PUT");
      }
      data.append("myAccount_id",authService.getUserId());
      // return data to send
      return data;
    };

    myAccountAPI.path =function(){
  		var course = config.API_URL+"my_account";//<<CHANGE
  		return course;
  	};

    myAccountAPI.inputRequestServiceFactory=function (method,element,id=undefined) {
      var requestConfig = authService.authRequest();
      if (id==undefined) {var path =this.path();}else{var path =this.path()+"/"+id;}
      return $http.post(path,this.preparedDataToTransfer(element,method),requestConfig)//;
    };

    myAccountAPI.outputRequestServiceFactory=function (elementId=null,page=null,searchItem=null) {
      var requestConfig = authService.authRequest();
      //Request requiriments adjust for request, elementId and pages
      if(elementId!=null){elementId= "/"+elementId}else{elementId="";};
      if(page!=null){requestConfig['params']={page: page}};
      if(searchItem!=null){requestConfig['params']={search: searchItem}};
      return $http.get(this.path()+elementId,requestConfig).then(function (response) {
                          return response.data;
                      })//;
    };


    myAccountAPI.put = function (element) {
        var execution = authService.checkRequest(this.inputRequestServiceFactory("PUT",element),this);
        return execution;
    };

    myAccountAPI.getOne=function (idElement=null) {
        var execution =authService.checkRequest(this.outputRequestServiceFactory(idElement),this);
        return execution;
    };

    myAccountAPI.getInfo=function (infoUrl) {
      var requestConfig = authService.authRequest();
      return $http.get(this.path()+infoUrl,requestConfig).then(function (response) {
                          return response.data;
                      })//;
    };

    myAccountAPI.checkMail=function (mail) {
      var requestConfig = authService.authRequest();
      data = new FormData;
      data.append('email',mail);
      return $http.post(this.path()+"/check_mail",data,requestConfig)//;
    };

    return myAccountAPI;
})
