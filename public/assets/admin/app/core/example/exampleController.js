app.controller("examplesPostCrtl",
['$scope',
  'config',
  '$location',
  'authService',
  'preloader',
  'examplesAPIService',
  function($scope,config,$location,authService,preloader,examplesAPIService) {

  //Get User Information
  $scope = authService.userInformationToView($scope);
  //Set new item form post
  $scope.example = {};

  $scope.post=function () {

    //Show Preloader
    preloader.show();
    //Post the element of controller
    var request = examplesAPIService.post($scope.example);
    //If request is success
    request.success(function(data,status,header){
        //Set element id
        $scope.example.id = data.execution.id;
        //Down Preloader
        preloader.hide();
    });

  };

}])

app.controller("examplesListCrtl",
['$scope',
  'config',
  '$location',
  'authService',
  'preloader',
  'deleter',
  'listReader',
  'examplesAPIService',
  'elementsRequest',
  function($scope,config,$location,authService,preloader,deleter,listReader,examplesAPIService,elementsRequest) {

    var modelName = 'examples';
    var defaultAPIservice = examplesAPIService;
    var nameOfGroupOfElements = 'examples';

    $scope.nameRouteforControllerElement = 'exemplo';

    //Get User Information
    $scope = authService.userInformationToView($scope);
    $scope = listReader.readAndPaginator($scope,modelName,elementsRequest);

    $scope.deleteItemId=0;

    $scope.defineOrderTable=function(type){

      $scope.critOfOrder = type;
      $scope.orderOfcrit = !$scope.orderOfcrit;

    }

    $scope.goToPostItem=function () {

      preloader.show();
      $location.path("dashboard/"+$scope.nameRouteforControllerElement+"/novo");

    }


    $scope.recharge=function (page=null,itemSearch=null) {

        return listReader.rechargeList($scope,page=null,itemSearch=null,defaultAPIservice,nameOfGroupOfElements);

    }

    $scope.searchItem=function () {

      $scope.recharge(null,$scope.search);

    }

    $scope.goToPage=function(page){

      $(".preloader").fadeIn();
      $scope.recharge(page);

    }

    $scope.deleteItens=function () {

      return deleter.deleteAllInChest=function($scope,nameOfGroupOfElements,'selected','id',defaultAPIservice);

    }

    $scope.deleteItem=function(itemId){

      return deleter.deleteOne=function($scope,defaultAPIservice,itemId);

    }

}])

app.controller("examplesOneCrtl",
['$scope',
'config',
'$location',
'authService',
'preloader',
'deleter',
'examplesAPIService',
'example',
function($scope,config,$location,authService,preloader,deleter,examplesAPIService,example) {

  var defaultAPIservice = examplesAPIService;

  //Get User Information
  $scope = authService.userInformationToView($scope);

  $scope.example = example.element[0];

  $scope.put=function () {

    preloader.show();
    var request = examplesAPIService.put($scope.example,$scope.example.id)
      .success(function(data,status,header){
          $scope.example.id = data.execution.id;
          preloader.hide();
        });

  };

  $scope.deleteItem=function(){

    return deleter.deleteOne=function($scope,defaultAPIservice,itemId);

  }

}])
