app.config(function($routeProvider){

  //***********Examples Routes POST***********//
  $routeProvider.when("/dashboard/exemplos/novo",{
    templateUrl: "assets/admin/app/modules/examples/views/examplesPost.view.html",
    controller: "examplesCrtl",
    title: "Novo Exemplo",
    authenticated: true,
    usertype: 'admin',
    resolve:{
      load: function (preloader){
        preloader.show();
      }
    }
  });

  //***********Examples Routes LIST***********//
  $routeProvider.when("/dashboard/exemplos",{
    templateUrl: "assets/admin/app/modules/examples/views/examplesList.view.html",
    controller: "examplesListCrtl",
    title: "Meus Exemplos",
    authenticated: true,
    usertype: 'admin',
    resolve:{
      load: function (){
        preloader.show();
      },
      elementsRequest:function (examplesAPIService) {
        return examplesAPIService.getAll();
      }
    }
  });

  //***********Examples Routes SHOW***********//
  $routeProvider.when("/dashboard/exemplos/:id",{
    templateUrl: "assets/admin/app/modules/examples/views/examplesOne.view.html",
    controller: "examplesOneCrtl",
    title: "Editar Exemplo",
    authenticated: true,
    usertype: 'admin',
    resolve:{
      load: function (){
        preloader.show();
      },
      example:function (examplesAPIService,$route) {
        return examplesAPIService.getOne($route.current.params.id);
      }
    }
  });


});
