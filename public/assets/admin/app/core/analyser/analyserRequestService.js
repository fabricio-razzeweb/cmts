app.factory('analyserRequestService',function (config,$location,alerter,preloader) {

  var analyserRequestService={};
  analyserRequestService.config=config;

  analyserRequestService.analyse=function(response,APIService) {

    var username = localStorage.getItem('username');

      if (response.status==422) {

        var AlertMsg="<b>Por favor "+username+":</b><br> ";

        for (var i = 0; i < APIService.fillable.length; i++) {

            if (response.data[APIService.fillable[i]]) {

               AlertMsg += " "+response.data[APIService.fillable[i]]+"<br>";

            }

        }
        alerter.alert(AlertMsg);
        preloader.hide();
      }
      if (response.status==500) {

          alerter.alert(username+" desculpe ocorreu um erro ao processar está atividade por favor entre em contato com nossa equipe");
          preloader.hide();

      }



  };


  return analyserRequestService;
});
