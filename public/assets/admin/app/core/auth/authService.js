app.factory('authService',function (config,$location,analyserRequestService,alerter) {

    var authService={};
    authService.config=config;

    authService.getStored=function(nameItem) {
        config = this.config;
        if(this.getItem(nameItem)){
            return true;
        }else{
            return false;
        }
    };

    authService.getItem=function(nameItem){
        try {
            var value = window.localStorage.getItem(nameItem);

            if (value==undefined) {
                throw false;
            }

        } catch (e) {
            $(document).ready(function () {
                var value = window.localStorage.getItem(nameItem);
                return value;
            })
        }

        return value;
    };

    authService.getAuthenticated=function() {
        if(this.getStored("userId")
        && this.getStored("username")
        && this.getStored("tk")){
            return true;
        }else{
            return false;
        }
    };

    authService.getUserInfo=function(info){
        return this.getItem(info);
    };

    authService.getUserId=function(){
        return this.getItem("userId");
    }

    authService.logout=function(){
        window.localStorage.clear();
        //var msg = "Você precisa logar novamente para usar este serviço!";
        //alerter.alert(msg);
        window.location="dashboard/logout";
    }

    authService.checkRequest=function(request,APIService){
        request
        .then(function(response) {

            if (response.data) {

                if (response.data.execution.msg!=undefined) {
                    //alerter.alert(response.data.execution.msg);
                }

            }

            return response.data;

        }, function(response) {

            if(response.status==401){

                return authService.logout();

            }else{

                return analyserRequestService.analyse(response,APIService);

            }

        });

        return request;
    }

    authService.authRequest=function(){

        var requestConfig = {
            headers: {
                transformRequest: angular.identity,
                'Content-Type': undefined,
                'Authorization': 'Bearer '+authService.getUserInfo("tk")
            },};

            return requestConfig;
        }

        authService.userInformationToView=function ($scope) {

            $scope.username = authService.getItem('username');
            $scope.userPhoto = authService.getItem('userPhoto');
            $scope.userId = authService.getItem('userId');
            return $scope;
        }

        return authService;
    });
