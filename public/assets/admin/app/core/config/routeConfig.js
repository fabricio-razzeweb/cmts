//AUTHENTICATED ROUTES
app.run(['$location', '$rootScope','authService', function($location, $rootScope, authService) {
    $rootScope.$on('$routeChangeSuccess', function (event, next, current) {
        if(next.$$route.authenticated){
            if(!authService.getAuthenticated()){
                $location.path("/");
            }
        }
        if(next.$$route.originalPath == "/"){
            if(authService.getAuthenticated()){
                $location.path(current.$$route.originalPath);
            }
        }
    });
}]);

app.config(function($routeProvider,config){

    //***********Dashboard Acess***********//
    $routeProvider.when("/dashboard/logout",{
        resolve:{
            logout: function (authService){
                return authService.logout();
            }
        }
    });

});
