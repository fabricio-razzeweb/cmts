// IDEA Create repository with name "RazzorChat" this is the name of application
;(function ($, document, window) {
    'use strict';

    var chat = {
        connection: {
            socket: io('http://imasto.com.br:6969'), //IDEA Set this atribute in constructor
            current_hash: $('#session_hash').val() //IDEA Set this atribute in constructor with optional value and default value
        },
        controls: {
            send: $('#send-message'), //IDEA Set this atribute in constructor with optional value and default value
            messages: $('#chat-messages'), //IDEA Set this atribute in constructor with optional value and default value
            message: $('#message'), //IDEA Set this atribute in constructor with optional value and default value
            info: $('#chat-info')
        },
        model: {

            // Type: sent, received or error.
            // Message: content of message.
            message: function (type, message) {
                return '<div class="message clearfix">' +
                    '<div class="message-container ' + type + '">' +
                            '<span>' + message + '</span>' +
                    '</div>' +
                '</div>';
            },
            data: function (time) {
                return '<div class="message clearfix">' +
                    '<div class="message-data">' +
                        '<time datetime="' + time +'">' +
                            time +
                        '</time>' +
                    '</div>' +
                '</div>';
            }
        },
        methods: {
            scroll: function () {
                this.controls.messages.parent('.modal-body').animate({
                    scrollTop: this.controls.messages.parent('.modal-body').scrollTop() + $('.message').last().height() + 10
                }, 300);
            },
            data: function (message) {
                var data = message.created_at.split(' ').shift();
                var lastData = $('.message-data')
                                            .last()
                                            .children('time')
                                            .attr('dateTime');

                if (lastData !== data) {
                    var dataDom = chat.model.data(data);
                    chat.controls.messages.append(dataDom);
                }

                return;
            },
            publishMessage: function (type, message) {

                // Parte crítica do sistema, necessário uso de métodos de
                // diversos módulos, portanto, dispensável o uso do this.
                var messageDom = chat.model.message(type, message.message)

                if (type !== 'error')
                    this.data(message);

                chat.controls.messages.append(messageDom);
                this.scroll.call(chat);
            },
            channel: function (connection_hash) {
                chat.connection.socket.on('chat.' + connection_hash, function (data) {
                    this.listMessage(data.message);
                }.bind(this));
            },
            requestInfo: function () {
                $('#chat-info').show('fast');
                this.controls.messages.hide('slow');
                this.controls.send.attr('disabled', true);
                this.controls.message.attr('placeholder', 'Estamos quase lá :P!');
                $('#user-send').on('click', this.methods.registerUser.bind(this));
            },
            registerUser: function (e) {
                e.preventDefault();

                var user = {
                    name: $('#user-name').val(),
                    phone: $('#user-phone').val(),
                    email: $('#user-email').val()
                };

                if (user.name && user.phone && user.email) {
                    $.ajax({
                        url: 'api/chat/site/' + chat.connection.current_hash,
                        type: 'POST',
                        dataType: 'json',
                        data: user,
                        success: function (data) {
                            localStorage.setItem('session_hash', this.connection.current_hash);
                            this.controls.messages.show('slow');
                            this.controls.send.attr('disabled', false);
                            this.controls.message.attr('placeholder', 'Digite a sua mensagem');
                            $('#chat-info').hide('fast');
                        }.bind(this),
                        error: function (jqXHR, textStatus, errorThrown) {}.bind(this)
                    });
                } else {
                    $('#chat-alert').modal('show');
                }
            },
            verifyIfExistOldHash: function () {
                if (!localStorage.getItem('session_hash')) {
                    this.methods.requestInfo.call(this);
                    return false;
                }

                $('#chat-info').hide('fast');
                this.connection.current_hash = localStorage.getItem('session_hash');
                this.methods.getMessages(this.connection.current_hash);
            },
            listMessage: function (message) {
                var messageSession = message.chat_sessions_id;

                if (messageSession) {
                    this.publishMessage('sent', message);
                } else {
                    this.publishMessage('received', message);
                }
            },
            getMessages: function (connection_hash) {
                $.ajax({
                    url: 'api/chat/site/' + connection_hash,
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        var messages = data.chat.messages;

                        messages.forEach(function (message) {
                            this.listMessage(message);
                        }.bind(this));

                        this.channel(data.chat.hash);
                    }.bind(this),
                    error: function (jqXHR, textStatus, errorThrown) {}.bind(this)
                });
            },
            sendMessage: function (e) {
                e.preventDefault();
                var message = this.controls.message.val();

                if (message) {
                    $.ajax({
                        url: '/api/chat/site',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            message: message,
                            session_hash: this.connection.current_hash
                        },
                        success: function (data) {
                            var date = new Date().toJSON().slice(0,10);
                            var res = {
                                message: message, //IDEA add callback function editable for example add two points crypt and others tecnologys
                                created_at: date
                            };

                            this.methods.publishMessage('sent', res);
                            this.controls.message.val('');
                            this.controls.message.focus();
                        }.bind(this),
                        error: function (jqXHR, textStatus, errorThrown) {
                            this.methods.publishMessage('error', {
                                message: 'Desculpe, não foi possível enviar sua mensagem. Por favor, tente novamente mais tarde.'
                            });
                        }.bind(this)
                    });
                }
            }
        },
        init: function () {
            this.methods.verifyIfExistOldHash.call(this);
        }
    };

    chat.init();

    $('#chat-modal').on('show.bs.modal', function () {
        $(document).keypress(function (e) {
            if (e.which === 13 || e.keyCode === 13) {
                e.preventDefault();
                chat.controls.send.click();
            }
        });
    });

    chat.controls.send.on('click', chat.methods.sendMessage.bind(chat));

})(jQuery, document, window);
//IDEA Good Job - N = 8.9
