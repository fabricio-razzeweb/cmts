;(function ($, document, window) {
    'use strict';

    function scrollBanner() {
        var scrollPos = $(window).scrollTop(),
            headerText = $('.header-paralax h1')[0];

        headerText.style.marginTop = -(scrollPos / 3) + 'px';
        headerText.style.opacity = 1 - (scrollPos / 300);
    }

    function navbarByScroll() {
        var scrollTop = $(window).scrollTop(),
            mobileWidth = 768,
            navbar = $('nav');

        if (parseInt(scrollTop, 10) > 80 ||
            $(window).width() < mobileWidth) {
            navbar.removeClass('navbar-custom');
            navbar.addClass('navbar-default');
        } else {
            navbar.removeClass('navbar-default');
            navbar.addClass('navbar-custom');
        }
    }

    if ($('body').hasClass('paralax')) {
        scrollBanner();
        $(window).scroll(scrollBanner);
    }

    if ($('body').hasClass('custom-navbar')) {
        navbarByScroll();
        $(window).scroll(navbarByScroll);
    }

    $(window).load(function(){
        $('#preloader').delay(1000).fadeOut('slow');
    });
})(jQuery, document, window);
