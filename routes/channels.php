<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/
Broadcast::channel('chat.*', function ($user, $chatHash) {
    ($user->id === \Modules\Chat\Chat::findFromHash($chatHash)->receiver_id ||
            $user->id === \Modules\Chat\Chat::findFromHash($chatHash)->sender_id);

    return true;
});
