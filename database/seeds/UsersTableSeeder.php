<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
          'name' => "Fabricio Magalhães",
          'photo'=>'default-user.png',
          'email'=>'fabriciosena70@gmail.com',
          'password'=>bcrypt("f8b8c1r1"),
      ]);
    }
}
