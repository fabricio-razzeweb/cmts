<?php

return array(
    '/' => 'index',
    'curso' => 'course',
    'cursos' => 'courses',
    'artigo' => 'article',
    'artigos' => 'articles',
    'noticias' => 'notices',
    'agenda' => 'diary',
    'instalacoes' => 'locations',
    'historico' => 'historic'
);
