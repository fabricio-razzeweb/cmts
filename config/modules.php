<?php
/*
* Set Modules which has Routes
*/

    return [
        'modules' => [
            'Blog',
            'Users',
            'Hospital',
            'MyEnterprise',
            'Site',
            'Mails',
            'Chat',
            'PushNotifications',
            'FullNotifications',
            'Newsletter',
            'BussinessAnalyse',
            'NotificationsAlerts'
        ]
    ];


?>
