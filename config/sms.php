<?php
return [
    'sms_api_url' => env('SMS_API_URL'),
    'sms_api_username' => env('SMS_API_USERNAME'),
    'sms_api_password' => env('SMS_API_PASSWORD')
];
