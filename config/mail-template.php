<?php
$base = "www.imasto.com.br/assets/mails/";
return [
    'basePathOfResources' => $base,
    'mainColor' => "#C6146C",

    'socialLinksList' => [
        'facebook' => [
            'name' => "facebook",
            'link' => "facebook",
            'icon' => "icon-facebook.png"
        ],
        'instagram' => [
            'name' => "instagram",
            'link' => "instagram",
            'icon' => "icon-instagram.png"
        ],
        'twitter' => [
            'name' => "twitter",
            'link' => "twitter",
            'icon' => "icon-twitter.png"
        ],
    ],

    'enterpriseInfo' => [
        'name' => "Imasto - Instituto Conquistense de Mastologia",
        'contactMethods' => [
            'phone' => [
                'image' => $base."images/icon-phone.png",
                'name' => "Telefone:",
                'content' => "(077) 9.8809-2842"
            ],
            'location' => [
                'image' => $base."images/icon-location.png",
                'name' => "Localização:",
                'content' => "Localização"
            ],
            'mail' => [
                'image' => $base."images/icon-email.png",
                'name' => "Emails:",
                'content' => "contato@imasto.com.br"
            ]
        ],
        'logo' => 'www.imasto.com.br/src/layout/logos/logo-imasto-pink.png',
        'website' => 'www.imasto.com.br',
        'copyright' => 'Todos os direitos reservados Imasto -- Desenvolvido por Razze Web',
        'btnSiteLink' => [
            'label' => "Visitar",
            'link' => "www.imasto.com.br"
        ]
    ],
];
